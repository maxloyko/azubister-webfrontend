if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

function getCookieData( name ) {
    var patrn = new RegExp( "^" + name + "=(.*?);" ),
        patr2 = new RegExp( " " + name + "=(.*?);" );
    if ( match = (document.cookie.match(patrn) || document.cookie.match(patr2)) )
        return match[1];
    return false;
}

function isUrlExternal(url) {

    if (url.search('/') === 0) {
        return false;
    }

    if (url.search('#') === 0) {
        return false;
    }

    if (url.search('javascript:') === 0) {
        return false;
    }

    if (url.search('azubister.net') >= 0) {
        return false;
    }
    return true;
}

function trackExternalClick(url, link_label) {
    if (typeof url != 'undefined' && isUrlExternal(url)) {
        var label = _trackExternalData.label;

        if (link_label) {
            label += ' ' + link_label;
        }
        var td = _trackExternalData;

        _gaq.push(['_trackEvent', td.category, td.action, label, td.value, td.nonInteraction]);
    }
}

jQuery(document).ready(function($) {

    // Add flag to links to blacklist them from event tracking
    $('.likes a, .footer a').each(function() {
        $(this).data('nonTrackable', true);
    });

    $(document).on('click','.externaljoboffer a', function() {
        var p = $(this).parents('.externaljoboffer');
        if (_gaq) {
            _gaq.push(['_trackEvent', 'External joboffer click', p.data('source'), p.data('id')+'', 0, false]);
        }
    });

    $('a').click(function() {
        var url = $(this).attr('href');
        if (!isUrlExternal(url)) {
            return true;
        }

        if ($(this).data('nonTrackable')) {
            return true;
        }
        
        /* Tracking of partners joboffers is done separately */
        var p = $(this).parents('.externaljoboffer');
        if (p && p.length > 0) {
            return true;
        }

        if ((typeof _gaq != 'undefined') && (typeof _trackExternalData != 'undefined')) {
            var label = '';
            if ($(this).data('linklabel')) {
                label = $(this).data('linklabel');
            } else {
                label = $(this).html().replace(/\s+/g, ' ');
            }
            trackExternalClick(url, label);
        }
    });

//main slider

var shouldCycle = true;

if (document.documentElement && document.documentElement.clientWidth <= 480) {
    shouldCycle = false;
}
if (shouldCycle && $('.main-slider').length >0) {
	$('.main-slider').cycle({ 
	    fx: 'fade',
	    timeout: 10000,
	    pager: '.main-slider-navi'
	});
	$('.main-slider-navi a').html('').click(function(){ $('.main-slider').cycle('pause'); });
};
$('.main-partners__logos img').one('load', function() {
    var w = $(this).width();
    var h = $(this).height();
    $(this).css({
        'top':  44 - h / 2 + 'px',
        'left': 76.5 - w/ 2 + 'px',
        'margin': 0}); 
}).each(function() {
    if (this.complete) $(this).load();
});

//block small nav and search
$('.nav-s li').click(function() {
	var nav_s = $(this).attr('data-item');
	$('.nav, .search').removeClass('show');	
	if ($(this).hasClass('active')) {
		$('.nav-s li').removeClass('active');
		$('.'+nav_s).removeClass('show');		
	}
	else {
		$('.nav-s li').removeClass('active');
		$(this).addClass('active');	
		$('.'+nav_s).addClass('show');
	}
});
$('.nav__search').click(function() {
	$('.search').addClass('show');
	return false;
});
$('.search__close').click(function() {
	$('.search').removeClass('show');
});

if ($.fn.truncate) {
    $('.company-profile__detail__description').truncate({max_length: 200, more: 'mehr...', less: ' <br>weniger anzeigen'});
}
//get hash on page
var hash = window.location.hash;
switch (hash) {
	case '#Ausbildungen':
		$('.tabs__nav a').removeClass('active');
		$('.job-training_article').css('display', 'none');
		$('.tabs__nav li:nth-child(2) a').addClass('active');
		$('.training').css('display', 'block');
	break;
	case '#Betriebe':
		$('.tabs__nav a').removeClass('active');
		$('.job-training_article').css('display', 'none');
		$('.tabs__nav li:nth-child(3) a').addClass('active');
		$('.logos').css('display', 'block');
	break;

	default:
	break;
}

$('.taxonomy_tabs li a').click(function() {
	switch ($(this).attr('href')){
		case '#job-training_article':
			window.location.hash = '';
		break;

		case '#training':
			window.location.hash = 'Ausbildungen';
		break;
		case '#logos':
			window.location.hash = 'Betriebe';
		break;
		default:
		break;
	}
});
//tabs
$('.company_tabs .tabs__nav a, .joboffers_landing .tabs__nav a, .taxonomy_tabs .tabs__nav a').click(function() {
    if ($(this).parent().hasClass('tabs__blue')) {
        return true;
    }
	if ($(this).hasClass('active')) {}
	else {
		$('.tabs__nav a').removeClass('active');
		$('.tabs__item').slideUp(200);
		var tab_val = $(this).attr('href');
        if ($(this).data('tab')) {
            tab_val = $(this).data('tab');
        }
		$(this).addClass('active');
		$('.'+tab_val).slideDown(200);
	}	
	return false;
});

//video popup
$('.our-video a').click(function() {
	var video = $(this).attr('href');
	$('.bg, .popup_video').fadeIn(200);
	$('.popup__iframe').html(video);
	return false;
});
$('.popup__close').click(function() {
	$('.bg, .popup').fadeOut(200);
});


$('a.gallery, a.lightbox').fancybox({
    openEffect: 'none',
    closeEffect: 'none',
    prevEffect: 'none',
    nextEffect: 'none'
});

//show all gallery items
$('.gallery .view-all').click(function() {
	var all_shown = $(this).data('all_shown');

	if (!all_shown) {
		all_shown = false; 
	}

	if (all_shown) {
		$('.gallery li:gt(5)').hide();
	} else {
		$('.gallery li').show();
	}
	$(this).data('all_shown', !all_shown);
	return false;
});

$('.tabs__item.partners a.partner_button').click(function(){
    var popupObj=$('.popup_partners_template').clone();
    var popup_partner_name=$(this).find('span').text();
    var popup_partner_url=$(this).next().find('a.popup_partner_url').attr('href');
    var popup_partner_image=$(this).next().find('img.popup_partner_image').attr('src');
    var popup_partner_text=$(this).next().find('div.popup_partner_text').html();

    $('.bg').css('display','block');
    popupObj.css('display','block');
    popupObj.addClass('popup').addClass('popup_partners');
    popupObj.removeClass('popup_partners_template');
    popupObj.children('a.link-out').attr('href',popup_partner_url);
    popupObj.children('a.link-out').html(popup_partner_name+'<i></i>');
    popupObj.children('img').attr('src',popup_partner_image);
    popupObj.children('img').attr('alt',popup_partner_name);
    popupObj.children('strong').text(popup_partner_name);
    popupObj.children('p').replaceWith(popup_partner_text);

    popupObj.insertAfter($('.popup_partners_template'));

    popupObj.find('.popup__close').click(function() {
	$('.bg, .popup').fadeOut(200);
        $('.popup_partners').remove();

    });

    return false;
});

$('.job-training .articles img').one('load', function() {
    var h = $(this).height();
    if (h < 90) {
        $(this).css({
            'padding': 45 - h / 2 + 'px' + ' 0'});
    }
}).each(function() {
    if (this.complete) $(this).load();
});

//show other staticpages on company landing page
$('.job-training .view-all a').click(function() {
    $('.articles li').fadeIn(200).show();
    $(this).parent().hide();
    return false;
});

$('form.search').azubisterSearchform();
$('form.ext-search, .big-search form').each(function(){
    var type = $(this).data('search-type');
    $(this).azubisterSearchform({'type': type })
    }
);

// tooltips
var tooltip_timeout;
var last_tooltip;
$('.tooltip').hide();
$('.tooltip-open').mouseover(function(){
    if (this !== last_tooltip) {
        $(last_tooltip).children('.tooltip').hide();
    }
    clearTimeout(tooltip_timeout);
    $(this).children('.tooltip').show();
});
$('.tooltip-open').mouseout(function(){
    tooltip_timeout = setTimeout(function() {
        $('.tooltip').hide();
    }, 500);
    last_tooltip = this;
});
$('.tabs__item.study-offers .view-all a').click(function() {
    $('.tabs__item.study-offers li').fadeIn(200).show();
    $(this).parent().hide();
    return false;
});
$('.tabs__item.apprenticeships .view-all a').click(function() {
    $('.tabs__item.apprenticeships li').fadeIn(200).show();
    $(this).parent().hide();
    return false;
});

$('.aside-articles .view-all a').click(function() {
    $('.aside-articles li').fadeIn(200).show();
    $(this).parent().hide();
});

$('.more-articles.widget .view-all a').click(function() {
    $('.more-articles.widget li').fadeIn(200).show();
    $(this).parent().hide();
    return false;
});
$('.company-jobs .view-all a')
    .add('.aside-info .view-all a')
    .add('.city-logos .view-all a')
    .add('.more-jobgroups .view-all a')
    .add('.partner-widget .view-all a').click(function() {
    if ($(this).data('expanded') == undefined || $(this).data('expanded') == false) {
        $(this).parent().parent().find('li').fadeIn(200).show();
        $(this).text('Weniger anzeigen');
        $(this).data('expanded', true);
    } else {
        var less_count = 4;
        if ($(this).data('less_count')) {
            less_count = $(this).data('less_count')-1;
        }
        $(this).parent().parent().find('li:gt('+less_count+')').fadeOut(200).hide();
        $(this).text('Alle anzeigen');
        $(this).data('expanded', false);
    }
    return false;
});

$('.splittable_show').click(function() {
	$(this).hide();
	$('.splittable_content').fadeIn(200).show;
	return false;
});

$('.top-employer__city .view-all a').click(function() {
    $('.top-employer__city li').fadeIn(200).show();
    return false;
});


});

