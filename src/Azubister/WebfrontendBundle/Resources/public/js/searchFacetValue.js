/*!
 * searchFacetValue.js
 * 
 * SearchFacetValue Class
 * 
 * @author: Karsten Beyer <karsten@webpard.de>
 */
function SearchFacetValue(value, field, label) {
	this.value = value;
	this.selected = false;
	this.count = 0;
	this.field = field;
	this.label = label;
}

SearchFacetValue.prototype.render = function() 
{
	var html = '<li>';
	
	html += '<input';
	html += ' type="checkbox"';
	if (this.selected) {
		html += ' checked="checked"';
	}
	
	html += ' data-value="' + this.value + '"';
	html += ' data-field="' + this.field + '"';
	html += ' class="toggleFacetValue"';
	html += '/><label for="">';
	html += this.label;// + '(' + this.count + ')';
	html += '</label></li>';
	
	return html;
};
SearchFacetValue.prototype.renderFilter = function() 
{
	var html = '';
	
	if (this.selected)
	{
		html +='<li data-type="facet" data-field="' + this.field + '" data-value="' + this.value + '">' + this.label + '<i></i></li>';
	}
	
	return html;
};
//<li><input id="f1" type="checkbox"><label for="f1">Betriebe</label></li>
