var joboffer = new function(){

    this.job_id = '';
    this.city_id = '';

    this.getURLHashParameter = function(name) {

        return decodeURI(
            (RegExp('[#|&]' + name + '=' + '(.+?)(&|$)').exec(location.hash)||[,''])[1]
        );
    } 

    this.onHashChange = function() {
        this.city_id = this.getURLHashParameter('city');
        this.job_id = this.getURLHashParameter('job');
        this.updateTooltips();
        $('.current_city').add('.current_job').hide();
    }

    this.changeCity = function(city_id){
        this.changeHash(city_id, this.job_id);
    }

    this.changeJob = function(job_id){
        this.changeHash(this.city_id, job_id);
    }

    this.changeHash = function(city, job) {
        var values = new Array();
        if (city != '')
            values.push('city='+city);
        if (job != '')
            values.push('job='+job);

        location.hash = '#' + values.join('&');
        this.updateTooltips();
    }

    this.updateTooltips = function() {

        var checks = ['city', 'job'];
        
        for (i in checks) {
            var id = ( checks[i] == 'city' ) ? this.city_id : this.job_id;
            var current = (id != '') ? id : $('.current_'+checks[i]+' a').first().data('id');
            
            if (typeof(current) == 'undefined') continue;

            $('.current_'+checks[i]).removeClass('current_'+checks[i]).show();
            $('.'+checks[i]+'_'+current).addClass('current_'+checks[i]);
            $('.current_'+checks[i]+'_container').html($('.current_'+checks[i]+' a').html());
        }
    }

    this.init = function() {
        $(window).bind('hashchange', function() {
            joboffer.onHashChange();
        });

        $('.another-articles .view-all a').click(function() {
            $('.articles li').fadeIn(200).show();
            $(this).parent().hide();
            return false;
        });

        var popup_app = $('.popup_app');

        $('.job-info__apply a').click(function() {
             $('.bg').add(popup_app).fadeIn(200);
             if (_gaq && _jobofferApplyTrackData) {
                 var _ad = _jobofferApplyTrackData;
                 _gaq.push(['_trackEvent', _ad.category, _ad.action, _ad.label, _ad.value, _ad.nonInteraction]);
             }
             return false;
        });

        $(popup_app).find('.app-list li.app-list__post a').click(function() {
             $(popup_app).children('.app-list').hide();
             $(popup_app).children('.app-info').show();
             return false;
        });

        $(popup_app).find('.app-info > a').add($(popup_app).children('.popup__close')).click(function() {
             $(popup_app).children('.app-info').hide();
             $(popup_app).children('.app-list').show();
             return false;
        });

        $(popup_app).find('.app-list li.app-list__email a').add(
        $(popup_app).find('.app-list li.app-list__online a')).click(function() {
	       $('.bg, .popup').fadeOut(200);
        });


        if (window.addEventListener){
            window.addEventListener("message", this.listener,false);
        } else {
            window.attachEvent("onmessage", this.listener);
        }

        this.onHashChange();
    }

    this.listener = function(event){
        if (event.origin.indexOf("azubister") == -1)
            return;

        var data = JSON.parse( event.data );

        if ( typeof(data.height) != 'undefined') {
            $('#offer_frame').height(data.height); 
        }

        if (data.e_type != undefined && data.e_type == 'linkClick') {
            trackExternalClick(data.href, data.label);
        }
    }
    
 
    this.setHeight = function() {
    }

}
