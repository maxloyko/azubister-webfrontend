var Socialbar = function(fixedHeight) { 
    var $ = jQuery;

    this.offset = {'top' : 0};
    this.offset.top = $('.likes').offset().top * 1;

    this.fixed = fixedHeight;

    this.ToFixed = function() {
        $('.likes').css('position', 'fixed');
        $('.likes').css('top', this.fixed);
    }

    this.ToNormal = function() {
        $('.likes').css('position', 'relative');
        $('.likes').css('top', 0);
    }   

    this.Resize = function() {
        this.updateBox();
        this.OnScroll();
    }

    this.updateBox = function(duration) {
        this.specialWidth = ($('.likes__out:visible').length <= 0);
    }

    this.OnScroll = function() {

        if ($(window).scrollTop() > this.offset.top - this.fixed && !this.specialWidth) {
            this.ToFixed();
        } else {
            this.ToNormal();
        }

    }

    this.init = function() {

        this.OnScroll();
        this.updateBox();
        
    }

}

var Skyscraper = function(fixedHeight, timeout) { 
    var $ = jQuery;

    this.offset = {'top' : 0, 'left': 0};
    this.offset.top = $('.likes').offset().top * 1 + $('.likes').height() * 1;
    this.offset.left = $('.wrap').offset().left * 1 + $('.wrap').width() * 1;

    this.fixed = fixedHeight;
    this.timeout = timeout;

    this.ToFixed = function() {
        $('.banner-wrapper').css('position', 'fixed');
        $('.banner-wrapper').css('top', this.fixed);
        $('.banner-wrapper').css('left', this.offset.left - (180 * this.specialWidth));
    }

    this.ToNormal = function() {
        $('.banner-wrapper').css('position', 'relative');
        $('.banner-wrapper').css('top', 0);
        $('.banner-wrapper').css('left', 0);
    }   

    this.Resize = function() {
        this.updateBox();
        this.OnScroll();
    }

    this.updateBox = function(duration) {

        var pspw = (typeof this.specialWidth == 'undefined') ? -1 : this.specialWidth;

        this.specialWidth = ($('.banner:visible').length > 0) ? 0 : 1;

        if (pspw == -1 || pspw != this.specialWidth) {
            this.loadWithTimeout();
        }

        this.offset.top = $('.likes').offset().top * 1 + $('.likes').height() * 1;
        this.offset.left = $('.wrap').offset().left * 1 + $('.wrap').width() * 1;
    }

    this.OnScroll = function() {

        if ($(window).scrollTop() > this.offset.top - this.fixed) {
            this.ToFixed();
        } else {
            this.ToNormal();
        }

    }

    this.loadWallpaper = function(id) {

        $.post('/ajax/banner', {'banner_id': id}).done(function(data) {
            if (data == '') { return; }

            var banner = document.getElementById("wallpaper_banner");
            banner.innerHTML = data; // we should use innerHTML instead of jquery $(*).html, as with jquery tag script will be ignored
        })
    }

    this.load = function() {

        if ($('.banner-wrapper').length <= 0) { return; }
        $('#wallpaper_banner').html('');

        var type = ($('.banner:visible').length > 0) ? 'skycrapper' : 'mobile_floater';
        if (type == 'mobile_floater') {
            return;
        }

        $.post('/ajax/advertisement', {'type': type}).done(function(data) {
            if (data == '') { return; }

            if (type == 'skycrapper') {
                //data = data + '<div class="banner-label"><span>Anzeige</span></div>';
                var banner = document.getElementById("skyscrapper_banner");
                banner.innerHTML = data; // we should use innerHTML instead of jquery $(*).html, as with jquery tag script will be ignored
            } else if (type == 'mobile_floater') {

                data = '<span class="floater-label">Anzeige</span>' + data;

                if ($('.mobile_floater-wrapper').length > 0) {
                    $('.mobile_floater').html(data);
                } else {
                    data = '<div class="mobile_floater-wrapper"><div class="mobile_floater">' + data + '</div></div>';
                    $('.footer-out').after(data);
                }
            }
        })
    }

    var self = this;

    this.listener = function(event) {

        var type = ($('.banner:visible').length > 0) ? 'skycrapper' : 'mobile_floater';
        if (type != 'skycrapper') {
            return;
        }

        if (event.origin.indexOf("azubister") == -1)
            return;

        var data = JSON.parse( event.data );

        var parent_this = this;
        if ( typeof(data.banner_rel) != 'undefined') {
            self.loadWallpaper(data.banner_rel);
        }

    }

    this.loadWithTimeout = function() {
        this.load();

        if (typeof this.timer != 'undefined') {
            clearTimeout(this.timer);
        }

        if (typeof this.timeout !== 'undefined') {
            this.timer = setInterval(this.load, this.timeout);
        }
    }

    this.init = function() {

        this.OnScroll();
        this.updateBox();
        
        if (window.addEventListener){
            window.addEventListener("message", this.listener,false);
        } else {
            window.attachEvent("onmessage", this.listener);
        }
    }
}
