$(window).load(function(){
    var h;
    if (window.document.documentElement) {
        h =  window.document.documentElement.scrollHeight;
    } else {
        h = window.document['body'].scrollHeight;
    }
    parent.postMessage(
        JSON.stringify( { 'height': h } ),
        "*"
    );
});

$(document).ready(function() {
    $('a').click(function() {
        if ($(this).attr('href') !== undefined) {
            parent.postMessage(
                JSON.stringify( { 'e_type': 'linkClick', 'label': $(this).html().replace(/\s+/g, ' '), 'href': $(this).attr('href') } ),
                "*"
            );
        }
    });
});
