var SponsorsReloader = function(timeout) {
    
    this.load = function() {

        $('.search-results__sponsored').each(function() {

            var current = this;

            $.post('/ajax/sponsored').done(function(data) {
                if (data == '') { return; }
                $(current).replaceWith(data);
            });
        });
    }

    if (typeof timout != 'undefined') {
        this.timer = setInterval(this.load, timeout);
    }
}
