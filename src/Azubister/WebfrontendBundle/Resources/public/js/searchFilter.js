/*!
 * searchFilter.js
 * 
 * SearchFilter Class
 * 
 * @author: Karsten Beyer <karsten@webpard.de>
 */
 function SearchFilter(field, value) {

	this.label = null;
	this.field = field;
	this.value = value;
	
}

 SearchFilter.prototype.setLabel = function(label) {
	this.label = label;
};

 SearchFilter.prototype.getLabel = function() {
	return this.label;
};

 SearchFilter.prototype.getField = function() {
	return this.field;
};

 SearchFilter.prototype.getValue = function() {
	return this.value;
};




