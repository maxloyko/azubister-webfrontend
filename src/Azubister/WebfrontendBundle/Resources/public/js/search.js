var supportsHistoryAPI=!!(window.history && history.pushState);

function goToWithAjax(el) {
    var link = el.attr('href');
    if (typeof link !== 'undefined' && link !== false) {
        refreshPage(link, true);
    }
}

function refreshPage(link, changeLink) {
    if (initLocation == link) {
        return;
    }
    initLocation = link;

    jQuery('#results .loading').show(); // loading show
    jQuery('html, body').animate({
        scrollTop: 0
    }, 500);

    jQuery.ajax({
        type: "POST",
        url: link,
        data: { json: 1 }
    })
    .success(function( json ) {
        if (json.status == 'ok') {
            jQuery('.resultsList').html(json.docs);
            if (json.docs.length == 0) {
                jQuery('#results .noResults').show();
            } else {
                jQuery('#results .noResults').hide();
            }
            jQuery('#facetList').html(json.facet_list);
            jQuery('#currentFilters').html(json.filters);
            jQuery('.pager').html(json.search_pager);

            init();
            if (changeLink) {
                var title = $('title').text();
                history.pushState(null, title, link);
            }
            jQuery('#results .loading').hide(); // loading hide
        } else {
            document.location = link;
        }
    })
    .fail(function(){
        document.location = link;
    });

}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function insertParam(key, value) {
    key = encodeURI(key); value = encodeURI(value);

    var kvp = document.location.search.substr(1).split('&');

    if (kvp == '') {
        return '?' + key + '=' + value;
    }

    var i=kvp.length;
    var x;
    while(i--) {
        x = kvp[i].split('=');

        if (x[0]==key)
        {
            x[1] = value;
            kvp[i] = x.join('=');
            break;
        }
    }

    if(i<0) {kvp[kvp.length] = [key,value].join('=');}

    return '?' + kvp.join('&');
}

function handleSearchformSubmit(el) {
    var q = getParameterByName('q');
    var searchterm = $('#inlineSearchForm').find('.searchform').val();
    if (q === searchterm) {
        return false;
    }
    var url = '';
    url = insertParam('q', searchterm);
    url = document.location.origin + document.location.pathname + url;
    refreshPage(url, true);
    return false;
}

function handleFacetToggle(event) {
	var node = event.currentTarget;

	if ($(node).attr('data-locked') == 'true') {
		return;
	}

    $(node).toggleClass('open');
    var ul = $(node).next();
    if (ul.is(':visible')) {
        ul.slideUp();
    } else {
        ul.slideDown();
    }
}

function init() {
    if (supportsHistoryAPI) {
        jQuery('#facets a, #results .pager a, #currentFilters a').click(function(){
            goToWithAjax(jQuery(this));
            return false;
       });
    }
    jQuery('#facets h3').click(function(el){
        handleFacetToggle(el);
    });

    $('#inlineSearchForm').unbind();
    $('#inlineSearchForm').submit(function(el) {
        return handleSearchformSubmit(el);
    });
}

var initLocation = location.href;

jQuery(function() {
    init();
    if (document.addEventListener) {
        window.addEventListener('popstate', function(event) {
            refreshPage(window.location.href, false);
        });
    }
});
