/*!
 * newsletter.js
 * 
 * Handling of newsletter registration
 * 
 * @author: Karsten Beyer <karsten@webpard.de>
 */

function NewsletterRegister(name, email, form) {

	this.name = $(name);
	this.email = $(email);
	this.domForm = $(form);
}

NewsletterRegister.prototype.clearErrors = function() {
	this.name.removeClass('error');
	this.email.removeClass('error');
};

NewsletterRegister.prototype.validate = function() {
	
	this.clearErrors();
	var valid = true;
	if (this.name.val() == '' || this.name.val() == undefined) {
		this.name.addClass('error');
		valid = false;
	} 
	
	if (this.email.val() == '' || this.email.val() == undefined ) {
		this.email.addClass('error');
		valid = false;
	} else {
		if (this.email.val().indexOf('@') == -1) {
			this.email.addClass('error');
			valid = false;
		}
	}
	
	return valid;
	
	
};
NewsletterRegister.prototype.showSuccess = function() {
	
	this.domForm.find('.showform').hide();
	this.domForm.find('.submitted').show();
	
};
NewsletterRegister.prototype.handleResponse = function(data) {
	
	if (data.status == 'error') {
		
		if (data.errors.indexOf('email') != -1) {
			this.email.addClass('error');
		}
		
		if (data.errors.indexOf('name') != -1) {
			this.name.addClass('error');
		}
	} else {
		this.showSuccess();
	}
};

NewsletterRegister.prototype.send = function() {
	
	$.post('/ajax/newsletter/register' , {name: this.name.val(), email: this.email.val() } , $.proxy(this.handleResponse, this));
	
};

function getNewsletterCountdown() {

    var cookie = " " + document.cookie;
    var search = " nlCdown=";
    var setStr = null;
    var offset = 0;
    var end = 0;
    if (cookie.length > 0) {
        offset = cookie.indexOf(search);
        if (offset != -1) {
            offset += search.length;
            end = cookie.indexOf(";", offset)
            if (end == -1) {
                end = cookie.length;
            }
            setStr = unescape(cookie.substring(offset, end));
        }
    }
    return(setStr);

}

function setNewsletterCountdown(value) {
    document.cookie='nlCdown='+escape(value)+'; path=/; expires=Mon, 01-Jan-2030 00:00:00 GMT';
}

function showNewsletterIfNeed() {

    var count = 5; // number of pages before newsletter would be shown

    var nwCdn = getNewsletterCountdown();
    nwCdn = parseInt(nwCdn, 10);

    if (!isNaN(nwCdn) && nwCdn < (count)){
        if(nwCdn === 0) {
            setNewsletterCountdown(--nwCdn);
            $('.bg, #newsletter-register').fadeIn(200);
        } else if (nwCdn > 0) {
            setNewsletterCountdown(--nwCdn);
        }
    } else {
        setNewsletterCountdown((count - 1));
    }
}

$(document).ready(function() {
	
	$('.newsletter-register-link').click(function() {
		$('.bg, #newsletter-register').fadeIn(200);
        setNewsletterCountdown(-1);
	});
	
	$('#newsletter-register form').submit(function(e) {
		e.preventDefault();
		var register = new NewsletterRegister('#register-name','#register-email', '#newsletter-register');
		
		if (!register.validate()) {
			return false;
		} else {
			register.send();
		}
	    
		return false;
	});

    //showNewsletterIfNeed();
});

