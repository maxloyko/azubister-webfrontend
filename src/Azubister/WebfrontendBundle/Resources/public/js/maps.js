/**
 * Wrapper object for google maps.
 * 'location' is used as main entity to target azubister specific needs
 */
var Map = function() {

    var T = this;

    // default map settings which work nice for a map with sizes near 600pxx600px
    // to fit different map size changing zoom value should be enough
    var germanyMapSettings = {
        lat: 51.0,
        lng: 10.0,
        zoom: 5
    };

    var supportedLocationTypes = ['default', 'joboffer', 'company', 'city'];

    var _gMap;

    // Marker index to simplify search by id
    var markerIndex = {};
    // Marker index to search by company job id
    var markerJobIndex = {};

    // Actual markers on the map
    var markers = [];

    var currentInfoView = null;
    
    this.init = function(el, mapSettings) {
        // Google Maps library is included only on pages which need maps
        // so, 'google' namespace is available only there
        // and one of the possibilities to set proper prototype is this method
        AzInfoWindow.prototype = new google.maps.OverlayView();

        if (typeof(mapSettings) != 'undefined') {
            germanyMapSettings = mapSettings;
        }

        var mapOptions = {
            center: new google.maps.LatLng(germanyMapSettings.lat, germanyMapSettings.lng),
            zoom: germanyMapSettings.zoom,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            panControl: false,
            streetViewControl: false,
            mapTypeControl: false,
						scrollwheel: false
        };

        _gMap = new google.maps.Map(document.getElementById(el),
                mapOptions);
        google.maps.event.addListener(_gMap, 'click', function() {
            if (currentInfoView) {
                currentInfoView.remove();
                currentInfoView = null;
            }
        });
    };

    this.toInitialState = function() {
        _gMap.setCenter(new google.maps.LatLng(germanyMapSettings.lat, germanyMapSettings.lng));
        _gMap.setZoom(germanyMapSettings.zoom);
    }

    function validLocation(loc) {
        return true;
    }

    function getMarkerImage(type) {
        if (supportedLocationTypes.indexOf(type) == -1) {
            return null;
        }
        var image = new google.maps.MarkerImage('/images/' + type + '_marker.png',
          new google.maps.Size(21, 34),
          // The origin for this image is 0,0.
          new google.maps.Point(0,0),
          // The anchor for this image is the base of the flagpole at 0,32.
          new google.maps.Point(0, 34));
        return image;
    }

    function getMarkerShadow() {
        var image = new google.maps.MarkerImage('/images/marker_shadow.png',
          new google.maps.Size(39, 34),
          // The origin for this image is 0,0.
          new google.maps.Point(0,0),
          // The anchor for this image is the base of the flagpole at 0,32.
          new google.maps.Point(0, 34));
        return image;
    }

    function showMarkerInfo(marker) {
            var contentString = '';
            if (marker.contentCallback != undefined && marker.contentCallback != null) {
                contentString = marker.contentCallback(marker);
            } else if (marker.content != undefined && marker.content != null) {
                contentString = marker.content;
            }

            if (contentString) {
                marker.infoView = new AzInfoWindow(_gMap, marker, contentString);
                currentInfoView = marker.infoView;
            }
    }

    function updateIndexByJob(loc) {
        var locationId = loc.id;
        var hasJoboffers = loc.joboffers && loc.joboffers.length;
        if (hasJoboffers) {
            for (var jobofferIdx in loc.joboffers) {
                var joboffer = loc.joboffers[jobofferIdx];

                var hasJobs = joboffer.companyjobs && joboffer.companyjobs.length;

                if (hasJobs) for (var jobIdx in joboffer.companyjobs) {
                    var job = joboffer.companyjobs[jobIdx];
                    if (markerJobIndex[job.id] == undefined) {
                        markerJobIndex[job.id] = [locationId];
                    } else {
                        var jobIndexPart = markerJobIndex[job.id];
                        if (jobIndexPart.indexOf(locationId) == -1) {
                            jobIndexPart.push(locationId);
                            markerJobIndex[job.id] = jobIndexPart;
                        }
                    }
                }
            }
        }
        
    }

    this.addLocation = function(geoLocation, contentCallback) {

        if (validLocation(geoLocation)) {
            var marker = T.addMarker(new google.maps.LatLng(geoLocation.lat, geoLocation.lng), geoLocation.type);
            marker.id = geoLocation.id;
            marker.jobclasses = geoLocation.jobclasses;
            marker.degrees = geoLocation.degrees;
            marker.joboffers = geoLocation.joboffers;
            marker.locationName = geoLocation.name;
            if (geoLocation.content != undefined && geoLocation.content != null) {
                marker.content = geoLocation.content;
            }
            
            markerIndex[geoLocation.id] = marker;

            updateIndexByJob(geoLocation);

            marker.contentCallback = contentCallback;

            google.maps.event.addListener(marker, 'click', function() {
                if (currentInfoView) {
                    currentInfoView.remove();
                    currentInfoView = null;
                }
                if (marker.type !== undefined && (marker.type == 'joboffer' || marker.type == 'company')) {
                if (marker.infoView) {
                    marker.infoView.remove();
                    marker.infoView = null;
                    currentInfoView = null;
                } else {
                    showMarkerInfo(marker);
                }
                }
            });

            return marker;
        } else {
            return null;
        }
    }

    // Add a marker to the map and push to the array.
    this.addMarker = function(location, type) {
        var marker = new google.maps.Marker({
            position: location,
               map: _gMap
        });
        var image = getMarkerImage(type);
        if (image) {
            marker.setIcon(image);
            marker.setShadow(getMarkerShadow());
        }
        markers.push(marker);
        marker.type = type;
        return marker;
    }

    // Sets the map on all markers in the array.
    function setAllMap(map) {
        for (var i = 0; i < markers.length; i++) {
            if (map == null && markers[i] == T.cityMarker) {
                continue;
            }
            markers[i].setMap(map);
        }
    }

    // Removes the overlays from the map, but keeps them in the array.
    this.hideAll = function() {
        setAllMap(null);
    }

    // Shows any overlays currently in the array.
    this.showAll = function() {
        if (currentInfoView) {
            currentInfoView.remove();
            currentInfoView = null;
        }

        setAllMap(_gMap);
        _gMap.setCenter(new google.maps.LatLng(germanyMapSettings.lat, germanyMapSettings.lng));
        _gMap.setZoom(germanyMapSettings.zoom);
    }

    // Deletes all markers in the array by removing references to them.
    this.deleteAll = function() {
        T.hideAll();
        markers = [];
    }

    this.showOnlyLocation = function(id, showInfo) {
        var marker = markerIndex[id];
        T.hideAll();
        marker.setMap(_gMap);
        showMarkerInfo(marker);
    }

    this.showOnlyJob = function(jobId) {
        if (currentInfoView) {
            currentInfoView.remove();
            currentInfoView = null;
        }
        if (markerJobIndex[jobId] !== undefined) {
            this.filterLocations(markerJobIndex[jobId]);
        } else {
            this.hideAll();
        }
    }

    this.showOnlyJobClass = function(jobClass) {
    }

    this.applyFilters = function(filters) {
        var jobclass = filters.jobclass;
        var degree = filters.degree;
        for (var i = 0; i < markers.length; i++) {
            var marker = markers[i];
            var showMarker = true;
            if (jobclass) {
                if (marker.jobclasses.indexOf(jobclass) == -1) {
                    showMarker = false;
                }
            }
            if (degree) {
                if (marker.degrees.indexOf(degree) == -1) {
                    showMarker = false;
                }
            }
            if (showMarker) {
                markers[i].setMap(_gMap);
            } else {
                markers[i].setMap(null);
            }
        }
    }

    this.filterLocations = function(ids) {
        T.hideAll();
        var marker;
        for (id in ids)
            if (ids.hasOwnProperty(id)) {
                var mId = ids[id];
                marker = markerIndex[mId];
                marker.setMap(_gMap);
            }
    }

    this.search = function(address) {
        if (!address) {
            this.deleteCityMarker();
            _gMap.setCenter(new google.maps.LatLng(germanyMapSettings.lat, germanyMapSettings.lng));
            _gMap.setZoom(germanyMapSettings.zoom);

        }
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode( { 'address': address}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                //_gMap.fitBounds(results[0].geometry.bounds);

                var loc = {
                    lat: results[0].geometry.location.Ya,
                    lng: results[0].geometry.location.Za,
                    type: 'city',
                    content: address
                };
                T.deleteCityMarker();
                T.cityMarker = T.addLocation(loc);
                //this.addMarker(results[0].geometry.location);
            }
        });
    }

    this.deleteCityMarker = function() {
        if (this.cityMarker != undefined) {
            this.cityMarker.setMap(null);

            for (var i = 0; i < markers.length; i++) {
                if (markers[i] == T.cityMarker) {
                    markers.splice(i, 1);
                    continue;
                }
            }
            this.cityMarker = null;
        }
    }

}

function AzInfoWindow(map, marker, content) {

    this._map = map;
    this._marker = marker;
    this._content = content;

    this._div = null;

    this.onAdd = function() {
        var div = document.createElement('div');
        div.className = 'map_info_window';
        div.innerHTML = content; 
        this._div = div;
        var panes = this.getPanes();
        //panes.overlayMouseTarget.appendChild(div);
        panes.overlayImage.appendChild(div);
    }

    this.draw = function() {
        var overlayProjection = this.getProjection();
        var p = overlayProjection.fromLatLngToDivPixel(this._marker.getPosition());
        var width = this._div.offsetWidth;
        var height = this._div.offsetHeight;
        this._div.style.left = parseInt(p.x) - parseInt((width/2)) + parseInt(11) + 'px';
        this._div.style.top = p.y - height - 36 + 'px';
    }

    this.onRemove = function() {
        this._div.parentNode.removeChild(this._div);
        this._div = null;
    }

    this.remove = function() {
        this.setMap(null);
    }

    this.setMap(map);
}

