/*!
 * searchHandler.js
 * 
 * SearchHandler Class
 * 
 * @author: Karsten Beyer <karsten@webpard.de>
 */
var $ = jQuery;
function SearchHandler(handlerUrl) {
	this.reset();

	this.facets = new Object();
	this.filters = new Array();

	this.itemsPerLoad = 10;
	this.debugMode = false;
	this.handlerUrl = handlerUrl;

	this.domFilters = null;
	this.domSearchfield = null;
	this.domFacets = null;
	this.domResults = null;
	this.domClearAllFilters = null;

}

SearchHandler.prototype.reset = function() {

	this.searchterm = null;
	this.numResults = 0;
	this.numShown = 0;
	this.lastDocid = 0;
	this.lastScore = 0;
	this.firstLimit = 0;
	this.currentState = null;

	
	// handle facets
	
	  
	 

};

SearchHandler.prototype.setDebugMode = function(mode) {
	this.debugMode = mode;
};

SearchHandler.prototype.setDomFilters = function(domFilters) {
	this.domFilters = domFilters;
};

SearchHandler.prototype.setDomSearchfield = function(domSearchfield) {
	this.domSearchfield = domSearchfield;
};

SearchHandler.prototype.setDomFacets = function(domFacets) {
	this.domFacets = domFacets;
};

SearchHandler.prototype.setDomResults = function(domResults) {
	this.domResults = domResults;
};

SearchHandler.prototype.setDomClearAllFilters = function(domClearAllFilters) {
	this.domClearAllFilters = domClearAllFilters;
	this.domClearAllFilters.click($.proxy(this.handleFilterToggle, this));

};

SearchHandler.prototype.addFacet = function(field, label) {

	this.facets[field] = new SearchFacet(field, label);
	return this.facets[field];

};

SearchHandler.prototype.getFacet = function(field) {

	return this.facets[field];

};

SearchHandler.prototype.getFacetConfig = function() {
	var facetConfig = new Object();
	for (key in this.facets) {

		var facet = this.getFacet(key);

		facetConfig[facet.field] = new Object();
		facetConfig[facet.field].field = facet.field;
		facetConfig[facet.field].expanded = facet.expanded;
		facetConfig[facet.field].values = new Object();

		for (vkey  in facet.values) {
			facetConfig[facet.field].values[vkey] = new Object();
			facetConfig[facet.field].values[vkey].value = facet.values[vkey].value;
			facetConfig[facet.field].values[vkey].selected = facet.values[vkey].selected;
		}

	}
	return facetConfig;
};

SearchHandler.prototype.getFilterConfig = function() {
	var filterConfig = new Object();
	for (var index = 0; index < this.filters.length; index++)  {
		filter = this.filters[index];
		filterConfig[index] = new Object();
		filterConfig[index].field = filter.getField();
		filterConfig[index].value = filter.getValue();
	}

	return filterConfig;
};


SearchHandler.prototype.showAds = function() {
	var showAds = true;
	
	for (var index = 0; index < this.filters.length; index++) {
		if (this.filters[index].field == 'co') {
			showAds = false;
		}
	}
	
	return showAds;
};

SearchHandler.prototype.addFacetValue = function(field, value) {
	this.facets[field].addValue(value);
};

SearchHandler.prototype.setFacetExpanded = function(field, expanded) {
	this.getFacet(field).expanded = expanded;
};

SearchHandler.prototype.setFacetValueSelected = function(field, value, selected) {
	if (!this.facets[field].values[value]) {
		this.addFacetValue(field, value);
	}

	this.facets[field].values[value].selected = selected;

};

SearchHandler.prototype.setFacetValueCount = function(field, value, count) {
	if (!this.getFacet(field).values[value]) {
		this.addFacetValue(field, value);
	}

	this.getFacet(field).values[value].count = count;

	if (count > 0) {
		this.getFacet(field).empty = false;
	}
};

SearchHandler.prototype.debugDumpFacets = function() {
	if (this.debugMode) {
		console.log('start dump');
		console.log(this.facets);
		for (key in this.facets) {
			console.log(this.facets[key]);
		}
	}

};

SearchHandler.prototype.renderResultEntries = function(resultEntries, more) {

	var domResultsList = this.domResults.children('.resultsList');
	var domLoading = this.domResults.children('.loading');
	var html = '';
	if (more) {
		html += this.domResults.children('.resultsList').html();
	}

	for (var i=0; i < resultEntries.length; i++) {
		html += resultEntries[i];
		this.numShown++;
	}
	
	if (this.showAds()) {
		this.numShown--; // Remove ad from count
	}
	
	domResultsList.html(html);
	domLoading.hide();
	domResultsList.show();
	this.domResults.children('.loadingMore').hide();

	if (this.numResults > this.numShown && this.numResults > 0) {
		this.domResults.find('.view-all').show();
		this.domResults.find('.view-all').children('a').unbind();
		this.domResults.find('.view-all').children('a').click(
				$.proxy(this.handleMoreLink, this));

	} else {
		this.domResults.find('.view-all').hide();
	}

};

SearchHandler.prototype.processResultFacets = function(resultFacets) {

	for (field in resultFacets) {
		facet = resultFacets[field];

		this.getFacet(field).empty = (facet.empty == 'true');
		this.getFacet(field).expanded = (facet.expanded == 'true');

		for (value in facet.values) {
			// this.addFacetValue(field, value);
			this.setFacetValueCount(field, value, facet.values[value].count);
		}

	}

};

SearchHandler.prototype.handleFacetToggle = function(event) {

	var node = event.currentTarget;
		
	if ($(node).attr('data-locked') == 'true') {
		return;
	}

	var facet = this.getFacet($(node).attr('data-field'));
	
	$(node).attr('data-expanded', facet.toggleExpanded());
    $(node).toggleClass('open');

	if (this.debugMode == true) {
		console.log('toggle facet, new status:');
		console.log(facet);
	}
	this.updateHashBang();

};

// http://azubister-www.localhost/suche/#/!/e:f_type/e:f_job_types/f:f_job_types:2/fi:f_job:2/n:2

SearchHandler.prototype.handleFilterToggle = function(event) {

	var node = event.currentTarget;

	var type = $(node).attr('data-type');

	if (type == 'facet') {
		this.setFacetValueSelected($(node).attr('data-field'), $(node).attr(
				'data-value'), false);
		this.requestAndProcessSearchResults(false);
	} else if (type == 'filter') {

		this.filters.splice($(node).attr('data-id'), 1);
		this.requestAndProcessSearchResults(false);
	} else if (type == 'all') {

		this.filters = new Array();

		for (field  in this.facets) {
			facet = this.getFacet(field);
			for (value in facet.values) {
				facet.values[value].selected = false;
			}

		}

		this.requestAndProcessSearchResults(false);
	}

};

SearchHandler.prototype.handleFacetValueToggle = function(event) {

	var node = event.currentTarget;

	var field = $(node).attr('data-field');
	var value = $(node).attr('data-value');

	var status = $(node).is(':checked');

	this.setFacetValueSelected(field, value, status);

	this.requestAndProcessSearchResults(false);

};
SearchHandler.prototype.handleMoreLink = function(event) {

	this.requestAndProcessSearchResults(true);

};

SearchHandler.prototype.registerFacetEvents = function() {
	this.domFacets.find('h3').click(
			$.proxy(this.handleFacetToggle, this));
	this.domFacets.find('.toggleFacetValue').click(
			$.proxy(this.handleFacetValueToggle, this));

};

SearchHandler.prototype.renderFilters = function() {

	html = '';

	for ( field in this.facets) {
		html += this.getFacet(field).renderFilters();
	}

	for (var index=0; index < this.filters.length; index++) {
		filter = this.filters[index];
		html += '<li data-type="filter" data-id="' + index + '">'
				+ filter.getLabel() + '<i></i></li>';
	}
	if (html != '') {
		html += '<li data-type="all">Alle Filter zurücksetzen<i></i></li>';
	}
	this.domFilters.html(html);
	this.domFilters.find('li').click($.proxy(this.handleFilterToggle, this));

};

SearchHandler.prototype.renderResultFacets = function() {
	var domFacetList = this.domFacets.children('.facetList');

	//var domLoading = this.domFacets.children('.loading');
	var html = '';

	for (field in this.facets) {

		facet = this.getFacet(field);

		html += facet.render();

	}

	domFacetList.html(html);

	$('.search-filter h3').click(function() {

		var ul = $(this).next();

		if (ul.is(':visible')) {
			ul.slideUp();
		} else {
			ul.slideDown();
		}

		/*
		 * if ($(this).hasClass('active')) {
		 * $(this).removeClass('active').next().slideUp(); } else {
		 * $(this).addClass('active').next().slideDown(); }
		 */
	});

	this.registerFacetEvents();
	//domLoading.hide();
	domFacetList.show();

};

SearchHandler.prototype.processResultFilters = function(resultFilters) {
	if (resultFilters !== null) {
		for (var index=0; index < resultFilters.length; index++) {
			this.filters[index].setLabel(resultFilters[index]['label']);
	
		}
	}
};

SearchHandler.prototype.blockSearchChanges = function(newStatus) {
	if (newStatus) {
		this.domFacets.find('.toggleFacetValue').attr('disabled', true);
		this.domSearchfield.find('.searchform').attr('disabled', true);
		this.domFacets.find('.toggleFacet').attr('data-locked', true);

	} else {
		this.domFacets.find('.toggleFacetValue').removeAttr('disabled');
		this.domSearchfield.find('.searchform').attr('disabled', false);
		this.domFacets.find('.toggleFacet').attr('data-locked', false);
	}
};

SearchHandler.prototype.resetResultList = function() {
	//this.domResults.find('.resultsList').html('');
	this.numShown = 0;

//	this.domResults.find('.resultsList').hide();
	this.domResults.find('.loadMoreLink').hide();
	this.domResults.find('.loadingMore').hide();
	
	this.domResults.children('.loading').show();

};

SearchHandler.prototype.processSearchResultsCommonBefore = function(data) {
	this.numResults = data.numResults;
	
	this.lastDocid = data.lastDocid;
	this.lastScore = data.lastScore;

};

SearchHandler.prototype.updateSearchform = function() {
	this.domSearchfield.find('.searchform').val(this.searchterm);
};

SearchHandler.prototype.processSearchResultsFull = function(data) {
	if (this.debugMode) {
		console.log('process results full');
		console.log(data);
	}

	this.processSearchResultsCommonBefore(data);
	this.renderResultEntries(data.docs, false);
	this.processResultFacets(data.facets);
	this.processResultFilters(data.filters);
	this.renderResultFacets();
	this.renderFilters();
	this.updateSearchform();
	
	if (this.numResults == 0) {
		this.domResults.find('.noResults').show();
		this.domFacets.children('.facetList').html('');
		
		
	} else {
		this.domResults.find('.noResults').hide();
	}
	
	this.blockSearchChanges(false);
	this.processSearchResultsCommonAfter(data);
};

SearchHandler.prototype.processSearchResultsMore = function(data) {
	if (this.debugMode) {
		console.log('process results more');
		console.log(data);
	}

	this.processSearchResultsCommonBefore(data);
	this.renderResultEntries(data.docs, true);
	this.blockSearchChanges(false);
	this.processSearchResultsCommonAfter(data);
};

SearchHandler.prototype.processSearchResultsCommonAfter = function(data) {
	this.updateHashBang();
};

SearchHandler.prototype.updateHashBang = function() {

	
	this.lockHashChanges = true;

	var hash = '/!/';
	// this.searchterm = 'hello/wält';
	if (this.searchterm !== null && this.searchterm !== '*') {
		hash += 'q:' + encodeURIComponent(this.searchterm) + '/';
		;
	}

	// facets
	var hash_expanded = '';
        var hash_facet_values = '';	
	for (field in this.facets) {

		facet = this.getFacet(field);
		
		if (facet.expanded == true) {
			hash_expanded += 'e:' + field + '/';
		}

		for (value in facet.values) {
			if (facet.getValue(value).selected == true) {
				hash_facet_values += 'f:' +field
						+ ':' + value
						+ '/';
			}
		}

	}
        hash += hash_expanded;
	hash += hash_facet_values;
	// filter

	for (var index=0;index<this.filters.length;index++) {
		filter = this.filters[index];
		hash += 'fi:'
				+ filter.getField()
				+ ':'
				+ filter.getValue()
				+ '/';

	}
	// numshown


	if(this.numShown < this.itemsPerLoad) {
		hash += 'n:' + this.itemsPerLoad;
	} else {
		hash += 'n:' + this.numShown;
	}


	this.currentState = hash;

	location.hash = hash;

	this.lockHashChanges = false;

};

SearchHandler.prototype.addFilter = function(filter) {
	this.filters.push(filter);
};

SearchHandler.prototype.configureFromHashBang = function() {

	this.reset();

	var hash = location.hash;
	
    
    if (hash !== '') {
		for (field  in this.facets) {
			  this.setFacetExpanded(field, false);
		}
	
        var parts = (hash.length==0 || hash.indexOf('/') == -1)? []:hash.split('/');



for (var i = 0;i < parts.length; i++) {
			 var part = parts[i];
			
             var partparams = (part.length==0 || part.indexOf(':') == -1)? []:part.split(':');
	
			// query string
			if (partparams[0] == 'q') {
				this.searchterm = decodeURIComponent(partparams[1]);
			}
	
			// facet value set
			if (partparams[0] == 'f') {
				// check if field exists
				var field = partparams[1];
				var value = partparams[2];
	
				if (this.facets[field] == undefined) {
					// TODO error handling
				} else {
					this.setFacetValueSelected(field, value, true);
				}
			}
	
			
			// facet expanded
			if (partparams[0] == 'e') {
				// check if field exists
				var field = partparams[1];
	
				if (this.facets[field] == undefined) {
					// TODO error handling
				} else {
					this.setFacetExpanded(field, true);
				}
			}
	
			// filters
			if (partparams[0] == 'fi') {
				// check if field exists
				var field = partparams[1];
				var value = partparams[2];
	
				this.addFilter(new SearchFilter(field, value));
	
			}
	
			if (partparams[0] == 'n') {
				this.firstLimit = partparams[1];
			}
		}
	}
    this.domSearchfield.unbind();
	this.domSearchfield.submit(
			$.proxy(this.handleSearchformSubmit, this));

};

SearchHandler.prototype.handleSearchformSubmit = function(moreResultsOnly) {

	if (this.domSearchfield.find('.searchform').val() == this.searchterm) {
		return false;
	}
    this.numShown = 0;

	this.searchterm = this.domSearchfield.find('.searchform').val();
	//console.log(this.searchterm);
	this.requestAndProcessSearchResults(false);

	return false;
};

SearchHandler.prototype.requestAndProcessSearchResults = function(
		moreResultsOnly) {
	var searchConfig = new Object();
	if (this.searchterm !== null) {
		searchConfig.searchterm = this.searchterm;
	}
	

	if (this.firstLimit > 0) {
		searchConfig.limit = this.firstLimit;
		this.firstLimit = 0;
	//} else if (this.numShown > 0 && !moreResultsOnly) {
	//	searchConfig.limit = this.numShown;
	} else {
		searchConfig.limit = this.itemsPerLoad;
	}
     
     var types = this.getFacet('t').values;
     var selected_count = 0;
     for(key in types) {
        if(types[key].selected) selected_count++;
     }     

     if (typeof this.getFacet('t').values.joboffer != 'undefined' && selected_count == 1) {
         searchConfig.sort = 'id+desc';
     }     

	if (!moreResultsOnly) {

		this.resetResultList();
		searchConfig.offset = 0;
		searchConfig.lastDocid = 0;
		searchConfig.lastScore = 0;
		this.blockSearchChanges(true);
		$.post(this.handlerUrl, {
			searchConfig : searchConfig,
			facetConfig : this.getFacetConfig(),
			filterConfig : this.getFilterConfig()
		}, $.proxy(this.processSearchResultsFull, this));
	} else {
		this.blockSearchChanges(true);
		searchConfig.offset = this.numShown;
		searchConfig.lastDocid = this.lastDocid;
		searchConfig.lastScore = this.lastScore;
		this.domResults.find('.loadMoreLink').hide();
		this.domResults.children('.loadingMore').show();
		$.post(this.handlerUrl, {
			searchConfig : searchConfig,
			facetConfig : this.getFacetConfig(),
			filterConfig : this.getFilterConfig()
		}, $.proxy(this.processSearchResultsMore, this));
	}

};
