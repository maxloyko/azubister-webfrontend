/*!
 * searchFacet.js
 * 
 * SearchFacet Class
 * 
 * @author: Karsten Beyer <karsten@webpard.de>
 */
 function SearchFacet(field, label) {

	this.label = label;
	this.field = field;
	this.values = new Object();
	this.expanded = false;
	this.dictionary = new Object();
}

SearchFacet.prototype.addValue = function(value) {
	if (!this.values[value]) {
		this.values[value] = new SearchFacetValue(value, this.field, this.getDictionaryEntry(value));
	}
};

SearchFacet.prototype.getValue = function(value) {
	return this.values[value];
};

SearchFacet.prototype.addDictionaryEntry = function(key,value) {
	this.dictionary[key] = value;
};

SearchFacet.prototype.getDictionaryEntry = function(key) {
	return this.dictionary[key];
};
SearchFacet.prototype.toggleExpanded = function() {
	if (this.expanded) {
		this.expanded = false;
	} else {
		this.expanded = true;
	}
	
	return this.expanded;
		
};

SearchFacet.prototype.isEmpty = function() {

	var empty = true;
	for (key in  this.values) {
		if (this.values[key].count > 0) {
			empty = false;
			break;
		}
	}

	return empty;

};

SearchFacet.prototype.hasSelected = function() {

	var selected = false;
	for (key in  this.values) {
		if (this.values[key].selected == true) {
			selected = true;
			break;
		}
	}

	return selected;

};

SearchFacet.prototype.render = function() {
	
	if (this.isEmpty()) {
		return '';
	}

	
	var html = '';
	//html += '<li class="' + facetclass + '">';
	html += '<h3';
	
	html += ' data-field="' + this.field + '"';
	html += ' data-expanded="' + this.expanded + '"';


    var classes = '';

	if (this.hasSelected()) {
		classes += 'active';
	}
   
    if (this.expanded) {
        if (classes != '') {
            classes += ' ';
        } 
        classes += 'open';
    }

    if (classes != '') {
        html += ' class="' + classes + '"';
    }
	
    html += '>';
	html += this.label + '<i></i></h3>';
//	html += '<a href="javascript:void(0);" class="toggleFacet" data-field="' + this.field + '">Toggle</a>';
	
	
	html += '<ul';
	if (this.expanded == false) {
		html += ' style="display:none">';
	} else {
		html += '>';
	}
	for (value in  this.values)
	{
		
		facetvalue = this.values[value];
		if (facetvalue.count > 0) {
			html += facetvalue.render();
		}
	}
	html += '</ul>';
	//html += this.renderFacetValues(field);

//	html += '</li>';
	
	return html;
};


SearchFacet.prototype.renderFilters = function() {

	html = '';
	for (value  in this.values)
	{ 
		html += this.values[value].renderFilter();
	}
	return html;
};

/*
 * 
 * <h3>Objekttyp<i></i></h3>
				<ul>			
				  <li><input id="f1" type="checkbox"><label for="f1">Betriebe</label></li>
				  <li><input id="f2" type="checkbox"><label for="f2">Ausbildungsplätze</label></li>
				  <li><input id="f3" type="checkbox"><label for="f3">Ausbildungen</label></li>
				  <li><input id="f4" type="checkbox"><label for="f4">Themen</label></li>
				  <li><input id="f5" type="checkbox"><label for="f5">Artikel</label></li>
				  <li><input id="f6" type="checkbox"><label for="f6">Community</label></li>
				</ul>
 */





