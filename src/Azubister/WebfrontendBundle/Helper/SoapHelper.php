<?php

namespace Azubister\WebfrontendBundle\Helper;

    class SoapHelper
    {

        private $keys;
        private $webdirectory;
        private $companies = array();
        private $cities = array();

        public function __construct($env = '')
        {
            $this->keys = array('test_VA4qSsm6A82R8kYAY37BWAAC', 'facebook_VA4qSsm6A82R8kYAY37BWAAC', 'iphone_H8ryfJc9VEgK56544d9r');
            $this->webdirectory = new \Azubister\Webdirectory\Directory($env);
        }

        private function getCompany($id)
        {
            if (!array_key_exists($id, $this->companies)) {
                $this->companies[$id] = $this->webdirectory->getCompanyItem($id);
            }

            return $this->companies[$id];
        }

        private function getCity($id)
        {
            if (!array_key_exists($id, $this->cities)) {
                $this->cities[$id] = $this->webdirectory->getCityItem($id);
            }

            return $this->cities[$id];
        }

        private function getDistance($user_lat, $user_long, $location_lat, $location_long, $id = null)
        {

                $pi80 = M_PI / 180;
                $user_lat *= $pi80;
                $user_long *= $pi80;
                $location_lat *= $pi80;
                $location_long *= $pi80;
             
                $r = 6372.797; // mean radius of Earth in km
                $dlat = $location_lat - $user_lat;
                $dlng = $location_long - $user_long;
                $a = sin($dlat / 2) * sin($dlat / 2) + cos($user_lat) * cos($location_lat) * sin($dlng / 2) * sin($dlng / 2);
                $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

                $dist = $r * $c;

                return $dist;
        }

        private function getNearestLocation($long, $lat, $locations)
        {
            $nl = array(); 
            $nd = null;

            if ($long === 0 || $lat === 0) {
                return $locations[0];
            }

            foreach ($locations as $l) {

                if ($l->geo_latitude === 0 || $l->geo_longitude === 0 ) continue;

                $distance = $this->getDistance($lat, $long, $l->geo_latitude, $l->geo_longitude, $l->id);
                if(empty($nl) || $nd === null || $distance < $nd) {
                    $nl = $l;
                    $nd = $distance;
                }
            }

            if (empty($nl)) {
                return $locations[0];
            }

            return $nl;
        }

        private function getJoboffersData($offset, $count, $long, $lat, $max_distance, $sources, $exclude_jobs)
        {

            $joboffers = $this->webdirectory->getJoboffersCollectionRequest()
                ->setLimit(0)
                ->addFilter(array('actual' => 1))
                ->send()->getData();
            return array_slice($joboffers, $offset, $count);

        }

        private function getCompaniesData($offset, $count, $long, $lat, $hide_duplicate_companies)
        {

            $companies = $this->webdirectory->getCompaniesCollectionRequest()
                ->setLimit(0)
                ->addFilter(array('profile_active' => 1))
                ->addFilter(array('profile_visible' => 1))
                ->send()->getData();
            return array_slice($companies, $offset, $count);

        }

        public function  getCompanies($api_key, $offset, $count, $long, $lat, $hide_duplicate_companies)
        {

            try {
                //$this->checkApiKey($api_key);

                $lat = str_replace(',' , '.', $lat);
                $long = str_replace(',' , '.', $long);

                $companies_data = $this->getCompaniesData($offset, $count, $long, $lat, $hide_duplicate_companies);
                $companies = array();

                foreach($companies_data as $cd) {
                    
                    $company = array();

                    if ( $cd->logo !== NULL){

                        $logo = $this->webdirectory->getMediaItem($cd->logo);
                        $size = 'logo_list';

                        if (property_exists($logo->urls, $size)) {
                            $company['companylist_logo_small'] = $logo->urls->$size;
                            $company['logo_template'] = array(
                                'url_part' => 'http://www.azubister.net/logos/' . $cd->id % 10 . '/' . $cd->id . '_',
                                'file_ext' => '.png'
                            );
                        }
                    }

                    $company['company_name'] = $cd->name;
                    $company['company_url'] = $cd->url;
                    $company['companycity_company_id'] = $cd->id;
                    $company['jobs_count'] = count($cd->jobs);

                    if (empty($cd->locations)) {
                        $company['city_name'] = 'bundesweit';
                        $company['companycity_geo_latitude'] = '51.165691';
                        $company['companycity_geo_longitude'] = '10.451526';
                    } else {
                        switch(count($cd->locations)) {
                            case 1:
                                $city = $this->getCity($cd->locations[0]->city);
                                $company['city_name'] = $city->name;
                                $company['companycity_geo_latitude'] = $cd->locations[0]->geo_latitude;
                                $company['companycity_geo_longitude'] = $cd->locations[0]->geo_longitude;
                                break;
                            default:
                                $nl = $this->getNearestLocation($long, $lat, $cd->locations);
                                $city = $this->getCity($nl->city);
                                $company['city_name'] = $city->name;
                                $company['companycity_geo_latitude'] = $nl->geo_latitude;
                                $company['companycity_geo_longitude'] = $nl->geo_longitude;
                        }
                    }

                    $companies[] = $company;

                }
                return $companies;

            }catch(Exception $e) {
                throw new \SoapFault('Client', $e->getMessage() , 'server.php', '');
            }
        
            return array();
        }

        public function  getJoboffers_2($api_key, $offset, $count, $long, $lat, $max_distance, $sources, $exclude_jobs, $fields = array())
        {

            try {
                //$this->checkApiKey($api_key);

                $lat = str_replace(',' , '.', $lat);
                $long = str_replace(',' , '.', $long);

                $joboffers_data = $this->getJoboffersData($offset, $count, $long, $lat, $max_distance, $sources, $exclude_jobs);
                $joboffers = array();

                foreach($joboffers_data as $jd) {
                    
                    $joboffer = array();

                    foreach($fields as $field) {

                        switch($field) {

                            case 'companylist_logo_small':
                                $company = $this->getCompany($jd->company);
                                if($company->logo === NULL) { break; }
                                $logo = $this->webdirectory->getMediaItem($company->logo);
                                $size = 'logo_list';
                                if (property_exists($logo->urls, $size)) {
                                    $joboffer['companylist_logo_small'] = $logo->urls->$size;
                                }
                                break;
                            case 'company_name':
                                $company = $this->getCompany($jd->company);
                                $joboffer['company_name'] = $company->name;
                                break;
                            case 'city_name':

                                if (empty($jd->companylocations)) {
                                    $joboffer['city_name'] = 'bundesweit';
                                } else {
                                    switch(count($jd->companylocations)) {
                                        case 1:
                                            $joboffer['city_name'] = $jd->companylocations[0]->name;
                                            break;
                                        default:
                                            $nl = $this->getNearestLocation($long, $lat, $jd->companylocations);
                                            $joboffer['city_name'] = $nl->name;
                                    }
                                }
                                break;
                            case 'jobofferpool_url':
                                if (!empty($jd->companylocations) && count($jd->companylocations) > 1) {
                                    $nl = $this->getNearestLocation($long, $lat, $jd->companylocations);
                                    $jd->url .= '#city=' . $nl->id;
                                }
                                $joboffer['jobofferpool_url'] = $jd->url;
                                break;
                            case 'job_seo_title':
                                $joboffer['job_seo_title'] = $jd->title;
                                break;
                            case 'job_id':
                                $joboffer['job_id'] = count($jd->companyjobs) > 0 ? $jd->companyjobs[0]->job : 0;
                                break;
                            case 'logo_template':
                                $company = $this->getCompany($jd->company);
                                if($company->logo === NULL) { break; }

                                $joboffer['logo_template'] = array(
                                    'url_part' => 'http://www.azubister.net/logos/' . $jd->company % 10 . '/' . $jd->company . '_',
                                    'file_ext' => '.png'
                                );
                                break;
                            case 'jobofferpool_external_key':
                                $joboffer['jobofferpool_external_key'] = $jd->id;
                                break;
                        }
                    }

                    $joboffers[] = $joboffer;

                }
                return $joboffers;

            }catch(Exception $e) {
                throw new \SoapFault('Client', $e->getMessage() , 'server.php', '');
            }
        
            return array();
        }

        private function checkApiKey($api_key) {

            if (in_array($api_key, $this->keys)) {
                return true;
            } else {
                throw new \Exception("Invalid API key. " . $api_key);
            }

        }

}
?>
