<?php

namespace Azubister\WebfrontendBundle\Helper;

class TplDataHelper
{

    private $webdirectory;
    private $fallbackImage;

    public function __construct($webdirectory, $fallbackImage = 0)
    {
        $this->webdirectory = $webdirectory;
        $this->fallbackImage = $fallbackImage;
    }

    public function prepareTeaserEntryData($data)
    {
        
        $obj = $data->obj;

        if (empty($obj)) {
            return array(
                'title' => ''
            );
        }

        $view_data = array();
        switch ($data->type) {
            case 'joboffer':
                $company = $this->webdirectory->getCompanyItem($obj->company);
                if ($company) {
                    if (!empty($company->logo)) {
                        $view_data['image_id'] = $company->logo;
                    }
                    $view_data['footer'] = array(
                        array('label' => $company->name),
                    );

                    if (!empty($obj->companyjobs)) {
                        $j = array(); $counter = 1;
                        foreach ($obj->companyjobs as $job) {
                            if ($counter > 3) break;
                            $j[] = $job->name;
                            $counter++;
                        }
                        $label = implode(', ', $j);
                        if (count($obj->companyjobs) > 3) $label .= ' ('.(count($obj->companyjobs) - 3).' weitere)';
                        $view_data['footer'][] = array('label' => $label);
                    }
                    if (!empty($obj->companylocations)) {
                        $j = array(); $counter = 1;
                        foreach ($obj->companylocations as $job) {
                            if ($counter > 3) break;
                            $j[] = $job->name;
                            $counter++;
                        }
                        $label = implode(', ', $j);
                        if (count($obj->companylocations) > 3) $label .= ' ('.(count($obj->companylocations) - 3).' weitere)';
                        $view_data['footer'][] = array('label' => $label);
                    } else {
                        $view_data['footer'][] = array('label' => 'bundesweit');
                    }

                    $view_data['footer'][] = array('label' => 'Quelle: azubister');

                } 
                if (empty($view_data['image_id'])) {
                    $view_data['image_id'] = $this->fallbackImage;
                }
                $view_data['title'] = $obj->title;
	            //$view_data['description'] = $obj->description;
                $view_data['description'] = '';

            break;
        }

        $view_data['image_context'] = 'logo';
        $view_data['clickUrl'] = $data->clickUrl;
        $view_data['logUrl'] = $data->logUrl;

        return $view_data;
    }

}
?>
