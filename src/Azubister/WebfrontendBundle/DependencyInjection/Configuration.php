<?php

namespace Azubister\WebfrontendBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('azubister_webfrontend');
        $rootNode
            ->children()
                ->arrayNode('contactform')
                    ->children()
                        ->scalarNode('recipient_mail_to')->end()
                    ->end()
                ->end()
                ->arrayNode('imprint')
                    ->children()
                        ->scalarNode('logo')->end()
                    ->end()
                ->end()
                ->arrayNode('about')
                    ->children()
                        ->arrayNode('images')
                            ->useAttributeAsKey('name')
                            ->prototype('scalar')
                                ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('entitylists')
                    ->children()
                        ->arrayNode('topjobs')
//                            ->useAttributeAsKey('name')
                            ->prototype('scalar')
                                ->end()
                        ->end()
                    ->end()
                ->end()
				->arrayNode('adzones')
					->useAttributeAsKey('name')
					->prototype('scalar')
					->end()
				->end()
				->scalarNode('facebook_app_prefix')
				->end()
/*
				->scalarNode('google_maps_api_key')
				->end()
 */
                ->arrayNode('search')
                    ->children()
                        ->arrayNode('expanded')
                          //  ->useAttributeAsKey('name')
                            ->prototype('scalar')
                            ->end()
                        ->end()
                     ->end()
                   ->end();
        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
