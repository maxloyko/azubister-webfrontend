<?php

namespace Azubister\WebfrontendBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class AzubisterWebfrontendExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
//        var_dump('$config');
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

		$loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
		$loader->load('services.yml');
//        var_dump("$config");
        $container->setParameter('contact_form', $config['contactform']);
        $container->setParameter('imprint_logo', $config['imprint']);
        $container->setParameter('adzones', $config['adzones']);
        $container->setParameter('facebook_app_prefix', $config['facebook_app_prefix']);
        $container->setParameter('search_config', $config['search']);
        //$container->setParameter('google_maps_api_key', $config['google_maps_api_key']);
        $container->setParameter('top_jobs', $config['entitylists']);
        ;
    }
}
