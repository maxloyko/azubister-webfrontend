<?php
namespace Azubister\WebfrontendBundle\Twig;

class OpenXAdExtension extends \Twig_Extension
{
	public function getFunctions()
	{
		return array(
			'openxad' => new \Twig_Function_Method($this, 'getAdHtml'),
			'openx_banner' => new \Twig_Function_Method($this, 'getBannerHtml')
		);
	}

	public function getAdHtml($zoneID, $refresh = false)
	{
		$location = 'adserver.azubister.net/www/delivery';
		$params = (empty($refresh)) ? '' : "refresh=$refresh";
        $width = 300;
        $height = 250;

		switch ($zoneID) {
            /* Skyscraper */
			case '57':
			case '70':
				$width = 160;
				$height = 620;
				break;
            /* Mobile floater */
			case '58':
				$width = 320;
				$height = 50;
				break;
			case '59':
			case '64':
			case '65':
				$width = 300;
				$height = 250;
				break;
		}     

		$random = md5(rand(0, 999999999));
		$n = substr(md5(rand(0, 999999999)), 0, 6);
		$amp = ($params == '') ? '' : '&amp;';

        $code = '';
//        $code = '<div class="label_single banner-label"><span>Anzeige</span></div>';
		// <!--/* OpenX iFrame Tag v2.8.5 (Rich Media - Doubleclick) */-->
		$code .= '<iframe id="' . $n . '" name="' . $n . '" src="http://' . $location . '/afr.php?resize=1&amp;' . $params . '&amp;zoneid=' . $zoneID . '&amp;n=' . $n . '&amp;cb=%n&amp;ct0=%c" frameborder="0" scrolling="no" width=" ' . $width . '" height="' . $height . '" allowtransparency="true">';
		$code .= '<a href="http://' . $location . '/ck.php?n=' . $n . '&amp;cb=%n" target="_blank">';
		$code .= '<img src="http://' . $location . '/avw.php?zoneid=' . $zoneID . '&amp;cb=%n&amp;n=' . $n . '&amp;ct0=%c" border="0" alt="" /></a></iframe>';
		$code .= '<script type="text/javascript" src="http://' . $location . '/ag.php"></script>';

		return $code;
	}

    public function getBannerHtml($bannerID, $width=728, $height=90)
    {
        $location = 'adserver.azubister.net/www/delivery';

	   $random = md5(rand(0, 999999999));
	   $n = substr(md5(rand(0, 999999999)), 0, 6);

        $code = '';
        $code .= "<iframe id='$n' name='$n' src='http://$location/afr.php?what=bannerid:$bannerID&amp;cb=$random' frameborder='0' scrolling='no' width='$width' height='$height'>";
        $code .= "<a href='http://$location/ck.php?n=$n&amp;cb=%n' target='_blank'>";
        $code .= "<img src='http://$location/avw.php?what=bannerid:$bannerID&amp;cb=$random&amp;n=$n' border='0' alt='' /></a></iframe>";
	   $code .= '<script type="text/javascript" src="http://' . $location . '/ag.php"></script>';

	   return $code;
    }

	public function getName()
	{
		return 'azubister_openx';
	}
}
