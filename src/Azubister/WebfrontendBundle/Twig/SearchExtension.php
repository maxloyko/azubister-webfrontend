<?php
namespace Azubister\WebfrontendBundle\Twig;
class SearchExtension extends \Twig_Extension
{
    protected $webdirectory;

    /**
     * @return Azubister\Webdirectory\Directory
     */
    public function getWebdirectory()
    {
        return $this->webdirectory;
    }

    public function __construct($webdirectory)
    {
        $this->webdirectory = $webdirectory;
    }

    public function getFunctions()
    {
        return array(
                'searchlink' => new \Twig_Function_Method($this, 'buildLink2'),);
    }

    public function getName()
    {
        return 'azubister_search';
    }

    protected function buildFacetPart($facetConfig, &$dictionary)
    {

    }

    public function buildLink2($searchterm = null, $expand = null, $facets = null, $filters = null)
    {

        $searchParams = array();
        $dictionary = $this->getWebdirectory()->createSearchDictionary();
        // query
        if ($searchterm !== null) {

            $searchParams[] = 'q=' . urlencode(urlencode($searchterm));
        }

        // facets
        if ($facets !== null) {

            if (is_array($facets)) {
                foreach ($facets AS $facetKey => $facetValues) {
                    if (is_array($facetValues)) {
                        $facetPrefix = 'f[' . $dictionary->getFacetKey($facetKey) . ']';

                        $index = 0;
                        foreach ($facetValues AS $value) {
                            $encodedKey = urlencode($facetPrefix . '[' . $index . ']');
                            $searchParams[] = $encodedKey . '=' . $value;
                            $index++;
                        }
                    }
                }
            }

        }

        //filters
        if ($filters !== null) {
            if (is_array($filters)) {
                foreach ($filters AS $facetKey => $facetValue) {
                    $encodedFilterPrefix = urlencode('fi[' . $dictionary->getFacetKey($facetKey) . ']');
                    $searchParams[] = $encodedFilterPrefix . '=' . $facetValue;
                }
            }
        }

        $url = '/suche/';

        if (count($searchParams) > 0) {
            $url .= '?' . implode('&', $searchParams);
        }

        return $url;
    }

    /**
     * Builds url to search with preset parameters.
     * 
     * All parameters take key for the facets and filters. Avaiable values
     * can be found in config.yml of the webdirectory.
     * 
     * Searchterm: unencoded keyword
     * 
     * Expand: list of facets to expand
     * 
     * Facets: list of facets which each contain a list of selected values
     * 
     * Filters: List of facets with a value each. Difference is that filters will not be offered
     * in the facet navigation. Instead they are just used to filter the results.
     * 
     * @param string $searchterm
     * @param array $expand
     * @param array $facets
     * @param array $filters
     * @return string
     */
    public function buildLink($searchterm = null, $expand = null,
            $facets = null, $filters = null)
    {

        // {'facets': { 'jobgroups': [31,19]}, 'expand': ['types', 'jobgroups'], }
        $searchParams = array();
        $dictionary = $this->getWebdirectory()->createSearchDictionary();
        // query
        if ($searchterm !== null) {

            $searchParams[] = 'q:' . urlencode(urlencode($searchterm));
        }
        // exand
        if ($expand !== null) {
            if (is_array($expand)) {
                foreach ($expand AS $value) {
                    $searchParams[] = 'e:' . $dictionary->getFacetKey($value);
                }
            }
        }
        // facets
        if ($facets !== null) {

            if (is_array($facets)) {
                foreach ($facets AS $facetKey => $facetValues) {
                    if (is_array($facetValues)) {
                        $facetPrefix = 'f:'
                                . $dictionary->getFacetKey($facetKey) . ':';

                        foreach ($facetValues AS $value) {
                            $searchParams[] = $facetPrefix . $value;
                        }
                    }
                }
            }

        }

        //filters
        if ($filters !== null) {
            if (is_array($filters)) {
                foreach ($filters AS $facetKey => $facetValue) {

                     $searchParams[] = 'fi:' . $dictionary->getFacetKey($facetKey)
                            . ':' . $facetValue;
                }
            }
        }

        $searchParams[] = 'n:10';
        
        $url = '/suche/#/!/';

        if (count($searchParams) > 0) {
            $url .= implode('/', $searchParams);
        }

        return $url;
    }

    /**
     * Template functions
     */

    public function doShortcodes($text)
    {
        $pattern = '|\[link\s+[^\]]+]|i';
        $res = preg_match_all($pattern, $text, $matches);
        if (!empty($res)) {
            $m = $matches[0];
            foreach ($m as $shortcode_raw) {
                $shortcode_clean = preg_replace(
                        array('|^\[link\s+|i', '|\s*\]\s*$|i'), array('', ''),
                        $shortcode_raw);
                preg_match_all('#\S+="[\S*\s*\S*]*"#U', $shortcode_clean,
                        $matches);
                $attrs = $matches[0];
                $attrs_data = array();

                foreach ($attrs as $a) {
                    list($key, $value) = explode('=', $a);

                    $attrs_data[$key] = trim($value, '" ');
                }
                $text = str_replace($shortcode_raw,
                        $this->handleLinkShortcode($attrs_data), $text);
            }
        }
        return $text;
    }

    public function fixQuickInfo($string)
    {
        return str_replace(array('<p>', '</p>'), array('', ''), trim($string));
    }

    protected function handleLinkShortcode($attrs)
    {
        $html = '';
        if (empty($attrs['type'])) {
            $attrs['type'] = 'onpage';
        }

        $anchor = empty($attrs['anchor']) ? ''
                : ' name="' . $attrs['anchor'] . '"';
        $target = empty($attrs['target']) ? ''
                : ' target="' . $attrs['target'] . '"';

        switch ($attrs['type']) {
        case 'external':
            $label = empty($attrs['label']) ? $attrs['url'] : $attrs['label'];
            $html = '<a href="' . $attrs['url'] . '"' . $target . $anchor . '>'
                    . $label . '</a>';
            break;
        case 'onpage':
            if (!empty($anchor)) {
                $html = '<a' . $anchor . '></a>';
            }
            break;
        case 'job':
            $job = $this->webdirectory->getJobItem($attrs['id']);
            $label = empty($attrs['label']) ? $job->title : $attrs['label'];
            $html = '<a href="' . $job->url . '"' . $target . $anchor . '>'
                    . $label . '</a>';
            break;
        case 'company':
            $company = $this->webdirectory->getCompanyItem($attrs['id']);
            $label = empty($attrs['label']) ? $company->name : $attrs['label'];
            $html = '<a href="' . $company->url . '"' . $target . $anchor . '>'
                    . $label . '</a>';
            break;
        case 'joboffer':
            $joboffer = $this->webdirectory->getJobofferItem($attrs['id']);
            $label = empty($attrs['label']) ? $joboffer->title : $attrs['label'];
            $html = '<a href="' . $joboffer->url . '"' . $target . $anchor
                    . '>' . $label . '</a>';
            break;
        case 'post':
            $post = $this->webdirectory->getArticleItem($attrs['id']);
            $label = empty($attrs['label']) ? $post > title : $attrs['label'];
            $html = '<a href="' . $post->url . '"' . $target . $anchor . '>'
                    . $label . '</a>';
            break;
        case 'term':
            $term = $this->webdirectory->getTermItem($attrs['id']);
            $label = empty($attrs['label']) ? $term->name : $attrs['label'];
            $html = '<a href="' . $term->url . '"' . $target . $anchor . '>'
                    . $label . '</a>';
            break;
        default:
            break;
        }

        return $html;
    }

    public function articleExcerpt($article)
    {
        $excerpt = empty($article->excerpt) ? $article->title
                : $article->excerpt;
        $length = 90; //modify for desired width
        if (strlen($excerpt) <= $length) {
            $excerpt = $excerpt; //do nothing
        } else {
            $excerpt = substr($excerpt, 0,
                    strpos(wordwrap($excerpt, $length), "\n"));
            $excerpt .= '&nbsp;...';
        }
        return $excerpt;
    }

    public function magazineFeatureImage($image_id, $size)
    {
        $html = '';
        $image = $this->webdirectory->getMagazinMediaItem($image_id);
        if (!empty($image)) {
            if (!empty($image->sizes->$size)) {
                $url = $image->sizes->$size->url;
                $html = '<img src="' . $url . '" />';
            }
        }
        return $html;
    }

    public function topicExcerpt($article)
    {
        $excerpt = empty($article->excerpt) ? $article->name : $article
                ->excerpt;
        $length = 90; //modify for desired width
        if (strlen($excerpt) <= $length) {
            $excerpt = $excerpt; //do nothing
        } else {
            $excerpt = substr($excerpt, 0,
                    strpos(wordwrap($excerpt, $length), "\n"));
            $excerpt .= '&nbsp;...';
        }
        return $excerpt;
    }

    public function showJobofferSchoolDegrees($id, $title)
    {
        $offer = $this->webdirectory->getJobofferItemCitiesPreload($id);
        $schoolDegrees = array();

        if (!isset($offer->companyjobs)) {
            return '';
        }

        foreach ($offer->companyjobs as $companyjob) {

            if (!isset($companyjob->possible_school_degrees)) {
                continue;
            }

            foreach ($companyjob->possible_school_degrees as $schoolDegree) {
                $schoolDegrees[$schoolDegree->id] = $schoolDegree->title;
            }
        }

        if (empty($schoolDegrees)) {
            return '';
        }

        $html = "<h3 class='h3'>$title</h3>";

        foreach ($schoolDegrees as $schoolDegree) {
            $html .= "<p>$schoolDegree</p>";
        }

        return $html;
    }
}
