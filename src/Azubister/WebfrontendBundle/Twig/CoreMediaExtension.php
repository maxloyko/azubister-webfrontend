<?php
namespace Azubister\WebfrontendBundle\Twig;

class CoreMediaExtension extends \Twig_Extension
{
	protected $webdirectory;
	public function __construct($webdirectory)
	{
		$this->webdirectory = $webdirectory;
	}
	public function getFunctions()
	{
		return array(
			'gallery' =>  new \Twig_Function_Method($this, 'getGallery'),
			'core_media' => new \Twig_Function_Method($this, 'getCoreMedia'),
			'core_media_url' => new \Twig_Function_Method($this, 'getCoreMediaUrl'),
            'core_media_caption' => new \Twig_Function_Method($this, 'getCoreMediaCaption'),
		);
	}

	public function getGallery($id)
	{
		$gallery = $this->webdirectory->getGalleryItem($id);

		if (isset($gallery->medias) && !empty($gallery->medias))
		{
			$gallery->hasMedias = true;
			$gallery->showMore = count($gallery->medias) > 6;

			foreach ($gallery->medias as $key => $item)
			{
				$gallery->medias[$key] = $this->webdirectory->getMediaItem($item->id);
			}
		}
		else
		{
			$gallery->hasMedias = false;
		}
		return $gallery;
	}

	public function getCoreMedia($media_id, $size, $context = null, $attr = array())
	{
		if (!empty($context))
		{
			$size = $context.'_'.$size;
		}
		$media = $this->webdirectory->getMediaItem($media_id);

		$html = '';

		if (!empty($media))
		{
			if (isset($media->urls->$size))
			{
                if (empty($attr['title']) && isset($media->description)) {
				    $attr['title'] = htmlspecialchars($media->description);
                }
                if (empty($attr['alt']) && !(array_key_exists('alt', $attr) && $attr['alt'] === '')) {
				    $attr['alt'] = isset($media->description) ? htmlspecialchars($media->description) : '';
                }

                foreach ($attr as $k => $v) {
                    $attr[$k] = $k . '="' . $v . '"';
                }

				$html = '<img src="'.$media->urls->$size.'" '.implode(' ', $attr).'/>';


				if (!empty($media->description))
				{
					//$html .= '<p>'.$media->description.'</p>';
				}
			}
		}
		return $html;
	}

	public function getCoreMediaUrl($media_id, $size, $context = null)
	{
		$url = null;
		if (!empty($context))
		{
			$size = $context.'_'.$size;
		}
		$media = $this->webdirectory->getMediaItem($media_id);
		switch ($media->type)
		{
			case 'image':
				if (!empty($media))
				{
					if (isset($media->urls->$size))
					{
						$url = $media->urls->$size;
					}
				}
				break;
			case 'youtube':
				$url = 'http://www.youtube.com/embed/'.$media->reference.'?autoplay=1';
				break;
		}

		return $url;
	}

    public function getCoreMediaCaption($id, $before = '', $after = '')
    {
        $mediaItem = $this->webdirectory->getMediaItem($id);
        $html = '';
        if (!empty($mediaItem->description))
        {
            $html = $before.$mediaItem->description.$after;
        }
        return $html;
    }

	public function getName()
	{
		return 'azubister_core_media';
	}
    
}
