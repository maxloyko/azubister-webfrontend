<?php
namespace Azubister\WebfrontendBundle\Twig;

class ContentExtension extends \Twig_Extension
{
	protected $webdirectory;
	public function __construct($webdirectory)
	{
		$this->webdirectory = $webdirectory;
	}

	public function getFunctions()
	{
		return array(
            'object_url' => new \Twig_Function_Method($this, 'objectUrl'),
			'do_shortcodes' => new \Twig_Function_Method($this, 'doShortcodes'),
			'generate_url_friendly_string' => new \Twig_Function_Method($this, 'generateUrlFriendlyString'),
			'fix_quick_info' => new \Twig_Function_Method($this, 'fixQuickInfo'),
			'article_excerpt' => new \Twig_Function_Method($this, 'articleExcerpt'),
            'topic_excerpt' => new \Twig_Function_Method($this, 'topicExcerpt'),
			'magazine_feature_image' => new \Twig_Function_Method($this, 'magazineFeatureImage'),
			'show_joboffer_school_degrees' => new \Twig_Function_Method($this, 'showJobofferSchoolDegrees'),
			'show_joboffer_primary_city' => new \Twig_Function_Method($this, 'showJobofferPrimaryCity'),
            'company_badge' => new \Twig_Function_Method($this, 'companyBadge'),
            'company_mailto' => new \Twig_Function_Method($this, 'companyMailto'),
            'do_splittable' => new \Twig_Function_Method($this, 'do_splittable'),

		);
	}

	public function getName()
	{
		return 'azubister_content';
	}

	/**
	 * Template functions
	 */

    public function objectUrl($type, $id) {
        $html = '';

        $obj = null;
        switch ($type) {
            case 'company':
                $obj = $this->webdirectory->getCompanyItem($id);
                if ($obj) {
                    $html = '<a href="'.$obj->url.'">'.$obj->name.'</a>';
                }
                break;
            case 'job':
                $obj = $this->webdirectory->getJobItem($id);
                if ($obj) {
                    $html = '<a href="'.$obj->url.'">'.$obj->title.'</a>';
                }
                break;
            
        }
        return $html;
    }

	public function doShortcodes($text)
	{
		$pattern = '|\[link\s+[^\]]+]|i';
		$res = preg_match_all($pattern, $text, $matches);
		if (!empty($res))
		{
			$m = $matches[0];
			foreach ($m as $shortcode_raw)
			{
				$shortcode_clean = preg_replace(array('|^\[link\s+|i', '|\s*\]\s*$|i'), array('', ''), $shortcode_raw);
				preg_match_all('#\S+="[\S*\s*\S*]*"#U', $shortcode_clean, $matches);
				$attrs = $matches[0];
				$attrs_data = array();

				foreach ($attrs as $a)
				{
					list($key, $value) = explode('=', $a, 2);

					$attrs_data[$key] = trim($value, '" ');
				}
				$text = str_replace($shortcode_raw, $this->handleLinkShortcode($attrs_data), $text);
			}
		}
		return $text;
	}

    public function generateUrlFriendlyString($source) {
        $stringHelper = new \Azubister\Webdirectory\Helper\String();

        return $stringHelper->generateUrlFriendlyString($source);
    }

	public function fixQuickInfo($string)
	{
		return str_replace(array('<p>', '</p>'), array('', ''), trim($string));
	}

	protected function handleLinkShortcode($attrs)
	{
		$html = '';
		if (empty($attrs['type']))
		{
			$attrs['type'] = 'onpage';
		}

		$anchor = empty($attrs['anchor']) ? '' : ' name="'.$attrs['anchor'].'"';
		$target = empty($attrs['target']) ? '' : ' target="'.$attrs['target'].'"';

		switch ($attrs['type'])
		{
			case 'external':
				$label = empty($attrs['label']) ? $attrs['url'] : $attrs['label'];
				$html = '<a href="'.$attrs['url'].'"'.$target.$anchor.'>'.$label.'</a>';
				break;
			case 'onpage':
				if (!empty($anchor))
				{
					$html = '<a'.$anchor.'></a>';
				}
				break;
			case 'job':
                $job = null;
                try {
                    $job = $this->webdirectory->getJobItem($attrs['id']);
                } catch (\Exception $e) {
                }
                if ($job) {
                    $label = empty($attrs['label']) ? $job->title : $attrs['label'];
                    $html = '<a href="'.$job->url.'"'.$target.$anchor.'>'.$label.'</a>';
                } else {
                    $html = empty($attrs['label']) ? '' : $attrs['label'];
                }
				break;
			case 'company':
                $company = null;
                try {
                    $company = $this->webdirectory->getCompanyItem($attrs['id']);
                } catch (\Exception $e) {
                }
                if ($company) {
                    $label = empty($attrs['label']) ? $company->name : $attrs['label'];
                    $html = '<a href="'.$company->url.'"'.$target.$anchor.'>'.$label.'</a>';
                } else {
                    $html = empty($attrs['label']) ? '' : $attrs['label'];
                }
				break;
			case 'joboffer':
                $joboffer = null;
                try {
                    $joboffer = $this->webdirectory->getJobofferItem($attrs['id']);
                } catch (\Exception $e) {
                }
                if ($joboffer) {
                    $label = empty($attrs['label']) ? $joboffer->title : $attrs['label'];
                    $html = '<a href="'.$joboffer->url.'"'.$target.$anchor.'>'.$label.'</a>';
                } else {
                    $html = empty($attrs['label']) ? '' : $attrs['label'];
                }
				break;
			case 'post':
                $post = null;
                try {
                    $post = $this->webdirectory->getArticleItem($attrs['id']);
                } catch (\Exception $e) {
                }
                if ($post) {
                    $label = empty($attrs['label']) ? $post>title : $attrs['label'];
                    $html = '<a href="'.$post->url.'"'.$target.$anchor.'>'.$label.'</a>';
                } else {
                    $html = empty($attrs['label']) ? '' : $attrs['label'];
                }
				break;
			case 'term':
                $term = null;
                try {
				    $term = $this->webdirectory->getTermItem($attrs['id']);
                } catch (\Exception $e) {
                }
                if ($term) {
				    $label = empty($attrs['label']) ? $term->name : $attrs['label'];
				    $html = '<a href="'.$term->url.'"'.$target.$anchor.'>'.$label.'</a>';
                } else {
                    $html = empty($attrs['label']) ? '' : $attrs['label'];
                }
				break;
			default:
				
				break;
		}

		return $html;
	}

	public function articleExcerpt($article)
	{
		$excerpt = empty($article->excerpt) ? $article->title : $article->excerpt;
		$length = 90; //modify for desired width
		if (strlen($excerpt) <= $length) {
			   $excerpt = $excerpt; //do nothing
		} else {
			   $excerpt = substr($excerpt, 0, strpos(wordwrap($excerpt, $length), "\n"));
			   $excerpt .= '&nbsp;...';
		}
		return $excerpt;
	}

	public function magazineFeatureImage($image_id, $size)
	{
		$html = '';
		$image = $this->webdirectory->getMagazinMediaItem($image_id);
		if (!empty($image))
		{
			if (!empty($image->sizes->$size))
			{
				$url = $image->sizes->$size->url;
				$html = '<img src="'.$url.'" />';
			}
		}
		return $html;
	}

        public function topicExcerpt($article)
	{
        $excerpt = !empty($article->excerpt)
            ? $article->excerpt
            : ( !empty($article->short_title)
                ? $article->short_title
                : $article->name 
            );

		$length = 90; //modify for desired width
		if (strlen($excerpt) <= $length) {
			   $excerpt = $excerpt; //do nothing
		} else {
			   $excerpt = substr($excerpt, 0, strpos(wordwrap($excerpt, $length), "\n"));
			   $excerpt .= '&nbsp;...';
		}
		return $excerpt;
	}

	public function showJobofferSchoolDegrees($id, $title)
	{
          $offer = $this->webdirectory->getJobofferItem($id);
          $schoolDegrees = array();

          if (!isset($offer->companyjobs)) {
                return '';
          }

          foreach($offer->companyjobs as $companyjob) {
                
                if (!isset($companyjob->possible_school_degrees)) { continue; }
                foreach ($companyjob->possible_school_degrees as $schoolDegree) {
                    $schoolDegree = $this->webdirectory->getSchooldegreeItem($schoolDegree->id);
                    $schoolDegrees[$schoolDegree->id] = $schoolDegree->title;
                }
          }

          if(empty($schoolDegrees)) {
                return '';
          }

          $html = "<h3 class='h3'>$title</h3>";

          foreach ($schoolDegrees as $schoolDegree) {
                $html .= "<p>$schoolDegree</p>";
          }

		return $html;
	}

    public function showJobofferPrimaryCity($id) {

          $wd = $this->webdirectory;
          $offer = $wd->getJobofferItem($id);

          if (!isset($offer->companylocations)) {
                return 'bundesweit';
          }

          $primary = 0;
          $primary_val = -1;

          foreach($offer->companylocations as $key => $location) {
                $location->city = $wd->getCityItem($location->city);
                if ($location->city->population > $primary_val) {
                    $primary_val = $location->city->population;
                    $primary = $key;
                }
          }

            return $offer->companylocations[$primary]->name;
    }

    public function companyBadge($company) 
    {
        $badgeClass = '';

        if (!empty($company->profile_featured)) {
            $badgeClass = 'company-profile__top';
        }

        return $badgeClass;

        /*
        if (!empty($company->premium)) {
            $badgeClass = 'company-profile__top';
        }

        if (!empty($company->features) && !empty($company->features->gold_badge)) {
            $badgeClass = 'company-profile__top_gold';
        }

        return $badgeClass;
         */
    }

    public function companyMailto($contact)
    {
        $url = '';
        if (!empty($contact->email_address)) {
            $url = $contact->email_address;
        }
        $parts = array();
        foreach (array('subject' => 'email_subject', 'body' => 'email_body') as $key => $part) {
            if (!empty($contact->$part)) {
                $parts[] = $key.'='.rawurlencode($contact->$part);
            }
        }

        if (!empty($parts)) {
            $url .= '?'.implode('&', $parts);
        }
        return $url;
    }
    public function do_splittable($text)
    {
		if(!empty($text)) {
                    $template = '&lt;!-- splitter --&gt;';
                    return str_replace($template, '', $text);
                    $show_text = strstr($text, $template, true);
                    if (!$show_text) {
                        return $text;
                    }
                        
                    $hide_text = str_replace($template, '', strstr($text, $template));
                    $html = $show_text.'<a class="splittable_show" href="javascript:void(0)">mehr..</a><span class="splittable_content" style="display:none">'.$hide_text.'</span>';
		}
		return $html;
    }
}
