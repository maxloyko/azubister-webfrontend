<?php

namespace Azubister\WebfrontendBundle\Controller;

class JobofferController extends BaseController
{
	protected $activeSection = 'joboffers';

    public function landingAction()
    {
        $fallback_image_id = $this->container->getParameter('fallback_image');

        $sd_items = $this->getWebdirectory()->getSchoolDegreesCollectionRequest()->execute();

        $tc_data = $this->getWebdirectory()->getTopCompaniesCollection(0);
        $tc_keys = array_rand($tc_data, min(count($tc_data), 10));

        $top_companies = array();
        foreach ($tc_keys as $key) {
            $top_companies[] = $tc_data[$key];
        }
        $new_offers = $this->getLastJoboffers(8);
        $azubisterLogoPath = $this->container->getParameter('home_page_og_image');

        $companies = parent::getCompaniesFromEntityList('landingpage_joboffers_companies');
        $jobs = parent::getJobsFromEntityList('landingpage_joboffers_jobs');
        $job_groups = parent::getJobGroupsFromEntityList('landingpage_joboffers_jobgroups');
        $cities = parent::getCitiesFromEntityList('landingpage_joboffers_cities');
        
        $citiesFromMagazin = $this->getWebdirectory()->getCityTopicsCollection(0, 0);
        if(!empty($cities) && !empty($citiesFromMagazin)){
            foreach ($cities as $city){
                foreach ($citiesFromMagazin as $cityMag){
                    if((isset($cityMag->city_id) && $city->id == $cityMag->city_id) || strcmp($city->name, $cityMag->name) == 0){
                        $city->magazine_topic_id = $cityMag->term_id;
                    }
                }
            }
        }

        $view_data = array(
            'jobs' => $jobs,
            'new_offers' => $new_offers,
            'companies' => $companies,
            'jobgroups'     => $job_groups,
            'schooldegrees' => $sd_items,
            'cities' => $cities,
            'top_comps' => $top_companies,
            'fallback_image_id' => $fallback_image_id,
            '_title'       => 'Ausbildungsplätze und duale Studienangebote',
            '_meta'        => array(
                'description' => 'Freie Ausbildungsplätze und duale Studienangebote aus ganz Deutschland. Mit unserer komfortablen Suche ist dein Ausbildungsplatz nur noch einen Klick entfernt.',
                'properties'  => array(
                    'og:description' => 'Freie Ausbildungsplätze und duale Studienangebote aus ganz Deutschland. Mit unserer komfortablen Suche ist dein Ausbildungsplatz nur noch einen Klick entfernt.',
                    'og:type'        => 'website',
                ),
            ),
            '_canonical' => $this->generateUrl('joboffer_landing', array(), true),
        );
        if (!empty($azubisterLogoPath))
        {
            $view_data['_meta']['properties']['og:image'] = $azubisterLogoPath;
        }
        $view_data = array_merge($this->getCommonViewData(), $view_data);

        return $this->render('AzubisterWebfrontendBundle:Joboffer:landing.html.twig', $view_data);
    }

    public function detailAction($joboffer_title, $id)
    {

        $get = $this->getRequest()->query;
        $offer = $this->getJoboffer($id);
//var_dump($offer);
        if (empty($offer)
            || (isset($offer->valid_from) && new \Datetime($offer->valid_from) > new \Datetime('today'))
            || (isset($offer->valid_till) && new \Datetime($offer->valid_till) < new \Datetime('today'))) {
            throw $this->createNotFoundException('The offer does not exist');
        }

        $company = $this->getWebdirectory()->getCompanyItem($offer->company);

        if (empty($company) || $company->profile_active == false) {
            throw $this->createNotFoundException('There is no such company.');
        }

        $company_premium = $company->premium;
        $joboffers = $this->getOtherCompanyJoboffers($company->id, $id);

        $companylocations = isset($offer->companylocations) 
                            ? $this->reorder($offer->companylocations, 'locations') : array();

        $companyjobs = isset($offer->companyjobs)
                            ? $this->reorder($offer->companyjobs, 'jobs') : array();

        $facebook_app_prefix = $this->container->getParameter('facebook_app_prefix');

        $stringHelper = new \Azubister\Webdirectory\Helper\String();
        $canonical_link = $this->generateUrl(
            'joboffer_detail',
            array(
                'joboffer_title' => $stringHelper->generateUrlFriendlyString($offer->title),
                'id' => $offer->id
            )
        );

	   $view_data = array(
           '_title' => 'Ausbildungsplatz '.$offer->title,
           'fallback_image_id' => $this->container->getParameter('fallback_image'),
           '_meta' => array(
                'description' => $offer->description_ooc,
                'properties' => array(
                     'og:description' => $offer->description_ooc,
                     'og:type' => $facebook_app_prefix.':ausbildungsplatz'
                )
            ),
            'offer'             => get_object_vars($offer),
            'company'           => get_object_vars($company),
            'companylocations'  => $companylocations,
            'companyjobs'       => $companyjobs,
            'joboffers'         => $joboffers,
            'jsLocations'       => json_encode($companylocations),
            '_include_maps'     => true,
            '_canonical'        => $canonical_link,
            '_show_skyscrapper' => false,
            '_trackEvents'      => array($this->getJobofferTrackEvents($offer, $company)),
            '_trackExternalData'      => json_encode($this->getJobofferTrackEvents($offer, $company)),
            '_jobofferApplyTrackData' => json_encode($this->getJobofferApplyTrackData($offer, $company)),
	   );

        if (!empty($company->logo)) {
		$media = $this->webdirectory->getMediaItem($company->logo);
          $view_data['_meta']['properties']['og:image'] = $media->urls->logo_company_detail;
        }
        if (!empty($company->url)){
            $company_url = $this->generateUrl('company_detail', array('id' => $company->slug), true);
            $view_data['_meta']['properties'][$facebook_app_prefix.':betrieb'] = $company_url;
        }

        if (!empty($offer->training_start)) {
            $view_data['_meta']['properties'][$facebook_app_prefix.':ausbildungsbeginn'] = $offer->training_start;
        }

        if (!empty($offer->final_application_date)) {
            $view_data['_meta']['properties'][$facebook_app_prefix.':bewerbungsschluss'] = $offer->final_application_date;
        }

        $job_urls = array();
        foreach($companyjobs as $cj) {
            $j = $this->getWebdirectory()->getJobItem($cj->job);
            if ($j) {
                $job_urls[] = $this->generateUrl('job_detail', array('slug' => $j->slug), true);;
            }
        }
        $view_data['_meta']['properties'][$facebook_app_prefix.':berufe'] = $job_urls;

        if (!empty($companylocations)) {
            $cities = array();
            foreach($companylocations as $cl) {
                $cities[] = $cl->name;
            }
            $view_data['_meta']['properties'][$facebook_app_prefix.':standorte'] = $cities;
        }

	   $view_data = array_merge($this->getCommonViewData(), $view_data);

        return $this->render('AzubisterWebfrontendBundle:Joboffer:detail.html.twig', $view_data);
    }

    public function getJoboffer($id)
    {     
        $wd = $this->getWebdirectory();

        $joboffer = $wd->getJobofferItem($id);

        if(isset ($joboffer->companylocations)) {
            foreach ($joboffer->companylocations as $location) {
                $location->city = $wd->getCityItem($location->city);
            }     
        }     

        return $joboffer;
    }     

    public function getOtherCompanyJoboffers($company_id, $current_joboffer_id)
    {
        $request = $this->getWebdirectory()->getJoboffersCollectionRequest()
                        ->addFilter(array('company' => $company_id));
        $request->send();
        $joboffers = $request->getData();

        foreach ($joboffers as $k => $joboffer) {
            if ($joboffer->id == $current_joboffer_id) {
                unset($joboffers[$k]);
                break;
            }
        }

        return $joboffers;
    }

    protected function getJobofferTrackEvents($offer, $company)
    {
        $viewOfferEvent = array(
            'category' => 'Joboffer view',
            'action' => html_entity_decode($company->name),
            'label' => html_entity_decode($offer->title),
            'value' => 0,
            'nonInteraction' => 'true'
        );

        return $viewOfferEvent;
    }

    protected function getJobofferApplyTrackData($offer, $company)
    {

        $applyOfferEvent = array(
            'category' => 'Joboffer apply dialog opened',
            'action' => html_entity_decode($company->name),
            'label' => html_entity_decode($offer->title),
            'value' => 0,
            'nonInteraction' => false
        );

        return $applyOfferEvent;
    }
    
    private function reorder($items, $type = null)
    {

        $primaryKey = null;

        if (count($items)) {
            return $items;
        }

        switch ($type) {
            case 'locations':
                $primaryKey = $this->getBiggestCityLocationKey($items);
                break;
            case 'jobs':
                $primaryKey = $this->getMostPriorityJobKey($items);
                break;
            default:
                $primaryKey = 0;
        }

        return $this->arrayReorder($items, $primaryKey);
    }

    private function getBiggestCityLocationKey($locations)
    {
        $biggest = array('key' => -1, 'population' => -1);
        foreach ($locations as $key => $location) {
            if ($location->city->population > $biggest['population'])
            {       
                $biggest = array(
                    'key' => $key,
                    'population' => $location->city->population
                );
            }
        }

        return $biggest['key'];
    }

    private function getMostPriorityJobKey($jobs)
    {
        //TODO: get key of most priority job (currently we have no field "Priority in companyjob, so will ignore it for now")
        return 0;
    }

    private function arrayReorder($array, $key)
    {
        $toTop = array_splice($array, $key, 1);
        return array_merge($toTop, $array);
    }

    private function getCompaniesMostJoboffer()
    {
        $request = $this->getWebdirectory()->getCompaniesCollectionRequest()
                        ->addFilter(array('orderbymostjoboffer' => true))->setLimit(5);
        return $request->send()->getData();
    }

    private function getJobsMostJoboffer()
    {
        $request = $this->getWebdirectory()->getJobsCollectionRequest()
                        ->addFilter(array('orderbymostjoboffer' => true))->setLimit(7);
        return $request->send()->getData();
    }

    private function getLastJoboffers($count = 4)
    {
        $request = $this->getWebdirectory()->getJoboffersCollectionRequest();
        $request->setLimit($count);
        $request->setFilters(array('last' => 1));
        
        $offers = $request->execute();

        foreach ($offers as $k => $offer) {
            $offers[$k]->company = $this->getWebdirectory()->getCompanyItem($offer->company);
        }

        return $offers;
    }

}
