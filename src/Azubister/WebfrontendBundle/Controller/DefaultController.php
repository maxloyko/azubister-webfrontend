<?php

namespace Azubister\WebfrontendBundle\Controller;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\MinLength;
use Symfony\Component\Validator\Constraints\MaxLength;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Cookie;

class DefaultController extends BaseController
{

    public function indexAction()
    {
        $fallback_image_id = $this->container->getParameter('fallback_image');
        $slider_images = $this->container->getParameter('slider_image');
        $azubisterLogoPath = $this->container->getParameter('home_page_og_image');

        $tc_data = $this->getWebdirectory()->getTopCompaniesCollection(0);
        $tc_keys = array_rand($tc_data, min(count($tc_data), 9));

        $top_companies = array();
        foreach ($tc_keys as $key) {
            $top_companies[] = $tc_data[$key];
        }

        $view_data = array(
            'slider_images' => $slider_images,
            '_title' => 'Ausbildung, Duales Studium, Ausbildungsplätze',
            '_canonical' => $this->generateUrl('homepage', array(), true),
            'fallback_image_id' => $fallback_image_id,
            'top_companies' => $top_companies,
            'last_joboffers' => $this->getLastJoboffers(),
            '_meta' => array(
                'description' => 'Alle Infos zu Ausbildungen, dualen Studien und zu wichtigen Fragen der Ausbildung. Bewirb dich direkt auf freie Ausbildungsplätze und lerne Betriebe kennen.',
                'properties' => array(
                    'og:description' => 'Alle Infos zu Ausbildungen, dualen Studien und zu wichtigen Fragen der Ausbildung. Bewirb dich direkt auf freie Ausbildungsplätze und lerne Betriebe kennen.',
                    'og:type' => 'website',
                ))
            );
        if (!empty($azubisterLogoPath))
        {
            $view_data['_meta']['properties']['og:image'] = $azubisterLogoPath;
        }

        $view_data = array_merge($view_data, $this->getCommonViewData());
        return $this
            ->render('AzubisterWebfrontendBundle:Default:index.html.twig',
                $view_data);
    }

    public function legacyCityAction($id)
    {

        $city = $this->getWebdirectory()->getCityItem($id);
        
        if (!empty($city) && !empty($city->magazine_topic_id)) {
            $topic = $this->getWebdirectory()->getCityTopicItem($city->magazine_topic_id);
            if(!empty($topic) && !empty($topic->url) && $topic->city_id == $id) {
                return $this->redirect($topic->url, 301);
            }
        }

        throw $this->createNotFoundException('city404');
    }
   
    public function legacyCompanyAction($id)
    {
        $company = $this->getWebdirectory()->getCompanyItem($id);
        if (empty($company) || $company->profile_active == false) {
           throw $this->createNotFoundException('There is no such company.');
        }

        return $this->redirect($this->generateUrl('company_detail', array('id' => $company->slug)), 301);
    }

    public function legacyJobAction($id)
    {
        $job = $this->getWebdirectory()->getJobItem($id);

        if (empty($job))
        {
            throw $this->createNotFoundException('There is no such job.');
        }

        if ($job->active === true) {
            return $this->redirect($this->generateUrl('job_detail', array('slug' => $job->slug)), 301);
        } elseif (isset($job->redirect_job)) {
            $redirectedJob = $this->getWebdirectory()->getJobItem($job->redirect_job);
            return $this->redirect($this->generateUrl('job_detail', array('slug' => $redirectedJob->slug)), 301);
        } else {
            throw $this->createNotFoundException('Diese Ausildung wird nicht weiter gepflegt');
        }
    }

    public function legacyCompanyLogoAction($company_id, $size)
    {

        $company = $this->getWebdirectory()->getCompanyItem($company_id);

        if (empty($company)) {
                header("HTTP/1.0 404 Not Found");
                header("Status: 404 Not Found");
                exit;
        }

        $size = 'logo_' . $size;

        header( "HTTP/1.1 301 Moved Permanently" );
        if (empty($company->logo)) {
            $logo = $this->getWebdirectory()->getMediaItem($this->container->getParameter('fallback_image'));
        } else {
            $logo = $this->getWebdirectory()->getMediaItem($company->logo);
        }

        header("Location: ".$logo->urls->$size);
        header("Connection: close");
        exit;
    }

    public function legacyJobofferAction($id)
    {
        $offer = $this->getWebdirectory()->getJobofferItem($id);

        if (empty($offer)
            || (isset($offer->valid_from) && new \Datetime($offer->valid_from) > new \Datetime('today'))
            || (isset($offer->valid_till) && new \Datetime($offer->valid_till) < new \Datetime('today'))) {
            throw $this->createNotFoundException('The offer does not exist');
        }

        $stringHelper = new \Azubister\Webdirectory\Helper\String();

        $canonical_link = $this->generateUrl(
            'joboffer_detail',
            array(
                'joboffer_title' => $stringHelper->generateUrlFriendlyString($offer->title),
                'id' => $offer->id
            )
        );

        return $this->redirect($canonical_link, 301);
    }

    private function getLastJoboffers()
    {
        $request = $this->getWebdirectory()->getJoboffersCollectionRequest();
        $request->setLimit(4);
        $request->setFilters(array('last' => 1));
        
        $offers = $request->execute();

        foreach ($offers as $k => $offer) {
            $offers[$k]->company = $this->getWebdirectory()->getCompanyItem($offer->company);
        }

        return $offers;
    }

    public function ajaxAdvertisementAction()
    {

        $request = $this->get('request');
        $type = $request->get('type');
        $refresh = $request->get('refresh');

	   $_adzones = $this->container->getParameter('adzones');
        return $this->render('AzubisterWebfrontendBundle:Common:advertisement.html.twig', array('adzone' => $_adzones[$type], 'adzone_refresh' => $refresh));
    }

    public function ajaxBannerAction()
    {

        $request = $this->get('request');
        $bannerId = $request->get('banner_id');

        return $this->render('AzubisterWebfrontendBundle:Common:ad_banner.html.twig', array('banner' => $bannerId));
    }

    public function ajaxSponsoredAction()
    {

        $data = $this->getWebdirectory()->getTeaserAd(66);

        if (empty($data)) {return;}

        $tplDataHelper = $this->get('azubister.tpl_data.helper');
        $data = $tplDataHelper->prepareTeaserEntryData($data);

        return $this->render('AzubisterWebfrontendBundle:Search:ad_resultentry.html.twig', $data);
    }

    public function ajaxNewsletterRegisterAction()
    {

        $response = array();
        $response['status'] = 'ok';
        $response['errors'] = array();

        $request = $this->get('request');
        $name = $request->get('name');
        $email = $request->get('email');

        $emailConstraint = new Email();
        // all constraint "options" can be set this way
        $emailConstraint->message = 'invalid';

        // use the validator to validate the value
        $errorList = $this->get('validator')
                ->validateValue($email, $emailConstraint);

        if (count($errorList) > 0) {
            // this is *not* a valid email address
            $response['status'] = 'error';
            $response['errors'][] = 'email';

        }

        // validate name

        $minContraint = new MinLength(array('limit' => 5));
        $maxContraint = new MaxLength(array('limit' => 50));

        $errorList = $this->get('validator')
                ->validateValue($name, $minContraint);

        if (count($errorList) > 0) {
            // this is *not* a valid email address
            $response['status'] = 'error';
            $response['errors'][] = 'name';
        }

        $errorList = $this->get('validator')
                ->validateValue($name, $maxContraint);

        if (count($errorList) > 0) {
            // this is *not* a valid email address
            $response['status'] = 'error';
            $response['errors'][] = 'name';
        }

        $parts = explode(' ', $name, 2);
        
        if (sizeof($parts) == 2) {
            list($fname, $lname) = $parts;
            $response['data'] = array('fname' => $fname, 'lname' => $lname);
            
        } else if (sizeof($parts) == 1) {
            $fname = $parts[0];
            $lname = '';
            $response['data'] = array('fname' => $fname, 'lname' => '');
        } else {
            $response['status'] = 'error';
            $response['errors'][] = 'name';
        }
        
        if ($response['status'] == 'error') {
            return $this->sendJson($response);
        }
        
        $rm_conf = $this->container->getParameter('rapidmail');
        $api = $this->getWebdirectory()
                ->getRapidmailConnector($rm_conf['node_id'], $rm_conf['recipientlist_id'], $rm_conf['api_key']);

        try {
            $api
                    ->add_recipient($email,
                            array('firstname' => $fname, 'lastname' => $lname,
                                    'status' => 'new',
                                    'activationmail' => 'yes'));

        } catch (Exception $e) {
            $response['status'] = 'error';
            $response['errors'][] = 'api';
        }

        if ($response['status'] == 'error') {
            return $this->sendJson($response);
        }

        // send to rapidmail api
        return $this->sendJson($response);

    }

    public function soapAction() {

        $env = $this->container->getParameter('directory_environment');
        $server = new \SoapServer(NULL,
             array('uri' => $this->generateUrl('soap_test') ));
        $server->setClass('Azubister\WebfrontendBundle\Helper\SoapHelper', $env);

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml; charset=ISO-8859-1');

        $soap = new \Azubister\WebfrontendBundle\Helper\SoapHelper($env);

        ob_start();

        $server->handle();
        $return = ob_get_contents();

        if (!empty($return)) {
            ob_end_clean();
        }

        $response->setContent($return);

        return $response;
    }

    /**
     * Handle server side action for test users identification
     */
    public function testAccessAction()
    {
        $response = new RedirectResponse('/');

        $expires = new \DateTime(); 
        $expires = $expires->add(new \DateInterval("P1Y"));
        $response->headers->setCookie(new Cookie('testUser', '1', $expires, '/', null, false, false));
        return $response;
   }
}
