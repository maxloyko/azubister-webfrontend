<?php

namespace Azubister\WebfrontendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\MinLength;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;

class StaticController extends BaseController
{

    public function aboutUsAction(Request $request)
    {
        $team = $this->container->getParameter('aboutus_images');
        $azubisterLogoPath = $this->container->getParameter('home_page_og_image');

        foreach ($team as $m => $member)
        {
            $team[$m]['media_item'] = $this->getWebdirectory()->getMediaItem($member['media']);
            if (empty($member['fb_url'])) {
                $team[$m]['fb_url'] = 'https://www.facebook.com/webpard';
            }
            if (empty($member['xing_url'])) {
                $team[$m]['xing_url'] = 'https://www.xing.com/companies/webpardug';
            }
        }

        $view_data = array(
                        'team'     => $team,
                        '_title' => 'Über uns',
                        '_canonical' => $this->generateUrl('about_us', array(), true),
			'_meta' => array(
                                'robots' => 'noindex, follow',
				'description' => 'azubister stellt sich vor: Wir sind ein junges, motiviertes 
                                        Team aus Digital Natives und Ausbildungsexperten. Entdecke hier die Köpfe
                                        hinter azubister.',
				'properties' => array(
					'og:description' => 'azubister stellt sich vor: Wir sind ein junges, motiviertes
                                                    Team aus Digital Natives und Ausbildungsexperten. Entdecke hier die Köpfe
                                                    hinter azubister.',
					'og:type' => 'website',
				),
                            ),
        );
        if (!empty($azubisterLogoPath))
        {
            $view_data['_meta']['properties']['og:image'] = $azubisterLogoPath;
        }
        $view_data = array_merge($this->getCommonViewData(), $view_data);
        return $this->render('AzubisterWebfrontendBundle:Default:aboutUs.html.twig', $view_data);
    }

    public function partnerAction(Request $request)
    {
        $partners = $this->getWebdirectory()->getPartnersCollectionRequest()
            ->setLimit(0)->setOffset(0)->execute();

        $azubisterLogoPath = $this->container->getParameter('home_page_og_image');

        $view_data = array(
            'partners' => $partners,
            '_title' => 'Partner',
            '_canonical' => $this->generateUrl('partner', array(), true),
            '_meta' => array(
                'robots' => 'noindex, follow',
                'description' => 'azubister verfügt über ein starkes Netzwerk an Partnern sowie
                Kooperationen aus den Bereichen Recruiting, Ausbildung, Jobbörsen
                und Internet.',
                'properties' => array(
                    'og:description' => 'azubister verfügt über ein starkes Netzwerk an Partnern sowie
                    Kooperationen aus den Bereichen Recruiting, Ausbildung, Jobbörsen
                    und Internet.',
                    'og:type' => 'website',
                ),
            ),
        );
        if (!empty($azubisterLogoPath))
        {
            $view_data['_meta']['properties']['og:image'] = $azubisterLogoPath;
        }
        $view_data = array_merge($this->getCommonViewData(), $view_data);

        return $this->render('AzubisterWebfrontendBundle:Default:partner.html.twig', $view_data);
    }

    public function imprintAction()
    {
        $imprintconfig = $this->container->getParameter('imprint_logo');

        if (isset($imprintconfig))
        {
            $logo_im = $this->getWebdirectory()->getMediaItem($imprintconfig);
        }

        $azubisterLogoPath = $this->container->getParameter('home_page_og_image');

        $view_data = array(
                        'logo_image' => $logo_im,
                        '_title' => 'Impressum',
                '_canonical' => '',
			'_meta' => array(
                                'robots' => 'noindex, follow',
				'description' => 'Alle Infos zu Ausbildungen, dualen Studien und zu wichtigen Fragen der
                                    Ausbildung. Bewirb dich direkt auf freie Ausbildungsplätze und lerne Betriebe kennen.',
				'properties' => array(
					'og:description' => 'Alle Infos zu Ausbildungen, dualen Studien und zu wichtigen Fragen der
                                            Ausbildung. Bewirb dich direkt auf freie Ausbildungsplätze und lerne Betriebe kennen.',
                                        'og:type' => 'website',
				),
                            ),
        );
        if (!empty($azubisterLogoPath))
        {
            $view_data['_meta']['properties']['og:image'] = $azubisterLogoPath;
        }
        $view_data = array_merge($this->getCommonViewData(), $view_data);

        return $this->render('AzubisterWebfrontendBundle:Default:imprint.html.twig', $view_data);
    }

    public function contactAction(Request $request, $thanks)
    {
        $contactconfig=$this->container->getParameter('contact_form');
        $azubisterLogoPath = $this->container->getParameter('home_page_og_image');

        $collectionConstraint = new Collection(array(
            'name' => new MinLength(array('message' => 'Mindestens 5 Zeichen.', 'limit' => 5 )),
            'email' => new Email(array('message' => 'Ungültige E-Mail Adresse.')),
            'message' => new NotBlank(array('message' => 'Fill the field')),
            ));
        //swiftmailer settings should be specified in config.yml - swiftmailer section
        $mailer = $this->get('mailer');

        $form = $this->createFormBuilder(null, array(
                    'validation_constraint' => $collectionConstraint,
                    ))
            ->add('name', 'text', array(
                                    'label' => 'Name',
                                    'required' => TRUE,
                                    'attr' => array('class' => 'form-txt',
                                                    'oninput' => 'check(this)',
                                                    'onfocus' => 'check(this)',
                                                    'onmouseover' => 'check(this)',
                                                    ),
					)
                    )
            ->add('email', 'email', array(
                                    'label' => 'E-Mail-Adresse',
                                    'required' => TRUE,
                                    'attr' => array('class' => 'form-txt',
                                                    'oninput' => 'check(this)',
                                                    'onfocus' => 'check(this)',
                                                    'onmouseover' => 'check(this)',
                                                    ),
					)
                    )
            ->add('message', 'textarea', array(
                                    'label' => 'Nachricht',
                                    'required' => TRUE,
                                    'attr' => array('class' => 'form-txt',
                                                    'oninput' => 'check(this)',
                                                    'onfocus' => 'check(this)',
                                                    'onmouseover' => 'check(this)',
                                                    ),
                                        )
                    )
            ->getForm();

        if ($request->getMethod() == 'POST')
        {
            $form->bindRequest($request);
            // data is an array with "name", "email", and "message" keys
            $data = $form->getData();
            if ($form->isValid())
            {
                $message = \Swift_Message::newInstance()
                    ->setSubject('Hello Email')
                    ->setFrom($data['email'])
                    ->setTo($contactconfig['recipient_mail_to'])
                    ->setBody(
                            'Name: '.$data['name']."\n".
                            'E-mail: '.$data['email']."\n".
                            'Message: '."\n\n".$data['message']."\n"
                            )
                    ;
                $mailer->send($message);

                return $this->redirect($this->generateUrl('thanks',array('thanks' => 1)));
            }
        }
        if ($thanks == 0)
        {
            $canonical_link = "";
        }
        else
        {
            $canonical_link = $this->generateUrl('contact', array(), true);
        }
        $view_data = array(
                        'form'   => $form->createView(),
                        'thanks' => $thanks,
                        '_title' => 'Kontakt',
                        '_canonical' => $canonical_link,
                        '_meta'  => array(
                            'robots'      => 'noindex, follow',
                            'description' => 'Wir stehen zu allen Fragen rund um Ausbildung, Duales Studium 
                                oder Ausbildungsrecruiting jederzeit zur Verfügung. Wir freuen uns auf jede 
                                Nachricht.',
                            'properties'  => array(
                                'og:description' => 'Wir stehen zu allen Fragen rund um Ausbildung, Duales Studium 
                                    oder Ausbildungsrecruiting jederzeit zur Verfügung. Wir freuen uns auf jede 
                                    Nachricht.',
                                'og:type'        => 'website',
                            ),
                        ),
        );
        if (!empty($azubisterLogoPath))
        {
            $view_data['_meta']['properties']['og:image'] = $azubisterLogoPath;
        }
        $view_data = array_merge($this->getCommonViewData(), $view_data);
        
        return $this->render('AzubisterWebfrontendBundle:Default:contact.html.twig', $view_data );
    }

}
