<?php

namespace Azubister\WebfrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller
{
	protected $webdirectory;
	protected $activeSection = '';

	/**
	 * @return \Azubister\Webdirectory\Directory
	 */
	public function getWebdirectory()
	{
		if ($this->webdirectory === null)
		{
            $env = $this->container->getParameter('directory_environment');
			$this->webdirectory = new \Azubister\Webdirectory\Directory($env);
		}

		return $this->webdirectory;
	}

	protected function getCommonViewData()
	{
		$wd = $this->getWebdirectory();

		$data = array(
			'_title' => 'Ausbildung und Ausbildungsplätze finden',
			'main_header' => $wd->getMainHeader($this->activeSection),
			'main_footer' => $wd->getMainFooter(),
			'_adzones' => $this->container->getParameter('adzones'),
			'html_prefixes' => 'og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# '.$this->container->getParameter('facebook_app_prefix').' http://ogp.me/ns/fb/'.$this->container->getParameter('facebook_app_prefix').'#',
            //'_google_maps_api_key' => $this->container->getParameter('google_maps_api_key'),
            '_include_maps' => false,
            '_socialbar' => true,
            '_show_skyscrapper' => true,
            '_test_user' => $this->isTestUser(),
		);

		return $data;
	}

    protected function isTestUser()
    {
        $request = $this->get('request');
        $cookies = $request->cookies;
        $testUser = $cookies->get('testUser');
        return !empty($testUser);
    }
	
	protected function sendJson($response) {
	    return new Response(json_encode($response), 200, array('Content-Type'=>'application/json'));
	}

        protected function getCompaniesFromEntityList($slug){
            $companies_list = $this->getWebdirectory()->getEntityListItem($slug);
            $companies = array();
            if(isset($companies_list) && !empty($companies_list->items)){
                foreach ($companies_list->items as $items){
                    foreach ($items as $val){
                        if( isset($val)){
                            $company = $this->getWebdirectory()->getCompanyItem($val->id);
                            $company->name = htmlspecialchars_decode($company->name);
                            $companies[] = $company;
                        }
                    }
                }
            }
            return $companies;
        }

        protected function getJobsFromEntityList($slug){
            $jobs_list = $this->getWebdirectory()->getEntityListItem($slug);
            $jobs = array();
            if(isset($jobs_list) && !empty($jobs_list->items)){
                foreach ($jobs_list->items as $items){
                    foreach ($items as $val){
                        if( isset($val)){
                            $job = $this->getWebdirectory()->getJobItem($val->id);
                            $job->title = htmlspecialchars_decode($job->title);
                            $jobs[] = $job;
                        }
                    }
                }
            }
            return $jobs;
        }

        protected function getJobGroupsFromEntityList($slug){
            $job_groups_list = $this->getWebdirectory()->getEntityListItem($slug);
            $job_groups = array();
            if(isset($job_groups_list) && !empty($job_groups_list->items)){
                foreach ($job_groups_list->items as $items){
                    foreach ($items as $val){
                        if( isset($val)){
                            $job_group = $this->getWebdirectory()->getJobGroupItem($val->id);
                            $job_group->name = htmlspecialchars_decode($job_group->name);
                            $job_groups[] = $job_group;
                        }
                    }
                }
            }
            return $job_groups;
        }

        protected function getCitiesFromEntityList($slug){
            $cities_list = $this->getWebdirectory()->getEntityListItem($slug);
            $cities = array();
            if(isset($cities_list) && !empty($cities_list->items)){
                foreach ($cities_list->items as $items){
                    foreach ($items as $val){
                        if( isset($val)){
                            $city = $this->getWebdirectory()->getCityItem($val->id);
                            $city->name = htmlspecialchars_decode($city->name);
                            $cities[] = $city;
                        }
                    }
                }
            }
            return $cities;
        }
}
