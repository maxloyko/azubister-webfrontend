<?php
namespace Azubister\WebfrontendBundle\Controller;

use Guzzle\Http\Client;
use \Azubister\Webdirectory\Connector\Solr AS SolrConnector;
use Symfony\Component\HttpFoundation\Response;

class JobController extends BaseController {

    protected $activeSection = 'jobs';

    public function detailAction($slug)
    {
        $fallback_image_id = $this->container->getParameter('fallback_image');
        $job = $this->getWebdirectory()->getJobItem($slug);

        if (empty($job)) {
            throw $this->createNotFoundException('There is no such job.');
        }
        if ($job->active != true) {
            if (isset($job->redirect_job)) {
                $redirectedJob = $this->getWebdirectory()->getJobItem($job->redirect_job);
                return $this->redirect($this->generateUrl('job_detail', array('slug' => $redirectedJob->slug)), 301);
            } else {
                throw $this->createNotFoundException('Diese Ausildung wird nicht weiter gepflegt');
            }
        }

        $facebook_app_prefix = $this->container->getParameter('facebook_app_prefix');
        // array of jobdroup names
        $jobGroups           = array();
        if (isset($job->main_job_group))
        {
            $jobGroups[0] = $this->getWebdirectory()->getJobGroupItem($job->main_job_group);
            $jobGroups[0] = $jobGroups[0]->name;
        }

        if (isset($job->other_job_groups))
        {
            foreach ($job->other_job_groups as $value)
            {
                $jobGroup      = $this->getWebdirectory()->getJobGroupItem($value->id);
                $jobGroups[]   = $jobGroup->name;
                if(isset($job->main_job_group) && $value->id == $job->main_job_group )
                { 
                    array_pop($jobGroups);
                }
            }
        }
        // array of schooldegree names
        $schoolDegrees = array();
        if (!empty($job->recommended_school_degrees)) 
            foreach ($job->recommended_school_degrees as $value) {
                $schoolDegree    = $this->getWebdirectory()->getSchoolDegreeItem($value->id);
                $schoolDegrees[] = $schoolDegree->title;
            }

        //jobgroup topics
        $jg_mag       = '';
        if (isset($job->main_job_group)) {
            $jg_items_mag = $this->getWebdirectory()->getJobgroupTopicsCollection(0, 0);
            if (!empty($jg_items_mag)) {
                foreach ($jg_items_mag as $item) {
                    if (!empty($item->job_group_id) && $job->main_job_group == $item->job_group_id) {
                        $item->name = htmlspecialchars_decode($item->name);
                        $jg_mag = $item;
                        break;
                    }
                }
            }
        }

        $view_data = array(
            'job'      => $job,
            'articles' => $this->getWebdirectory()->getArticlesForJob($job->id),
            'jg_mag'   => $jg_mag,
            'fallback_image_id' => $fallback_image_id,
            '_canonical'   => $this->generateUrl('job_detail', array('slug' => $job->slug), true),
            '_title'   => 'Ausbildung ' . $job->title,
            '_meta'    => array(
                'description' => $job->out_of_context_description,
                'properties'  => array(
                    'og:description'                           => $job->out_of_context_description,
                    'og:type'                                  => $facebook_app_prefix . ':ausbildung',
                    $facebook_app_prefix . ':berufsgruppen'    => $jobGroups,
                    $facebook_app_prefix . ':schulabschluesse' => $schoolDegrees,
                ),
            )
        );

        $ogFeatureImage = '';
        if (isset($job->feature_image))
        {
            $jobFeatureImage = $this->getWebdirectory()->getMediaItem($job->feature_image);
            $ogFeatureImage  = $jobFeatureImage->urls->reference;
        } else
        {
            $azubisterLogoPath     = $this->container->getParameter('home_page_og_image');
            if(!is_null($azubisterLogoPath)){
                $ogFeatureImage  = $azubisterLogoPath;
            }
        }

        if (!empty($ogFeatureImage))
        {
            $view_data['_meta']['properties']['og:image'] = $ogFeatureImage;
        }

        $replaceImage = $this->container->getParameter('fallback_image');
        if (isset($replaceImage))
        {
            $view_data['replaceImage'] = $replaceImage;
        }

        if (isset($job->duration))
        {
            $view_data['_meta']['properties'][$facebook_app_prefix . ':ausbildungsdauer'] = $job->duration . ' Monate';
        }

        if (isset($job->salary_from) || isset($job->salary_to))
        {
            $payment = '';
            if (isset($job->salary_from))
            {
                $payment = $job->salary_from . ' €';
            }
            if (isset($job->salary_from) && isset($job->salary_to))
            {
                $payment .= ' - ';
            }
            if (isset($job->salary_to))
            {
                $payment .= $job->salary_to . ' €';
            }
            $view_data['_meta']['properties'][$facebook_app_prefix . ':verguetung'] = $payment;
        }

        $view_data = array_merge($this->getCommonViewData(), $view_data);

        return $this->render('AzubisterWebfrontendBundle:Job:detail.html.twig', $view_data);
    }

    public function landingAction()
    {
        $topJobsConfig = $this->container->getParameter('top_jobs');

        $jg_items = $this->getWebdirectory()->getJobGroupsCollectionRequest()
            ->setLimit(20)->execute();
        
        foreach ($jg_items as $item)
        {
            $item->name = htmlspecialchars_decode($item->name);
        }

        $jg_items_mag = $this->getWebdirectory()->getJobgroupTopicsCollection(null, 0);
        
        foreach ($jg_items_mag as $item)
        {
            $item->name = htmlspecialchars_decode($item->name);
        }

        $jt_items = $this->getWebdirectory()->getJobTypesCollectionRequest()->execute();

        $el_items = array();
        foreach ($topJobsConfig as $value)
        {
            foreach ($value as $val)
            {
                if( isset($val))
                {
                    $el_items[] = $this->getWebdirectory()->getEntityListItem($val);
                }
            }
        }

        $i = 0;
        if (isset($el_items))
        {
            foreach ($el_items as $el_item)
            {
                foreach ($el_item->items as $value)
                {
                    $jobs = array();
                    foreach ($value as $val)
                    {
                        $jobs[] = $this->getWebdirectory()->getJobItem($val->id);
                    }
                    $el_item->items = $jobs;
                }
                $el_items[$i] = $el_item;
                $i++;
            }
        }

        $sd_items       = $this->getWebdirectory()->getSchoolDegreesCollectionRequest()->execute();
        $topic_items    = $this->getWebdirectory()->getTopicsCollection();
        $canonical_link = $this->generateUrl('job_landing', array(), true);

        $azubisterLogoPath = $this->container->getParameter('home_page_og_image');
        
        $view_data = array(
            'jg_items'     => $jg_items,
            'jg_items_mag' => $jg_items_mag,
            'jt_items'     => $jt_items,
            'el_items'     => $el_items,
            'sd_items'     => $sd_items,
            'topics'       => $topic_items,
            '_title'       => 'Finde deine Wunschausbildung',
            '_canonical'   => $canonical_link,
            '_meta'        => array(
                'description' => 'Ausbildung oder ein duales Studium? Wir präsentieren Dir die
                                        wichtigsten Ausbildungen mit allen wichtigen Infos. Deine Wunschausbildung
                                        ist nur noch einen Klick entfernt.',
                'properties'  => array(
                    'og:description' => 'Ausbildung oder ein duales Studium? Wir präsentieren Dir die
                                            wichtigsten Ausbildungen mit allen wichtigen Infos. Deine Wunschausbildung ist nur noch einen Klick entfernt.',
                    'og:type'        => 'website',
                ),
            )
        );
        if (!empty($azubisterLogoPath))
        {
            $view_data['_meta']['properties']['og:image'] = $azubisterLogoPath;
        }
        $view_data = array_merge($this->getCommonViewData(), $view_data);

        return $this->render('AzubisterWebfrontendBundle:Job:landing.html.twig', $view_data);
	}
}
