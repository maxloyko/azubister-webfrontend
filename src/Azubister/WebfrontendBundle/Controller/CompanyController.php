<?php

namespace Azubister\WebfrontendBundle\Controller;

class CompanyController extends BaseController
{
	protected $activeSection = 'companies';

	public function landingAction()
    {
        $fallback_image_id = $this->container->getParameter('fallback_image');
        $zone_id = $this->container->getParameter('adzones');
        $zone_id = $zone_id['companies_landing'];
        
        $tc_data = $this->getWebdirectory()->getTopCompaniesCollection(0);
        $tc_keys = array_rand($tc_data, min(count($tc_data), 10));

        $top_companies = array();
        foreach ($tc_keys as $key) {
            $top_companies[] = $tc_data[$key];
        }

        $url_og_images = array();
        if (isset($top_companies) && !empty($top_companies)) {
            foreach ($top_companies as $company)
            {
                if(isset($company->logo)){
                    $media = $this->getWebdirectory()->getMediaItem($company->logo);
                    if (!empty($media)) {
                        $url_og_images[] = $media->urls->reference;
                    }
                }
            }
        } 
        
        $video_data = $this->getRandomVideoData();
        $video = $this->getWebdirectory()->getMediaItem($video_data['video_id']);
        $random_video_company = $this->getWebdirectory()->getCompanyItem($video_data['company_id']);
        $new_companies = $this->getLastCompanies(8);
        
        $companies = parent::getCompaniesFromEntityList('landingpage_joboffers_companies');
        $jobs = parent::getJobsFromEntityList('landingpage_companies_jobs');
        $cities = parent::getCitiesFromEntityList('landingpage_companies_cities');

        $citiesFromMagazin = $this->getWebdirectory()->getCityTopicsCollection(0, 0);
        if(!empty($cities) && !empty($citiesFromMagazin)){
            foreach ($cities as $city){
                foreach ($citiesFromMagazin as $cityMag){
                    if((isset($cityMag->city_id) && $city->id == $cityMag->city_id) || strcmp($city->name, $cityMag->name) == 0){
                        $city->magazine_topic_id = $cityMag->term_id;
                    }
                }
            }
        }

        $view_data = array(
            'companies' => $companies,
            'cities' => $cities,
            'top_companies' =>$top_companies,
            'new_companies' => $new_companies,
            'jobs' => $jobs,
            'video' => $video,
            'random_video_company' => $random_video_company,
            'zone_id'=> $zone_id,
            'fallback_image_id' => $fallback_image_id,
            '_title' => 'Top Ausbildungsbetriebe stellen sich vor',
            '_canonical' => $this->generateUrl('company_landing', array(), true),
            '_meta' => array(
                'description' => 'Der richtige Ausbildungsbetrieb ist mit entscheidend für eine erfolgreiche Ausbildung. Lerne Top Ausbildungsbetriebe jetzt direkt auf azubister kennen',
                'properties' => array(
                    'og:description' => 'Der richtige Ausbildungsbetrieb ist mit entscheidend für eine erfolgreiche Ausbildung. Lerne Top Ausbildungsbetriebe jetzt direkt auf azubister kennen.',
                    'og:type' => 'website',
                    'og:image'=> $url_og_images,
                )),
        );
        $view_data = array_merge($this->getCommonViewData(), $view_data);

        return $this->render('AzubisterWebfrontendBundle:Company:landing.html.twig', $view_data);
    }
        
        public function ajaxLandingAction()
        {
                $request = $this->get('request');
        }

    public function detailAction($id)
    {
        $company = $this->getWebdirectory()->getCompanyItem($id);

        if (empty($company) || $company->profile_active == false) {
            throw $this->createNotFoundException('There is no such company.');
        }
        $fallback_image_id = $this->container->getParameter('fallback_image');
        $locations = $this->getAugmentCompanyLocations($company);

        $company->has_locations = !empty($locations);

        $premium_class = '';
        if (!empty($company->profile_featured)) {
            $premium_class = ' premium';
        }

        if ($company->premium != true || empty($company->features) || empty($company->features->show_media)) {
            if (!empty($company->media_gallery)) unset($company->media_gallery);
        }

        $company->locations_teaser = empty($company->content) ? '' : (empty($company->content->locations_teaser) ? '' : strip_tags($company->content->locations_teaser));
        $company->locations_teaser_add = empty($company->content) ? '' : (empty($company->content->locations_teaser_add) ? '' : strip_tags($company->content->locations_teaser_add));

        if (!empty($company->companyindustry) && is_numeric($company->companyindustry)) {
            $industry = $this->getWebdirectory()->getCompanyIndustryItem($company->companyindustry);
            if ($industry) {
                /* set industry with another name to not break functionality at other places */
                $company->industry = $industry;
            }
        } else {
                $company->industry = false; 
        }

        $joboffers = array();
        $company->more_joboffers = false;
        if (!empty($company->joboffers)) {
            $counter = 0;
            foreach ($company->joboffers as $j) {
                if ($counter >= 3) {
                    $company->more_joboffers = true;
                    break;
                }
                $item = $this->getWebdirectory()->getJobofferItem($j->id);
                if ($item) {
                    $notValid = (isset($item->valid_from) && new \Datetime($item->valid_from) > new \Datetime('today')) || (isset($item->valid_till) && new \Datetime($item->valid_till) < new \Datetime('today'));

                    if (!$notValid) {
                        $joboffers[] = $item;
                        $counter++;
                    }
                }
            }
        }

        $view_data = array(
            'company' => $company,
            'joboffers' => $joboffers,
            'jsLocations' => json_encode($locations),
            'jobs' => $this->getAugmentedCompanyJobs($company),
            'jsJobs' => json_encode($this->getAugmentedCompanyJobs($company)),
            'articles'=> $this->getWebdirectory()->getArticlesForCompany($company->id),
            //'mixed_articles' => $this->getMixedArticles($company),
            'premium_class' => $premium_class,
            'fallback_image_id' => $fallback_image_id,
            '_title' => 'Ausbildung '.$company->name,
            '_include_maps' => $company->has_locations,
            '_meta' => $this->getCompanyMeta($company),
            '_show_skyscrapper' => (!$company->premium && $company->show_ads),
            '_show_contactbox' => ($company->premium && !empty($company->contactdata) && !empty($company->contactdata->contact_field)),
            '_canonical' => $this->generateUrl('company_detail', array('id' => $company->slug), true),
            '_trackEvents' => array($this->getCompanyTrackEvents($company)),
            '_trackExternalData' => json_encode($this->getCompanyTrackEvents($company)),
        );

        $view_data = array_merge($this->getCommonViewData(), $view_data);

        return $this->render('AzubisterWebfrontendBundle:Company:detail.html.twig', $view_data);
    }

	public function staticPageAction($company_slug, $static_slug)
    {
        $page = $this->getWebdirectory()->getCompanyStaticpageItem($static_slug);
        $company = $this->getWebdirectory()->getCompanyItem($company_slug);
        $show_staticpage = $company->premium && !empty($company->features) && $company->features->static_pages && $company->profile_active;
        if ( !$show_staticpage || empty($page) )
        {
     
            throw $this->createNotFoundException('There is no such page.');
        }
        $fallback_image_id = $this->container->getParameter('fallback_image');
        $other_staticpages = array();
        foreach ($company->staticpages as $staticpage) {
            if ($staticpage->slug == $static_slug || $staticpage->id == $page->id || $staticpage->featured_on_profile == false)
                continue;
            $other_staticpages[] = $staticpage;
        }
        if (!empty($company->logo)) {
            $company_logo = $this->getWebdirectory()->getMediaItem($company->logo);
            $link_image = $company_logo->urls->logo_company_detail;
        }
        

        if (!empty($page->feature_image)) {
            $media = $this->webdirectory->getMediaItem($page->feature_image);
            $url_og_image = $media->urls->reference;
        } else {
            if (!empty($link_image))
            {
            $url_og_image = $link_image;
            }
            else
            {
                $fallback_image = $this->getWebdirectory()->getMediaItem($fallback_image_id);
                $url_og_image = $fallback_image->urls->logo_company_detail;
            }
        }
        
        $view_data = array(
            'page' => $page,
            'company' => $company,
            '_title' => $page->title,
            '_canonical' => $this->generateUrl('company_static_page', array('company_slug' => $company_slug, 'static_slug' => $static_slug), true),
            'other_staticpages' => $other_staticpages,
            'fallback_image_id' => $fallback_image_id,
            '_show_skyscrapper' => (!$company->premium && $company->show_ads),
            '_show_contactbox' => ($company->premium && !empty($company->contactdata) && !empty($company->contactdata->contact_field)),
            '_meta' => array(
                'description' => $page->description_out_of_context,
                'properties' => array(
                    'og:description' => $page->description_out_of_context,
                    'og:type' => 'website',
                    'og:image'=> $url_og_image
                )),
            '_trackEvents' => array($this->getCompanyStaticPageTrackEvents($company, $page)),
            '_trackExternalData' => json_encode($this->getCompanyStaticPageTrackEvents($company, $page)),
        );
        $view_data = array_merge($this->getCommonViewData(), $view_data);

        return $this->render('AzubisterWebfrontendBundle:Company:static_page.html.twig', $view_data);
       
    }

        private function getStaticpagesCollectionFilteredByCompanyLanding()                         
       {                                                                                                
                $request = $this->getWebdirectory()->getStaticpagesCollectionRequest()                                      
                              ->addFilter(array('companylanding' => true));                                
                $request->send();                                                                        
                $staticpages = $request->getData();                                                              
                   foreach ($staticpages as $staticpage)
                   {
                       $staticpage->company = $this->getWebdirectory()->getCompanyItem($staticpage->company);

                   }
                return $staticpages;
         }
         private function getCompanyJobs()
         {
                $request = $this->getWebdirectory()->getJobsCollectionRequest()                                      
                              ->addFilter(array('orderbycompanyusage' => true));
                return $request->send()->getData();
         }
         private  function getRandomVideoData()
         {
                $videos = $this->container->getParameter('company_landing_videos');
                $count = count($videos) - 1;
                $key = rand(0, $count);
                $video = $videos[$key];
                
                return $video;
         }

    /**
     * Add type property to locations to be used on Google map
     *
     * @param object $company
     * @return array $locations
     */
    private function getAugmentCompanyLocations($company)
    {
        $locations = array();
        if (empty($company->locations)) {
            return $locations;
        }
        $locations = $company->locations;
        // Add 'company' type indicate that location belongs to a company
        foreach ($locations as $key => $loc) {
            $loc->type = 'default';
            $loc->jobclasses = array();
            $loc->degrees = array();
            $locations[$key] = $loc;
        }

        // For those locations which are used in joboffers
        // type should be changed to 'joboffer'
        if (!empty($company->joboffers)) {
            foreach ($company->joboffers as $joboffer) {
                $joboffer = $this->getWebdirectory()->getJobofferItem($joboffer->id);
                if (!empty($joboffer->companylocations)) {
                    foreach ($joboffer->companylocations as $cLocation) {
                        $id = $cLocation->id;

                        foreach ($locations as $key => $loc) {
                            if ($loc->id == $id) {
                                $loc->type = 'default';


                                $joboffer->jobIds = array();
                                foreach ($joboffer->companyjobs as $cJob) {
                                    $joboffer->jobIds[] =  $cJob->id;
                                    if (isset($cJob->job_type) && is_object($cJob->job_type)) {
                                        if ($cJob->job_type->id == 1) {
                                            // dualesStudium
                                            if (!in_array('dualesstudium', $loc->jobclasses)) {
                                                $loc->jobclasses[] = 'dualesstudium';
                                            }
                                        } else {
                                            if (!in_array('nondualesstudium', $loc->jobclasses)) {
                                                $loc->jobclasses[] = 'nondualesstudium';
                                            }
                                        }
                                    }
                                    if (!empty($cJob->possible_school_degrees)) {
                                        foreach ($cJob->possible_school_degrees as $degree) {
                                            if (is_object($degree)) {
                                                if ($degree->id == 1) {
                                                    // hauptschule
                                                    if (!in_array('hauptschule', $loc->degrees)) {
                                                        $loc->degrees[] = 'hauptschule';
                                                    }
                                                } else {
                                                    if (!in_array('realschule', $loc->degrees)) {
                                                        $loc->degrees[] = 'realschule';
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (!isset($loc->joboffers)) {
                                    $loc->joboffers = array();
                                }

                                $loc->joboffers[] = $joboffer;
                                $locations[$key] = $loc;
                            }
                        }

                    }
                }
            }
        }

        return $locations;
    }
    /**
     * Build array of jobs suitable to be used in template
     *
     * @param object $company
     * @return array $jobs
     */
    private function getAugmentedCompanyJobs($company)
    {
        $jobs = array(
            'dualesStudium' => array(),
            'nonDualesStudium' => array(),
        );
        if (empty($company->jobs)) {
            return $jobs;
        }

        foreach ($company->jobs as $job) {

            // duales Studium (id = 1) type jobs need separation
            if (isset($job->job_type) && is_object($job->job_type)) {
                $job->dualesStudium = $job->job_type->id == 1;
            } else {
                $job->dualesStudium = false;
            }
            unset($job->job_type);

            if (!empty($job->possible_school_degrees)) {
                $degree_titles = array();
                foreach ($job->possible_school_degrees as $key => $degree) {
                    $degree = $this->getWebdirectory()->getSchoolDegreeItem($degree->id);
                    $degree_titles[] = $degree->title;
                }
                $job->school_degrees_string = implode(', ', $degree_titles);
                unset($job->possible_school_degrees);
            }


            if (empty($job->name)) {
                $j = $this->getWebdirectory()->getJobItem($job->job);
                $job->name = $j->title;
            }

            $jobs[$job->id] = $job;
        }

        if (!empty($company->joboffers)) {
            foreach ($company->joboffers as $joboffer_simple) {
                $joboffer = $this->getWebdirectory()->getJobofferItem($joboffer_simple->id);
                $notValid = empty($joboffer) || (isset($joboffer->valid_from) && new \Datetime($joboffer->valid_from) > new \Datetime('today')) || (isset($joboffer->valid_till) && new \Datetime($joboffer->valid_till) < new \Datetime('today'));
                if ($notValid) {
                    continue;
                }
                
                if (!empty($joboffer->companyjobs)) {
                    foreach ($joboffer->companyjobs as $cJob) {
                        $job = $jobs[$cJob->id];
                        if (empty($job)) {
                            continue;
                        }

                        if (!isset($job->joboffers)) {
                            $job->joboffers = array();
                        }

                        $job->joboffers[$joboffer->id] = $joboffer;

                    }
                }
            }
        }

        foreach ($jobs as $job) {
            if (empty($job)) {
                continue;
            }

            $key = $job->dualesStudium ? 'dualesStudium' : 'nonDualesStudium';
            $job->count_joboffers = empty($job->joboffers) ? 0 : count($job->joboffers);
            $jobs[$key][$job->id] = $job;
            unset($jobs[$job->id]);
        }
        
        return $jobs;
    }

    /**
     * Prepares meta data for html document from company
     *
     * @param object company
     * @return array
     */

    public function getCompanyMeta($company)
    {
        $meta = array();
        $meta['robots'] = $company->profile_visible ? 'index, follow' : 'noindex, follow';

        $meta['description'] = empty($company->content) ? '' : $company->content->description_out_of_context;

        $facebook_app_prefix = $this->container->getParameter('facebook_app_prefix');

        $companyLogo = null;
        if (!empty($company->logo)) {
            $companyLogo = $this->getWebdirectory()->getMediaItem($company->logo);
        }

        $meta['properties'] = array(
            'og:type' => $facebook_app_prefix.':betrieb',
            'og:description' => $meta['description'],
            'og:image' => $companyLogo ? $companyLogo->urls->reference: '',
        );

        if (!empty($company->companyindustry)) {
            $industry = $this->getWebdirectory()->getCompanyIndustryItem($company->companyindustry);

            if (!empty($industry)) {
                $meta['properties'][$facebook_app_prefix.':branche'] = $industry->title;
            }
        }

        $jobGroups = array();

        if (!empty($company->main_job_group)) {
            $jobGroup = $this->getWebdirectory()->getJobGroupItem($company->main_job_group);
            $jobGroups[] = $jobGroup->name;
        }

        if (!empty($company->other_job_groups)) {
            foreach ($company->other_job_groups as $otherJobGroup) {
                $jobGroup = $this->getWebdirectory()->getJobGroupItem($otherJobGroup->id);
                if (!in_array($jobGroup->name, $jobGroups)) {
                    $jobGroups[] = $jobGroup->name;
                }
            }
        }

        if (!empty($jobGroups)) {
            $meta['properties'][$facebook_app_prefix.':berufsgruppen'] = $jobGroups;
        }

        $cities = array();

        if (!empty($company->locations)) {
            foreach ($company->locations as $location) {
                $city = $this->getWebdirectory()->getCityItem($location->city);
                if ($city && !in_array($city->name, $cities)) {
                    $cities[] = $city->name;
                }
            }
        }

        if (!empty($cities)) {
            $meta['properties'][$facebook_app_prefix.':standorte'] = $cities;
        }

        

        return $meta;

    }

    protected function getCompanyTrackEvents($company)
    {
        $viewProfileEvent = array(
            'category' => 'Company profile view',
            'action' => html_entity_decode($company->name),
            'label' => 'Profile',
            'value' => 0,
            'nonInteraction' => 'true'
        );

        return $viewProfileEvent;
    }
    
    protected function getCompanyStaticPageTrackEvents($company, $page)
    {

        $viewProfileEvent = array(
            'category' => 'Company profile view',
            'action' => html_entity_decode($company->name),
            'label' => html_entity_decode($page->title),
            'value' => 0,
            'nonInteraction' => 'true'
        );

        return $viewProfileEvent;
    }

    protected function getMixedArticles($company)
    {
        $staticPages = empty($company->staticpages) ? null : $company->staticpages;
        $magazinArticles = $this->getWebdirectory()->getArticlesForCompany($company->id);

        $mixed = array();
        if (!empty($staticPages) && is_array($staticPages)) {
            foreach ($staticPages as $item) {
                $mixedItem = array(
                    'title' => $item->title,
                );
            }
        }
        return $mixed;
    }

    private function getLastCompanies($count = 4)
    {
        $request = $this->getWebdirectory()->getCompaniesCollectionRequest();
        $request->setLimit($count);
        $request->setFilters(array('last' => 1));
        
        return $request->execute();
    }

}
