<?php

namespace Azubister\WebfrontendBundle\Controller;

use Guzzle\Http\Client;
use \Azubister\Webdirectory\Connector\Solr AS SolrConnector;
use Symfony\Component\HttpFoundation\Response;

class SearchController extends BaseController
{

    public function resultsAction()
    {
	    $config = $this->container->getParameter('search_config');

	    $facetKeys = array('types', 'jobgroups', 'jobtypes', 'schooldegrees', 'joboffersources');

	    $dictionary = $this->getWebdirectory()->createSearchDictionary()->getDictionary($facetKeys, $config['expanded']);
	   // $dictionary['f_type']['expanded'] = true;
	   // $dictionary['f_job_groups']['expanded'] = true;

        $azubisterLogoPath = $this->container->getParameter('home_page_og_image');

	    $view_data = array();
        $view_data['_socialbar'] = false;
	    $view_data['facetDictionary'] = $dictionary;
	    $view_data['expanded'] = $config['expanded'];
        $view_data['_title'] = 'Suche Ausbildungsplätze und deine Wunschausbildung';
        $view_data['_meta'] = array(
            'description' => 'Über unsere komfortable Suche findest du Ausbildungsplätze, Ausbildungen, Ausbildungsbetriebe und wichtige Informationen zu dualer Ausbildung und dualem Studium.',
            'robots' => 'NOINDEX, FOLLOW',
            'properties' => array(
                'og:description' => 'Über unsere komfortable Suche findest du Ausbildungsplätze, Ausbildungen, Ausbildungsbetriebe und wichtige Informationen zu dualer Ausbildung und dualem Studium.',
                'og:type' => 'website',
        ));
        if (!empty($azubisterLogoPath)) {
            $view_data['_meta']['properties']['og:image'] = $azubisterLogoPath;
        }

        // SEARCH HERE:

        $request = $this->get('request');
        $allParamters = $request->query->all();

        // TODO: if need to handle expanded
        //if ($request->get('e')) {
            //$config = array('expanded' => $request->get('e'));
        //}

	    $dictionary = $this->getWebdirectory()->createSearchDictionary()->getDictionary($facetKeys, $config['expanded']);

        $searchConfig = $this->buildSearchConfig($allParamters);
        if (!empty($searchConfig['searchterm'])) {
            $view_data['searchterm'] = $searchConfig['searchterm'];
        }

        $facetParam = $request->get('f');
        if (!$facetParam) {
            $facetParam = array();
        }

        $facetConfig = $this->buildFacetConfig($facetParam, $dictionary);

        $filterConfig = array();
        if (array_key_exists('fi', $allParamters)) {
            $filterParams = $allParamters['fi'];
            foreach($filterParams as $filterKey => $filterVal) {
                $filterConfig[] = array('value' => $filterVal, 'field' => $filterKey);
            }
        }

        $filteringByCompany = false;
        if (!empty($filterConfig) && is_array($filterConfig)) {
            foreach ($filterConfig as $filterConfItem) {
                if (isset($filterConfItem['field']) && $filterConfItem['field'] == 'co') {
                    $filteringByCompany = true;
                    break;
                }
            }
        }
        $this->getWebdirectory()->createSearchDictionary()->fillFilters($filterConfig);

        $searchRequest = $this->getWebdirectory()->createSearchRequest();
        if (isset($searchConfig['searchterm'])) {
            $searchRequest->setSearchTerm($searchConfig['searchterm']);
        }

        $searchRequest->setLimit($searchConfig['limit']);
        $searchRequest->setOffset($searchConfig['offset']);
        if (!empty($searchConfig['sort'])) {
            $sort = explode('+', $searchConfig['sort']);
            $searchRequest->setSort($sort[0], $sort[1]);
        }

        if (!empty($searchConfig['lastDocid'])) {
            $searchRequest->setPageDoc($searchConfig['lastDocid']);
        }

        if ($searchConfig['lastScore'] > 0) {
            $searchRequest->setPageScore($searchConfig['lastScore']);
        }

        $searchRequest->loadFacetConfig($facetConfig);
        $searchRequest->loadFilterConfig($filterConfig);

        try {
            $this->getWebdirectory()->getDispatcher()->sendSearchRequest($searchRequest);
            $rawDocs = $searchRequest->getDocs();
            $docs = array();
            $lastDoc = false;
            if (count($rawDocs) > 0) {

                if (!$filteringByCompany) {
                    $teaserAd = $this->getWebdirectory()->getTeaserAd(66);
                    if ($teaserAd) {
                        $docs[] = $this->processTeaserEntry($teaserAd);
                    }
                }
                foreach ($rawDocs AS $rawDoc) {
                    $docs[] = $this->processResultEntry($rawDoc);
                }
                // we need to pass the score and last doc id of last item shown for paging
                reset($rawDocs);
                $lastDoc = end($rawDocs);
            }

            $response = array();

            $response['numResults'] = $searchRequest->getNumResults();
            $response['docs'] = $docs;
            $response['searchterm'] = $searchRequest->getSearchTerm();
            $response['facets'] = $searchRequest->getFacets();
            $response['filters'] = $searchRequest->getFilters();
            $response['requestUrl'] = $searchRequest->getFinalRequestUrl();

            if ($lastDoc) {
                $response['lastScore'] = $lastDoc->score;
                $docidKey = '[docid]';
                $response['lastDocid'] = $lastDoc->$docidKey;
            }

        } catch (\ARLException $e) {
            $response = array();

            $response['numResults'] = 0;
            $response['docs'] = array();
            $response['searchterm'] = $searchRequest->getSearchTerm();
            $response['error'] = 1;
        }

        $facets = $response['facets'];

        // Merge facets with dictionary, to get Labels.
        foreach ($dictionary as $key => $value) {
            $facets[$key]['label'] = $value['label'];
            foreach ($value['values'] as $valueKey => $valueLabel) {
                $facets[$key]['values'][$valueKey]['label'] = $valueLabel;
            }
        }


        // Set facet expanded if it has selected value.
        foreach ($facets as $key => $facet) {
            foreach ($facet['values'] as $facetValue) {
                if ($facetValue['selected'] == 'true') {
                    $facets[$key]['expanded'] = 'true';
                    break;
                }
            }
        }

        if (empty($response['docs'])) {
            $facetsHtml = '';
        } else {
            $facetsHtml = $this->getFacetsHtml($facets, $allParamters);
        }
        $view_data['facet_list'] = $facetsHtml;

        $view_data['docs'] = $response['docs'];

        // SEARCH FILTERS CLEAR URLS BUILDING
        $filtersHtml = '';

        // Facet filters
        $numberOfObjectTypesSelected = 0;
        $numberOfFiltersSelected = 0;
        foreach ($facets as $facet) {
            foreach ($facet['values'] as $facetValue) {
                if ($facetValue['selected'] == 'true') {
                    $numberOfFiltersSelected++;
                    if ($facet['field'] == 't') {
                        $numberOfObjectTypesSelected++;
                    }
                    $paramsCopy = $allParamters;
                    $keyToDelete = array_search($facetValue['value'], $paramsCopy['f'][$facet['field']]);
                    unset($paramsCopy['f'][$facet['field']][$keyToDelete]);
                    unset($paramsCopy['page']);
                    unset($paramsCopy['pageDoc']);
                    unset($paramsCopy['pageScore']);
                    $query = http_build_query($paramsCopy);
                    $href = $request->getScheme() . '://' . $request->getHttpHost() . $request->getPathInfo();
                    $href .= '?' . $query;
                    $filtersHtml .= '<li data-type"facet" data-field="' . $facet['field'] . '" data-value="' . $facetValue['value'] . '"><a href="' . $href . '">' . $facetValue['label'] . '<i></i></a></li>';
                }
            }
        }

        $filters = $response['filters'];
        foreach ($filters as $filter) {
            $paramsCopy = $allParamters;
            unset($paramsCopy['fi'][$filter['field']]);
            unset($paramsCopy['page']);
            $query = http_build_query($paramsCopy);
            $href = $request->getScheme() . '://' . $request->getHttpHost() . $request->getPathInfo();
            $href .= '?' . $query;
            $filtersHtml .= '<li data-type="filter"><a href="' . $href . '">' . $filter['label'] . '<i></i></a></li>';
        }

        // Clear all filters link
        $paramsCopy = $allParamters;
        unset($paramsCopy['f']);
        unset($paramsCopy['fi']);
        unset($paramsCopy['page']);
        unset($paramsCopy['pageDoc']);
        unset($paramsCopy['pageScore']);
        $query = http_build_query($paramsCopy);
        $href = $request->getScheme() . '://' . $request->getHttpHost() . $request->getPathInfo();
        if (!empty($query)) {
            $href .= '?' . $query;
        }

        if (!empty($filtersHtml)) {
		    $filtersHtml .= '<li data-type="all"><a href="' . $href . '">Alle Filter zurücksetzen<i></i></a></li>';
        }

        $view_data['clear_all_filters_url'] = $href;

        $view_data['filters'] = $filtersHtml;


        // PAGER rendering.

        $currentPage = $request->get('page');
        if (empty($currentPage)) {
            $currentPage = 1;
        }
        $numberOfItems = $response['numResults'];
        $numberOfItemsPerPage = 10; // TODO: make it dynamic.
        $numberOfPages = ceil($numberOfItems / $numberOfItemsPerPage);

        $pager = array(
            'current' => $currentPage,
            'pages' => array(),
        );

        if ($currentPage > 1) {
            $pager['first'] = 1;
            $pager['prev'] = $currentPage - 1;
        }

        if ($currentPage < $numberOfPages) {
            $pager['next'] = $currentPage + 1;
            $pager['last'] = $numberOfPages;
        }

        for ($p = 1; $p <= $numberOfPages; $p++) {
            $paramsCopy = $allParamters;
            if ($p > 1) {
                $paramsCopy['page'] = $p;
            } else {
                unset($paramsCopy['page']);
            }
            $query = http_build_query($paramsCopy);
            $href = $request->getScheme() . '://' . $request->getHttpHost() . $request->getPathInfo();
            $href .= '?' . $query;

            $pager['pages'][$p] = $href;
        }

        $pagerHtml = '<ul>';

        $isNoFollow = true;
        if ($numberOfFiltersSelected == 1 && $numberOfObjectTypesSelected == 1 && empty($searchConfig['searchterm'])) {
            $isNoFollow = false;
        }
        if ($currentPage > 1) {
            if (isset($pager['prev'])) {
                $paramsCopy = $allParamters;
                unset($paramsCopy['pageDoc']);
                unset($paramsCopy['pageScore']);
                if ($pager['prev'] > 1) {
                    $paramsCopy['page'] = $pager['prev'];
                } else {
                    unset($paramsCopy['page']);
                }
                $query = http_build_query($paramsCopy);
                $href = $request->getScheme() . '://' . $request->getHttpHost() . $request->getPathInfo();
                $href .= '?' . $query;

                $pagerHtml .= '<li class="previous"><a class="previous" href="' . $href . '"';
                if ($isNoFollow) {
                    $pagerHtml .= ' rel="nofollow"';
                }
                $pagerHtml .= '>Zurück</a></li>';
            }
        }

        if ($numberOfPages > 10) {
            $pageStart = 0;
            $pageEnd = 0;
            if ($currentPage <= 6) {
                $pageStart = 1;
                $pageEnd = 10;
            } else {
                if ($numberOfPages - $currentPage < 4) {
                    $pageStart = $numberOfPages - 9;
                    $pageEnd = $numberOfPages;
                } else {
                    $pageStart = $currentPage - 5;
                    $pageEnd = $currentPage + 4;
                }
            }
            for ($i = $pageStart; $i <= $pageEnd; $i++) {
                $pagerHtml .= '<li>';
                if ($i == $currentPage) {
                    $pagerHtml .= '<span>' . $i . '</span>';
                } else {
                    $pagerHtml .= '<a href="' . $pager['pages'][$i] . '"';
                    if ($isNoFollow) {
                        $pagerHtml .= ' rel="nofollow"';
                    }
                    $pagerHtml .= '>' . $i . '</a>';
                }
                $pagerHtml .= '</li>';
            }
        } else {
            foreach ($pager['pages'] as $number => $pageUrl) {
                $pagerHtml .= '<li>';
                if ($number == $currentPage) {
                    $pagerHtml .= '<span>' . $number . '</span>';
                } else {
                    $pagerHtml .= '<a href="' . $pageUrl . '"';
                    if ($isNoFollow) {
                        $pagerHtml .= ' rel="nofollow"';
                    }
                    $pagerHtml .= '>' . $number . '</a>';
                }
                $pagerHtml .= '</li>';
            }
        }

        // Next
        if (isset($pager['next'])) {
            $paramsCopy = $allParamters;

            // TODO: Do we need pageDoc and pageScore?
            //if (array_key_exists('lastDocid', $response)) {
                //$paramsCopy['pageDoc'] = $response['lastDocid'];
                //$paramsCopy['pageScore'] = $response['lastScore'];
            //}
            $paramsCopy['page'] = $pager['next'];
            $query = http_build_query($paramsCopy);
            $href = $request->getScheme() . '://' . $request->getHttpHost() . $request->getPathInfo();
            $href .= '?' . $query;

            $pagerHtml .= '<li class="next"><a class="next" href="' . $href . '"';
            if ($isNoFollow) {
                $pagerHtml .= ' rel="nofollow"';
            }
            $pagerHtml .= '>Weiter</a></li>';
        }
        $pagerHtml .= '</ul>';

        if ($numberOfPages > 1) {
            $view_data['search_pager'] = $pagerHtml;
        } else {
            $view_data['search_pager'] = '';
        }


		$view_data = array_merge($this->getCommonViewData(), $view_data);

        if ($request->get('json') == '1') {
            $response = array();
            $response['facet_list'] = $view_data['facet_list'];
            $response['docs'] = $view_data['docs'];
            $response['filters'] = $view_data['filters'];
            $response['search_pager'] = $view_data['search_pager'];
            $response['status'] = 'ok';
            return new Response(json_encode($response), 200, array('Content-Type'=>'application/json'));
        } else {
            return $this->render('AzubisterWebfrontendBundle:Search:results2.html.twig', $view_data);
        }
    }

    public function buildSearchConfig($params) {
        $searchConfig = array(
            'limit' => 10,
            'offset' => 0,
            'lastDocid' => 0,
            'lastScore' => 0,
        );

        $searchTerm = array_key_exists('q', $params) ? $params['q'] : '';

        if (array_key_exists('f', $params) && array_key_exists('t', $params['f'])) {
            if (count($params['f']['t']) == 1) {
                $searchConfig['sort'] = 'score+desc,id+desc';
            }
        }

        if (!empty($searchTerm)) {
            $searchConfig['searchterm'] = $searchTerm;
        }

        if (array_key_exists('pageDoc', $params)) {
            $searchConfig['lastDocid'] = $params['pageDoc'];
        }

        if (array_key_exists('page', $params) && is_numeric($params['page'])) {
            $pageNumber = $params['page'];
            $limit = 10;
            if ($pageNumber > 1) {
                $offset = $limit * ($pageNumber - 1);
            } else {
                $offset = 0;
            }
            $searchConfig['offset'] = $offset;
        }
        return $searchConfig;
    }

    public function buildFacetConfig($facetParam, $dictionary) {
        $facetConfig = array();
        foreach ($dictionary as $key => $value) {
            $facetConfig[$key] = array();
            $facetConfig[$key]['field'] = $key;
            $facetConfig[$key]['expanded'] = ($value['expanded']) ? 'true' : 'false';
            $facetConfig[$key]['values'] = array();
            foreach ($value['values'] as $value_key => $value_value) {
                $facetConfig[$key]['values'][$value_key] = array();
                $facetConfig[$key]['values'][$value_key]['value'] = $value_key;
                $selected = 'false';
                if (array_key_exists($key, $facetParam)) {
                    $selected = (in_array($value_key, $facetParam[$key])) ? 'true' : 'false';
                }
                $facetConfig[$key]['values'][$value_key]['selected'] = $selected;
            }
        }

        return $facetConfig;
    }

    public function getFacetsHtml($facets, $params) {
        $html = '';
        $request = $this->get('request');
        foreach ($facets as $facet) {

            // Checking if facet has counts
            $hasCount = false;
            foreach ($facet['values'] as $facetValue) {
                if (array_key_exists('count', $facetValue) && $facetValue['count'] > 0) {
                    $hasCount = true;
                }
            }

            if (!$hasCount) {
                continue;
            }
            // end counts checking

            $html .= '<h3';
            $html .= ' data-field="' .  $facet['field'] . '"';
            $html .= ' data-expanded="' . $facet['expanded'] . '"';

            $classes = '';

            if ($facet['expanded'] == 'true') {
                $classes .= 'open ';
            }

            $isSelected = false;
            // Check if has selected and expanded
            foreach ($facet['values'] as $facetValue) {
                if ($facetValue['selected'] == 'true') {
                    $isSelected = true;
                    break;
                }
            }

            if ($isSelected) {
                $classes .= ' active';
            }

            if (!empty($classes)) {
                $html .= ' class="' . $classes . '"';
            }

            $html .= '>';
            $html .= $facet['label'] . '<i></i></h3>';


            $html .= '<ul';
            if ($facet['expanded'] == 'false') {
                $html .= ' style="display:none">';
            } else {
                $html .= '>';
            }

            foreach ($facet['values'] as $facetValue) {
                if (array_key_exists('count', $facetValue) && $facetValue['count'] > 0) {
                    // RENDER FACET VALUES THERE
                    $html .= '<li>';
                    $isValueSelected = ($facetValue['selected'] == 'true');
                    if (!$isValueSelected) {
                        $paramsCopy = $params;
                        unset($paramsCopy['page']);
                        unset($paramsCopy['pageDoc']);
                        unset($paramsCopy['pageScore']);
                        if (array_key_exists('f', $paramsCopy)) {
                            if (array_key_exists($facet['field'], $paramsCopy['f'])) {
                                $paramsCopy['f'][$facet['field']][] = $facetValue['value'];
                            } else {
                                $paramsCopy['f'][$facet['field']] = array();
                                $paramsCopy['f'][$facet['field']][] = $facetValue['value'];
                            }
                        } else {
                            $paramsCopy['f'] = array();
                            $paramsCopy['f'][$facet['field']] = array();
                            $paramsCopy['f'][$facet['field']][] = $facetValue['value'];
                        }
                        $query = http_build_query($paramsCopy);
                        $href = $request->getScheme() . '://' . $request->getHttpHost() . $request->getPathInfo();
                        $href .= '?' . $query;
                        $html .= '<a href="'. $href . '" rel="nofollow">';
                    } else {
                        $html .= '<span>';
                    }
                    $html .= '<span></span>';
                    $html .= $facetValue['label'];
                    if (!$isValueSelected) {
                        $html .= '</a>';
                    } else {
                        $html .= '</span>';
                    }
                    $html .= '</li>';
                }
            }

            $html .= '</ul>';
        }

        return $html;
    }

    public function ajaxResultsAction() 
    {
        
        $request = $this->get('request');
        
        $searchConfig = $request->get('searchConfig');
        $facetConfig = $request->get('facetConfig');
        $filterConfig = $request->get('filterConfig');

        $filteringByCompany = false;
        if (!empty($filterConfig) && is_array($filterConfig)) {
            foreach ($filterConfig as $filterConfItem) {
                if (isset($filterConfItem['field']) && $filterConfItem['field'] == 'co') {
                    $filteringByCompany = true;
                    break;
                }
            }
        }
        
       
       $this->getWebdirectory()->createSearchDictionary()->fillFilters($filterConfig);
       
        $searchRequest = $this->getWebdirectory()->createSearchRequest();
        if (isset($searchConfig['searchterm'])) 
        {
            $searchRequest->setSearchTerm($searchConfig['searchterm']);
        }
        $searchRequest->setLimit($searchConfig['limit']);
        $searchRequest->setOffset($searchConfig['offset']);
        if (!empty($searchConfig['sort'])) {
            $sort = explode('+', $searchConfig['sort']);
            $searchRequest->setSort($sort[0], $sort[1]);
        }
        
        if (!empty($searchConfig['lastDocid'])) {
            $searchRequest->setPageDoc($searchConfig['lastDocid']);
        }
        
        if ($searchConfig['lastScore'] > 0) {
            $searchRequest->setPageScore($searchConfig['lastScore']);
        }
        
        
     /*   $searchRequest->setLimit(2);
        $searchRequest->setOffset(0);*/
        
        $searchRequest->loadFacetConfig($facetConfig);
        $searchRequest->loadFilterConfig($filterConfig);

        try {
            $this->getWebdirectory()->getDispatcher()->sendSearchRequest($searchRequest);
            $rawDocs = $searchRequest->getDocs();
            $docs = array();
            $lastDoc = false;
        if (count($rawDocs) > 0) 
        {

            if (!$filteringByCompany) {
                $teaserAd = $this->getWebdirectory()->getTeaserAd(66);
                if ($teaserAd) {
                    $docs[] = $this->processTeaserEntry($teaserAd);
                }
            }
            foreach ($rawDocs AS $rawDoc) {
                $docs[] = $this->processResultEntry($rawDoc);
            }
            // we need to pass the score and last doc id of last item shown for paging
            reset($rawDocs);
            $lastDoc = end($rawDocs);

        }



        $response = array();

        $response['numResults'] = $searchRequest->getNumResults();
        $response['docs'] = $docs;
        $response['searchterm'] = $searchRequest->getSearchTerm();
        $response['facets'] = $searchRequest->getFacets();
        $response['filters'] = $searchRequest->getFilters();
        $response['requestUrl'] = $searchRequest->getFinalRequestUrl();

        if ($lastDoc) 
        {
            $response['lastScore'] = $lastDoc->score;
            $docidKey = '[docid]';
            $response['lastDocid'] = $lastDoc->$docidKey;
        }
        } catch (\ARLException $e) {
            $response = array();

            $response['numResults'] = 0; 
            $response['docs'] = array();
            $response['searchterm'] = $searchRequest->getSearchTerm();
            $response['error'] = 1;

        }
        
        
        return new Response(json_encode($response), 200, array('Content-Type'=>'application/json'));

    }
    
    
	public function resultsOldAction()
	{
	
	    $config = $this->container->getParameter('search_config');
	    
	    
	    
	    $facetKeys = array('types', 'jobgroups', 'jobtypes', 'schooldegrees', 'joboffersources');
	    
	    $dictionary = $this->getWebdirectory()->createSearchDictionary()->getDictionary($facetKeys, $config['expanded']);
	   // $dictionary['f_type']['expanded'] = true;
	   // $dictionary['f_job_groups']['expanded'] = true;
	    
        $azubisterLogoPath = $this->container->getParameter('home_page_og_image');

	    $view_data = array();
        $view_data['_socialbar'] = false;
	    $view_data['facetDictionary'] = $dictionary;
	    $view_data['expanded'] = $config['expanded'];
        $view_data['_title'] = 'Suche Ausbildungsplätze und deine Wunschausbildung';
        $view_data['_meta'] = array(
            'description' => 'Über unsere komfortable Suche findest du Ausbildungsplätze, Ausbildungen, Ausbildungsbetriebe und wichtige Informationen zu dualer Ausbildung und dualem Studium.',
            'robots' => 'NOINDEX, FOLLOW',
            'properties' => array(
                'og:description' => 'Über unsere komfortable Suche findest du Ausbildungsplätze, Ausbildungen, Ausbildungsbetriebe und wichtige Informationen zu dualer Ausbildung und dualem Studium.',
                'og:type' => 'website',
        ));
        if (!empty($azubisterLogoPath)) {
            $view_data['_meta']['properties']['og:image'] = $azubisterLogoPath;
        }
	    
		
		
		$view_data = array_merge($this->getCommonViewData(), $view_data);
        return $this->render('AzubisterWebfrontendBundle:Search:results.html.twig', $view_data);
       
	}

    protected function processTeaserEntry($data)
    {
        $tplDataHelper = $this->get('azubister.tpl_data.helper');        
        $view_data = $tplDataHelper->prepareTeaserEntryData($data);

        if (!isset($view_data['image_id'])) {
            return '';
        }

        return $this->container->get('templating')->render('AzubisterWebfrontendBundle:Search:ad_resultentry.html.twig', $view_data);
    }
	
	public function processResultEntry($doc) {
	     
	    $view_data = array();
	     
	    if (!empty($doc->score)) $view_data['score'] = $doc->score;
	    $docid = '[docid]';
	    if (!empty($doc->$docid)) $view_data['docid'] = $doc->$docid;
	    switch ($doc->type) {
	         
            case 'post':
                $this->handlePostResultEntry($doc, $view_data);
                break;
            case 'city_topic':
            case 'topic_topic':
            case 'jobgroup_topic':
                $this->handleTopicResultEntry($doc, $view_data);
                break;
	        case 'job':
	            $this->handleJobResultEntry($doc, $view_data);
	            break;
	        case 'company':
	            $this->handleCompanyResultEntry($doc, $view_data);
                break;
            case 'joboffer':
                $this->handleJobofferResultEntry($doc, $view_data);
                break;
            case 'externaljoboffer':
                $this->handleExternalJobofferResultEntry($doc, $view_data);
                break;
            case 'companystaticpage':
                $this->handleCompanystaticpageResultEntry($doc, $view_data);
                break;
	        default:
	            $view_data['title'] = 'Unknown type ' . $doc->type;
	            $view_data['image_id'] = $this->container->getParameter('fallback_image');
	            $view_data['image_context'] = 'logo';
	            $view_data['description'] = 'none';
	            $view_data['url'] = '#todo';
	            break;
	             
	             
	    }
	     
        if (empty($view_data['url'])) {
            return '';
        } else {
	     return $this->container->get('templating')->render('AzubisterWebfrontendBundle:Search:resultentry.html.twig', $view_data);
        }
	     
	    return $this->render('AzubisterWebfrontendBundle:Search:resultentry.html.twig', $view_data);
	}
	
	public function facetsAction($facets) {
	   
	 
	  //  die();
	    return $this->render('AzubisterWebfrontendBundle:Search:facets.html.twig', array('facets' => $facets));
	}

    protected function cutDescription($text) {
        if (strlen($text) > 150) {
            $text = wordwrap($text, 150, '---#---');
            $text = strstr($text, '---#---', true);
            $text .= '...';
        }
        return $text;
    }
	
	/************
	 * Helper methods
	 ************/
    protected function handlePostResultEntry($doc, &$view_data)
    {
        $item = $this->getWebdirectory()->getArticleItem($doc->typeid);
        if ($item) {
            $fallback_image_id = $this->container->getParameter('fallback_image');
            $fallback_context = 'logo';

            $view_data['title'] = $item->title;    
            $view_data['image_id'] = empty($item->feature_image) ? $fallback_image_id : $item->feature_image;
            $view_data['image_context'] = empty($item->feature_image) ? $fallback_context : 'magazin';
            $view_data['description'] = $item->excerpt;
            $view_data['url'] = $this->cutDescription($item->url);

            $view_data['footer'] = array();
        }
    }

    protected function handleTopicResultEntry($doc, &$view_data)
    {
        $item = $this->getWebdirectory()->getTermItem($doc->typeid);
        if ($item) {
            $fallback_image_id = $this->container->getParameter('fallback_image');
            $fallback_context = 'logo';

            $view_data['title'] = $item->name;    
            $view_data['image_id'] = empty($item->image_id) ? $fallback_image_id : $item->image_id;
            $view_data['image_context'] = empty($item->image_id) ? $fallback_context : 'magazin';
            $view_data['description'] = $this->cutDescription($item->description);
            $view_data['url'] = $item->url;

            $view_data['footer'] = array();
        }
    }
	 
	protected function handleJobResultEntry($doc, &$view_data) {
	    
	    
	    $job = $this->getWebdirectory()->getJobItem($doc->typeid);
	    
        $fallback_image_id = $this->container->getParameter('fallback_image');
        $fallback_context = 'logo';

        if ($job && $job->active) {
            $view_data['title'] = $job->title;    
            $view_data['image_id'] = empty($job->feature_image) ? $fallback_image_id : $job->feature_image;
            $view_data['image_context'] = empty($job->feature_image) ? $fallback_context : 'feature';
            $view_data['description'] = $this->cutDescription($job->short_description);
            $view_data['url'] = $job->url;

            $view_data['footer'] = array();

            if (!empty($job->job_type)) {
                $jt = $this->getWebdirectory()->getJobTypeItem($job->job_type);
                if ($jt) {
                    $view_data['footer'][] = array(
                        'label' => $jt->title,
                        //'url' => '#f:jt:'.$jt->id,
                    );
                }
            }

            if (!empty($job->main_job_group)) {
                $jg = $this->getWebdirectory()->getJobgroupItem($job->main_job_group);
                if ($jg) {
                    $view_data['footer'][] = array('label' => 'Branche: '.$jg->name);
                }
            }

            /*

            if (!empty($job->recommended_school_degrees)) {
                $sd = array();
                foreach ($job->recommended_school_degrees as $school_degree ) {
                    $school_degree = $this->getWebdirectory()->getSchoolDegreeItem($school_degree->id);
                    if ($school_degree) {
                        $sd[] = $school_degree->title;
                    }
                }
                $view_data['footer'][] = array('label' => implode(', ', $sd));
            }
             */
        }
	  
	   
	}
	
	protected function handleCompanyResultEntry($doc, &$view_data) {
	    
	    
	    $item = $this->getWebdirectory()->getCompanyItem($doc->typeid);
	    
        $fallback_image_id = $this->container->getParameter('fallback_image');
        $fallback_context = 'logo';

        if ($item && $item->profile_active == true) {
            $view_data['title'] = $item->name;
            $view_data['image_id'] = empty($item->logo) ? $fallback_image_id : $item->logo;
            $view_data['image_context'] = empty($item->logo) ? $fallback_context : 'logo';
            $view_data['description'] = empty($item->content->short_description) ? '' : $item->content->short_description;
            $view_data['description'] = $this->cutDescription($view_data['description']);
            $view_data['url'] = $item->url;

            $view_data['footer'] = array();

            if (!empty($item->companyindustry)) {
                $industry = $this->getWebdirectory()->getCompanyindustryItem($item->companyindustry);
                if ($industry) {
                    $view_data['footer'][] = array('label' => 'Branche: '.$industry->title);
                }
            }

            if (!empty($item->jobs)) {
                $label = count($item->jobs);
                $label .= count($item->jobs) == 1 ? ' Ausbildung' : ' Ausbildungen';
                $view_data['footer'][] = array('label' => $label);
            }

            if (empty($item->locations)) {
                $label = 'bundesweit';
            } else {
                $label = count($item->locations);
                $label .= count($item->locations) == 1 ? ' Standort' : ' Standorte';
            }

            $view_data['footer'][] = array('label' => $label);
        }
	  
	   
	}

    protected function handleCompanystaticpageResultEntry($doc, &$view_data) {
	    $item = $this->getWebdirectory()->getCompanyStaticpageItem($doc->typeid);
        if ($item) {
            $fallback_image_id = $this->container->getParameter('fallback_image');
            $fallback_context = 'logo';

            $company = $this->getWebdirectory()->getCompanyItem($item->company);
            $show_staticpage = $company->premium && !empty($company->features) && $company->features->static_pages && $company->profile_active;
            if (!$show_staticpage) {
                return;
            }

            $imageId = false;
            $imageContext = 'logo';
            if (!empty($item->feature_image)) {
                $imageId = $item->feature_image;
                $imageContext = 'feature';
            } else if (!empty($company->logo)) {
                $imageId = $company->logo;
            } else {
                $imageId = $fallback_image_id;
            }


            $view_data['title'] = $item->title;
            $view_data['image_id'] = $imageId;
            $view_data['image_context'] = $imageContext;
            $view_data['description'] = $item->description;
            $view_data['description'] = $this->cutDescription($view_data['description']);
            $view_data['url'] = $this->get('router')->generate('company_static_page', array('company_slug' => $company->slug, 'static_slug' => $item->slug));

            $view_data['footer'] = array(
                array('label' => $company->name),
            );

            if (!empty($company->companyindustry)) {
                $industry = $this->getWebdirectory()->getCompanyindustryItem($company->companyindustry);
                if ($industry) {
                    $view_data['footer'][] = array('label' => 'Branche: '.$industry->title);
                }
            }
        }
    }

    protected function handleJobofferResultEntry($doc, &$view_data) {
	    $item = $this->getWebdirectory()->getJobofferItem($doc->typeid);
	    

        if ($item) {

            if ((isset($item->valid_from) && new \Datetime($item->valid_from) > new \Datetime('today'))
            || (isset($item->valid_till) && new \Datetime($item->valid_till) < new \Datetime('today'))) {
                return;
            }
            $company = $this->getWebdirectory()->getCompanyItem($item->company);

            if (empty($company) || $company->profile_active == false) {
                return;
            }


            $fallback_image_id = $this->container->getParameter('fallback_image');
            $fallback_context = 'logo';


            $view_data['title'] = $item->title;    
            $view_data['image_id'] = empty($company->logo) ? $fallback_image_id : $company->logo;
            $view_data['image_context'] = empty($company->logo) ? $fallback_context : 'logo';

            //$view_data['description'] = $item->description;
            //$view_data['description'] = $this->cutDescription($view_data['description']);
            $view_data['description'] = '';
            $view_data['url'] = $item->url;

            $view_data['footer'] = array(
                array('label' => $company->name),
            );

            if (!empty($item->companyjobs)) {
                $j = array(); $counter = 1;
                foreach ($item->companyjobs as $job) {
                    if ($counter > 3) break;
                    $j[] = $job->name;
                    $counter++;
                }
                $label = implode(', ', $j);
                if (count($item->companyjobs) > 3) $label .= ' ('.(count($item->companyjobs) - 3).' weitere)';
                $view_data['footer'][] = array('label' => $label);
            }
            if (!empty($item->companylocations)) {
                $j = array(); $counter = 1;
                foreach ($item->companylocations as $job) {
                    if ($counter > 3) break;
                    $j[] = $job->name;
                    $counter++;
                }
                $label = implode(', ', $j);
                if (count($item->companylocations) > 3) $label .= ' ('.(count($item->companylocations) - 3).' weitere)';
                $view_data['footer'][] = array('label' => $label);
            } else {
                $view_data['footer'][] = array('label' => 'bundesweit');
            }

            $view_data['footer'][] = array('label' => 'Quelle: azubister');
        }
    }
    
    protected function handleExternalJobofferResultEntry($doc, &$view_data) 
    {
	    $item = $this->getWebdirectory()->getExternalJobofferItem($doc->typeid);
	    

        if ($item) {
            $fallback_image_id = $this->container->getParameter('fallback_image');
            $fallback_context = 'logo';

            $view_data['title'] = $item->title;    
            $view_data['image_id'] = $fallback_image_id;
            $view_data['image_context'] = $fallback_context;

            $view_data['description'] = '';
            $view_data['url'] = $item->link;
            $view_data['link_target'] = '_blank';
            $view_data['footer'] = array();

            if (!empty($item->company_name)) {
                $view_data['footer'][] = array('label' => $item->company_name);
            }

            if (!empty($item->job)) {
                $job = $this->getWebdirectory()->getJobItem($item->job);
                $view_data['footer'][] = array('label' => $job->title);
            }
            if (!empty($item->cities)) {
                $j = array(); $counter = 1;
                foreach ($item->cities as $city) {
                    if ($counter > 3) break;
                    $j[] = $city->name;
                    $counter++;
                }
                $label = implode(', ', $j);
                if (count($item->cities) > 3) $label .= ' ('.(count($item->cities) - 3).' weitere)';
                $view_data['footer'][] = array('label' => $label);
                
            }

            $view_data['item_html_class'] = 'externaljoboffer';
            $view_data['data_attr'] = ' data-id="'.$item->partner_id.'" data-source="'.$item->source.'"';
            $dict = $this->getWebdirectory()->createSearchDictionary()->getJobofferSources();

            $offer_src = isset($dict[$item->source]) ? $dict[$item->source] : $item->source;
            $view_data['footer'][] = array('label' => 'Quelle: '.$offer_src);
        }
    }
	
	
}
