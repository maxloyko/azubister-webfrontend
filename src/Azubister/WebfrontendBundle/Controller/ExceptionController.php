<?php
namespace Azubister\WebfrontendBundle\Controller;

use Symfony\Bundle\TwigBundle\Controller\ExceptionController as BaseExceptionController;
use Symfony\Component\HttpKernel\Exception\FlattenException;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Symfony\Component\HttpFoundation\Response;

class ExceptionController extends BaseExceptionController {

    public function showAction(FlattenException $exception, DebugLoggerInterface $logger = null, $format = 'html')
    {
        $this->container->get('request')->setRequestFormat($format);

        $templating = $this->container->get('templating');

        $msg = $exception->getMessage();
        if (strcmp($msg, 'city404') == 0)
        {
            $view_data = $this->getCityViewData();
        } else
        {
            $view_data = $this->getDefaultViewData($exception,$logger);
        }

        $view_data = array_merge($this->getCommonViewData(), $view_data);
        return $templating->renderResponse('AzubisterWebfrontendBundle:Common:error.html.twig', $view_data);
    }

    public function getCommonViewData()
    {
        $env = $this->container->getParameter('directory_environment');
        $wd = new \Azubister\Webdirectory\Directory($env);

        $data = array(
            '_title'            => 'Ausbildung, Duales Studium, Ausbildungsplätze',
            'main_header'       => $wd->getMainHeader(''),
            'main_footer'       => $wd->getMainFooter(),
            '_adzones'          => $this->container->getParameter('adzones'),
            'html_prefixes'     => '',
            //'_google_maps_api_key' => $this->container->getParameter('google_maps_api_key'),
            '_include_maps'     => false,
            '_socialbar'        => false,
            '_show_skyscrapper' => true,
        );

        return $data;
    }

    public function getDefaultViewData($exception, $logger = null)
    {
        $code           = $exception->getStatusCode();
        $errorTitle     = $code."";
        $errorMessage   = $code == 404 ? "Uuups, nichts gefunden!": "Uuups, ein Problem ist aufgetreten.";
        $currentContent = $this->getAndCleanOutputBuffering();


        $data           = array(
            'errorTitle'     => $errorTitle,
            'errorMessage'   => $errorMessage,
            'isDebug'        => $this->container->get('kernel')->isDebug(),
            'status_code'    => $code,
            'status_text'    => isset(Response::$statusTexts[$code]) ? Response::$statusTexts[$code] : '',
            'exception'      => $exception,
            'logger'         => $logger,
            'currentContent' => $currentContent,
        );
        return $data;
    }

    public function getCityViewData()
    {
        $env = $this->container->getParameter('directory_environment');
        $wd     = new \Azubister\Webdirectory\Directory($env);
        $cities = $wd->getCityErrorPage();
        
        $data   = array(
            'cities'        => $cities,
//            '_include_maps' => true,
            'isDebug'       => false,
        );
        return $data;
    }
}
