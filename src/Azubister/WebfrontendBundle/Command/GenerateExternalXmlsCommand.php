<?php

namespace Azubister\WebfrontendBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateExternalXmlsCommand extends ContainerAwareCommand
{

    protected $_xml_head = '<?xml version="1.0" encoding="UTF-8"  standalone="yes"?>';
    protected $_webdir;
    protected $_filesDir;

    protected function configure()
    {
        $this
            ->setName('azubister:generate:external-xmls')
            ->setDescription('Generate xmls for import');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_input = $input;
        $this->_output = $output;
        $this->_dialog = $this->getHelperSet()->get('dialog');
        $this->_webdir = $this->getContainer()->get('webdirectory');

        $path = realpath(__DIR__.'/../../../../web');
        $this->_filesDir = $path.'/files';

        if (!is_dir($this->_filesDir)) {
            mkdir($this->_filesDir);
        }

        $joboffers = $this->_webdir->getJoboffersActualCollection();

        $jobs_xml = $this->_xml_head . '<offers>';
        $jobrapido_xml = $this->_xml_head . '<adverts>';

        $i = 0;

        foreach ($joboffers as $k => $joboffer) {

            $data = $this->prepareJobsItemData($joboffer);

            if (!empty($data)) {

                foreach($data as $offer_data) {
                    $jobs_xml .= '<offer>';
                    $jobs_xml .= $this->getItemXml($offer_data);
                    $jobs_xml .= '</offer>';
                }

            }

            $data = $this->prepareJobrapidoItemData($joboffer);
            if (!empty($data)) {

                foreach($data as $offer_data) {
                    $jobrapido_xml .= '<advert>';
                    $jobrapido_xml .= $this->getItemXml($offer_data);
                    $jobrapido_xml .= '</advert>';
                }
                $i += count($data);              

            }
        }

        $jobs_xml .= '</offers>';
        $jobrapido_xml .= '</adverts>';

        file_put_contents($this->_filesDir.'/jobs.xml', $jobs_xml);
        file_put_contents($this->_filesDir.'/jobrapido.xml', $jobrapido_xml);

        $this->_output->writeln("$i joboffers ware insert to xml");
        $this->_output->writeln("Command executed");
   
    }

    private function prepareJobsItemData($joboffer)
    {

        if (empty($joboffer->companyjobs)) {
            return array();
        }

        $r = array();
        $data = array();
        $data['id']['val']          = $joboffer->id;
        $data['bundesweit']['val']  = empty($joboffer->companylocations);
        $company                    = $this->_webdir->getCompanyItem($joboffer->company);
        $data['company']['val']     = array(
                                        'name' => array(
                                            'val'   => $company->name,
                                            'wrap'  => true
                                        ),
                                        'profile'   => array(
                                            'val'   => $company->url,
                                            'wrap'  => true
                                        )
        );
        $data['offer_publishing_date']['val']           = $joboffer->created;

        if(!empty($joboffer->training_start)) {
            $data['training_start_date']['val'] = $joboffer->training_start;
            $data['training_start_date']['wrap'] = true;
        }

        $data['count_jobs']['val']                      = !empty($joboffer->free_positions) ? $joboffer->free_positions : 0;
        $data['training_application_deadline']['val']   = !empty($joboffer->final_application_date) ? $joboffer->final_application_date : '';

        if(!empty($joboffer->description)) {
            $data['description']['val']                     = $joboffer->description;
            $data['description']['wrap']                    = true;
        }

        if(!empty($joboffer->weoffer)) {
            $data['offering']['val']                        = $joboffer->weoffer;
            $data['offering']['wrap']                       = true;
        }

        if(!empty($joboffer->wesearch)) {
            $data['lookingfor']['val']                      = $joboffer->wesearch;
            $data['lookingfor']['wrap']                     = true;
        }

        if(!empty($joboffer->applications_documents)) {
            $data['required_documents']['val']              = $joboffer->applications_documents;
            $data['required_documents']['wrap']             = true;
        }

        foreach($joboffer->companyjobs as $ji => $cj ) {
            if (!empty($joboffer->companylocations)) {

                foreach($joboffer->companylocations as $li => $cl){
                    $tmp_data = array();

                    $tmp_data['url']['val'] = $joboffer->url . '#city=' . $cl->id . '&job=' . $cj->id;
                    $tmp_data['url']['wrap'] = true;
                    $tmp_data['job']['val'] = $cj->name;
                    $tmp_data['job']['wrap'] = true;

                    $tmp_data['location']['val'] = array(
                        'label' => array(
                            'val'   => $cl->name,
                            'wrap'  => true
                        ) 
                    );
                    if (!empty($cl->geo_latitude) && !empty($cl->geo_longitude)) {
                        $tmp_data['location']['val']['latitude']['val'] = $cl->geo_latitude;
                        $tmp_data['location']['val']['longitude']['val'] = $cl->geo_longitude;
                    }

                    $r[] = array_merge($data, $tmp_data);
                    
                }

            } else {

                $tmp_data = array();
                $tmp_data['url']['val'] = $joboffer->url . '#job=' . $cj->id;
                $tmp_data['url']['wrap'] = true;
                $tmp_data['job']['val'] = $cj->name;
                $tmp_data['job']['wrap'] = true;

                $r[] = array_merge($data, $tmp_data);
            }
        }

        return $r;
    }

    private function prepareJobrapidoItemData($joboffer)
    {


        if (empty($joboffer->companyjobs)) {
            return array();
        }

        $r = array();
        $data = array();
        $company = $this->_webdir->getCompanyItem($joboffer->company);
        $data['company'] = array(
                                    'val'   => $company->name,
                                    'wrap'  => true
        );
        $data['publishdate']['val']           = $joboffer->created;
        $data['publishdate']['wrap']                    = true;

        foreach($joboffer->companyjobs as $ji => $cj ) {
            if (!empty($joboffer->companylocations)) {

                foreach($joboffer->companylocations as $li => $cl){
                    $tmp_data = array();

                    $tmp_data['description']['val'] = !empty($joboffer->description) ? $joboffer->description :
                        (!empty($joboffer->weoffer) ? $joboffer->weoffer : (
                            'Ausbildung als ' . $cj->name . ' bei ' . $company->name . ' in ' . $cl->name
                    ));

                    $tmp_data['url']['val'] = $joboffer->url . '#city=' . $cl->id . '&job=' . $cj->id;
                    $tmp_data['url']['wrap'] = true;
                    $tmp_data['title']['val'] = $cj->name;
                    $tmp_data['title']['wrap'] = true;

                    $tmp_data['location'] = array(
                            'val'   => $cl->name,
                            'wrap'  => true
                    );

                    $r[] = array_merge($data, $tmp_data);
                    
                }

            } else {

                $tmp_data['description']['val'] = !empty($joboffer->description) ? $joboffer->description :
                    (!empty($joboffer->weoffer) ? $joboffer->weoffer : (
                        'Ausbildung als ' . $cj->name . ' bei ' . $company->name . ' in bundesweit'
                    ));

                $tmp_data = array();
                $tmp_data['url']['val'] = $joboffer->url . '#job=' . $cj->id;
                $tmp_data['url']['wrap'] = true;
                $tmp_data['title']['val'] = $cj->name;
                $tmp_data['title']['wrap'] = true;

                $r[] = array_merge($data, $tmp_data);
            }
        }

        return $r;

    }

    private function getItemXml($data)
    {

        $xml = '';

        foreach($data as $tag => $job) {
            $xml .= $this->getOneLevelXmlTag($tag, $job);
        }

        return $xml;
    }

    private function getOneLevelXmlTag($tag, $job)
    {
            if (empty($job['val']) && $job['val'] !== 0) {
                return '';
            }

            $xml = '<' . $tag . '>';
            if(is_array($job['val'])) {
                foreach ($job['val'] as $t => $j) {
                    $xml .= $this->getOneLevelXmlTag($t, $j);
                }
            } else {
                $job['val'] = $this->remove_javascript($job['val']);
                if (!empty($job['wrap'])) {
                    $job['val'] = '<![CDATA[' . $job['val'] . ']]>';
                }
                $xml .= $job['val'];
            }
            $xml .= '</' . $tag . '>';
        
            return $xml;
    }

    private function remove_javascript($text) {
        return preg_replace(
            '/<script\b[^>]*>(.*?)<\/script>/is',
            '',
            $text
        );
    }

}
