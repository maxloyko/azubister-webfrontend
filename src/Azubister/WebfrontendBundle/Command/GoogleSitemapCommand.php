<?php

namespace Azubister\WebfrontendBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GoogleSitemapCommand extends ContainerAwareCommand
{

    private $_output = null;

    protected function configure()
    {
        $this 
            ->setName('azubister:sitemap:generate')
            ->setDescription('Generates sitemap');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_output = $output;
        $path = realpath(__DIR__.'/../../../../web');
        $sitemapFilename = $path.'/sitemap.xml';

        $this->filesDir = $path.'/files';
        if (!is_dir($this->filesDir)) {
            mkdir($this->filesDir);
        }


        $sitemaps = array();

        $sitemaps[] = $this->generateStaticSitemap();
        $sitemaps[] = $this->generateJobSitemap();
        $sitemaps[] = $this->generateCompanySitemap();
        $sitemaps[] = $this->generateJobofferSitemap();
        $sitemaps[] = $this->generateStaticpageSitemap();
        $sitemaps[] = '/magazin/sitemap.xml.gz';

        $globalXml = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
        $globalXml .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";

        $today = date('Y-m-d');
        foreach ($sitemaps as $s) {
            $globalXml .= '<sitemap>'."\n";
            $globalXml .= '<loc>http://www.azubister.net'.$s.'</loc>'."\n";
            $globalXml .= '<lastmod>'.$today.'</lastmod>'."\n";
            $globalXml .= '</sitemap>'."\n";
        }

        $globalXml .= '</sitemapindex>';
    
        file_put_contents($sitemapFilename, $globalXml);
        $output->writeln('Global file saved.');
    }

    private function generateStaticSitemap() {
        $generator = $this->getContainer()->get('router');
        $urls = array(
            array(
                'name' => 'homepage',
            ),
            array(
                'name' => 'job_landing',
            ),
            array(
                'name' => 'company_landing',
            ),
            array(
                'name' => 'joboffer_landing',
            ),
        );

        $this->_output->write('Static pages: '); $i = 0;
        $staticXml = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
        $staticXml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";

        foreach ($urls as $url) {
            $staticXml .= "<url>\n";
            $staticXml .= "<loc>http://www.azubister.net".$generator->generate($url['name'])."</loc>\n";
            $staticXml .= $this->getLastModToday();
            $staticXml .= $this->getFreq();
            $staticXml .= "</url>\n";
            $i++;

        }

        $staticXml .= '</urlset>';
        file_put_contents($this->filesDir.'/sitemap_static.xml', $staticXml);
        $this->_output->writeln(' '.$i.' Total static pages');

        return $generator->generate('homepage').'files/sitemap_static.xml';
    }

    private function generateJobSitemap() {
        //$generator = $this->getContainer()->get('router');
        $xml = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
        $xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";

        $items = $this->getContainer()->get('webdirectory')->getJobsCollectionRequest()->setFilters(array('active' => true))->setLimit(0)->execute();
        $this->_output->write('Jobs: '); $i = 0;
        foreach ($items as $item) {
            $xml .= "<url>\n";
            $xml .= "<loc>".$item->url."</loc>\n";
            $xml .= $this->getLastModToday();
            $xml .= $this->getFreq();
            $xml .= "</url>\n";
            $i++;

            if ($i % 10 == 0) $this->_output->write('*');
        }
        $xml .= '</urlset>';
        $this->_output->writeln(' '.$i.' Total jobs');
        file_put_contents($this->filesDir.'/sitemap_jobs.xml', $xml);
        return '/files/sitemap_jobs.xml';
        //return $generator->generate('homepage').'files/sitemap_jobs.xml';
    }

    private function generateCompanySitemap() {
        $generator = $this->getContainer()->get('router');
        $xml = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
        $xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
        $items = $this->getContainer()->get('webdirectory')->getCompaniesCollectionRequest()->setFilters(array('profile_active' => true, 'profile_visible' => true))->setLimit(0)->execute();
        $this->_output->write('Companies: '); $i = 0;
        foreach ($items as $item) {
            $xml .= "<url>\n";
            $xml .= "<loc>".$item->url."</loc>\n";
            $xml .= $this->getLastModToday();
            $xml .= $this->getFreq();
            $xml .= "</url>\n";
            $i++;

            if ($i % 10 == 0) $this->_output->write('*');
        }
        $xml .= '</urlset>';
        $this->_output->writeln(' '.$i.' Total companies');
        file_put_contents($this->filesDir.'/sitemap_companies.xml', $xml);
        return $generator->generate('homepage').'files/sitemap_companies.xml';
    }

    private function generateJobofferSitemap() {
        $generator = $this->getContainer()->get('router');
        $xml = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
        $xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
        $items = $this->getContainer()->get('webdirectory')->getJoboffersActualCollection();
        $this->_output->write('Joboffers: '); $i = 0;
        foreach ($items as $item) {
            $xml .= "<url>\n";
            $xml .= "<loc>".$item->url."</loc>\n";
            $xml .= $this->getLastModToday();
            $xml .= $this->getFreq();
            $xml .= "</url>\n";
            $i++;

            if ($i % 10 == 0) $this->_output->write('*');
        }
        $xml .= '</urlset>';
        $this->_output->writeln(' '.$i.' Total joboffers');

        file_put_contents($this->filesDir.'/sitemap_joboffers.xml', $xml);
        return $generator->generate('homepage').'files/sitemap_joboffers.xml';
    }

    private function generateStaticpageSitemap() {
        $generator = $this->getContainer()->get('router');
        $xml = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
        $xml .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
        $items = $this->getContainer()->get('webdirectory')->getStaticpagesActualCollection();
        $this->_output->write('Staticpages: '); $i = 0;
        foreach ($items as $item) {
            $company = $this->getContainer()->get('webdirectory')->getCompanyItem($item->company);
            $xml .= "<url>\n";
            $xml .= "<loc>http://www.azubister.net".$generator->generate('company_static_page', array('company_slug' => $company->slug, 'static_slug' => $item->slug))."</loc>\n";
            $xml .= $this->getLastModToday();
            $xml .= $this->getFreq();
            $xml .= "</url>\n";
            $i++;

            if ($i % 10 == 0) $this->_output->write('*');
        }
        $xml .= '</urlset>';
        $this->_output->writeln(' '.$i.' Total staticpages');
        file_put_contents($this->filesDir.'/sitemap_staticpages.xml', $xml);
        return $generator->generate('homepage').'files/sitemap_staticpages.xml';
    }

    private function getLastModToday() {
        $today = date('Y-m-d');
        return "<lastmod>".$today."</lastmod>\n";
    }

    private function getFreq() {
        return "<changefreq>daily</changefreq>\n";
    }
}
