<?php

namespace Azubister\WebfrontendBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AzubisterWebfrontendBundle extends Bundle
{
	public function boot()
	{
		parent::boot();
		include("Azubister/Webdirectory/bootstrap-symfony.php");
	}
}
